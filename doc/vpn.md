# Wireguard VPN Dialin

## wg-quick template

```ini
[Interface]
Address = 172.20.76.TODO/28
Address = fd23:42:c3d2:585::TODO/64
Address = 2a00:8180:2c00:285::TODO/64
PrivateKey = TODO

[Peer]
PublicKey = PG2VD0EB+Oi+U5/uVMUdO5MFzn59fAck6hz8GUyLMRo=
AllowedIPs = 0.0.0.0/0, ::/0
Endpoint = upstream4.dyn.zentralwerk.org:1337
```
