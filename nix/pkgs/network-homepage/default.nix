{ self
, substituteAll
, stdenv
, pandoc
, bat
, ansi2html
}:

let

  inherit (self.packages.${stdenv.system})
    export-config
    gateway-report network-graphs
    subnetplans switch-report vlan-report;

in
stdenv.mkDerivation {
  pname = "network-homepage";
  version = self.lastModifiedDate;

  src = ./src;

  nativeBuildInputs = [
    pandoc
    bat
    ansi2html
  ];
  buildPhase = ''
    pandoc -t html ${../../../doc/hello.md} > index.html
    cat ${./linked-data.html} >> index.html
    pandoc -t html ${../../../doc/vpn.md} > vpn.html
    pandoc -t html ${gateway-report} > gateway-report.html
    cat ${../../../cabling.md} ${switch-report} >> switch-report.md
    pandoc -t html switch-report.md > switch-report.html
    pandoc -t html ${vlan-report} > vlan-report.html

    echo '<pre>' > config.html
    bat --color=always --theme=GitHub -p ${export-config} | \
      ansi2html -il >> config.html
    echo '</pre>' >> config.html

    ln -s ${substituteAll {
      src = ./figure.html;
      img = "physical.png";
      caption = "Physikalische Netzwerkstruktur";
    }} physical.html
    ln -s ${substituteAll {
      src = ./figure.html;
      img = "logical.png";
      caption = "Logische Netzwerkstruktur";
    }} logical.html

    ln -s ${subnetplans}/share/doc/subnetplans/* .
    for F in *.html; do
      cat ${./header.html} "$F" ${./footer.html} > "$F.new"
      rm "$F"
      mv "$F.new" "$F"
    done
  '';

  installPhase = ''
    DIR=$out/share/doc/network-homepage/www
    mkdir -p $DIR

    ln -s ${network-graphs}/share/doc/network-homepage/* $DIR/
    ln -s ${../../../doc/core.png} $DIR/core.png
    ln -s ${substituteAll {
      src = ./security.txt;
      inherit (self.lib.config.site.data) contact hiringLink;
    }} $DIR/security.txt
    ln -s ${./captive.json} $DIR/captive.json
    cp *.{html,css,png,svg} $DIR/

    mkdir -p $out/nix-support
    echo doc homepage $DIR index.html >> $out/nix-support/hydra-build-products
  '';
}
