{ lib, ruby, starlink-grpc, writeShellScriptBin }:

writeShellScriptBin "starlink-stats" ''
  DEST=$1
  COMMAND=get_status

  while true; do
    ${lib.getExe starlink-grpc} $DEST $COMMAND | ${lib.getExe ruby} ${./convert.rb}

    sleep 60
  done
''
