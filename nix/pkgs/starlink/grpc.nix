{ lib, grpcurl, writeShellScriptBin }:

writeShellScriptBin "grpc" ''
  DEST=$1
  COMMAND=$2

  exec ${lib.getExe grpcurl} -plaintext -d "{\"$COMMAND\":{}}" $DEST SpaceX.API.Device.Device/Handle
''
