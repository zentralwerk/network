{ pkgs }:

rec {
  starlink-grpc = pkgs.callPackage ./grpc.nix { };
  starlink-stats = pkgs.callPackage ./stats.nix {
    inherit starlink-grpc;
  };
}
