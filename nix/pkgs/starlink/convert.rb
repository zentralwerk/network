#!/usr/bin/env ruby

# Converts Starlink status JSON to a format that is suitable for collectd exec

require 'json'

HOSTNAME = IO::readlines("/proc/sys/kernel/hostname").join.strip

def out path, x
  puts "PUTVAL \"#{HOSTNAME}/exec-starlink/current-#{path.join '.'}\" N:#{x}"
end

def recurse path, x
  if x.kind_of? Hash
    x.each { |k, v|
      recurse path + [k], v
    }
  elsif x.kind_of? Array
    x.each_with_index { |v, i|
      recurse path + [i], v
    }

  elsif x.kind_of? Float
    out path, x
  elsif x.kind_of? Integer
    out path, x.to_f
  elsif x.kind_of? String and x =~ /^\-?\d*\.?\d+$/
    out path, x.to_f
  end
end


recurse [], JSON.load(STDIN)
