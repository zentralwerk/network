{ self, lib, pkgs }:
let
  config = self.lib.config;
in
pkgs.writeText "vlan-report.md" ''
  # VLAN Report

  ${lib.concatMapStrings (net: ''
    ## ${net}${lib.optionalString (config.site.net.${net}.vlan != null) " (VLAN ${toString config.site.net.${net}.vlan})"}
    ${lib.concatStringsSep "\n" (
      lib.concatMap (host:
        map (target: "- ${host} -> ${target}") (
          builtins.attrNames (
            lib.filterAttrs (_: { nets, ... }:
              lib.elem net nets
            ) config.site.hosts.${host}.links
          )
        )
      ) (lib.attrNames config.site.hosts)
    )}

  '') (lib.attrNames config.site.net)}
''
