{ bundlerEnv, stdenv }:
let
  gems = bundlerEnv {
    name = "gems-for-subnetplan";
    gemdir = ./.;
  };
in stdenv.mkDerivation rec {
  name = "subnetplan";
  buildInputs = [ gems.wrappedRuby ];
  buildCommand = ''
    install -D -m755 ${./render.rb} $out/bin/${name}
    patchShebangs $out/bin/${name}
  '';
}
