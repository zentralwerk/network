{ self, config, nixpkgs, system, openwrt-imagebuilder }:

let
  pkgs = nixpkgs.legacyPackages.${system};
  inherit (pkgs) lib;

  rootfs-packages =
    builtins.foldl' (rootfs: hostName: rootfs // {
      "${hostName}-rootfs" = self.nixosConfigurations.${hostName}.config.system.build.toplevel;
    }) {} (
      builtins.attrNames (
        lib.filterAttrs (_: { role, ... }: builtins.elem role ["server" "container"])
          config.site.hosts
      )
    );

  lxc-configs =
    builtins.foldl' (rootfs: hostName: rootfs // {
      "${hostName}-lxc-config" = self.nixosConfigurations.${hostName}.config.system.build.lxcConfig;
    }) {} (
      builtins.attrNames (
        lib.filterAttrs (_: { role, ... }: role == "container")
          config.site.hosts
      )
    );

  vm-packages =
    builtins.foldl' (rootfs: hostName: rootfs // {
      "${hostName}-vm" = self.nixosConfigurations.${hostName}.config.system.build.vm
        .overrideAttrs (_oa: {
          meta.mainProgram = "run-${hostName}-vm";
        });
    }) {} (
      builtins.attrNames (
        lib.filterAttrs (_: { role, ... }: role == "server")
          config.site.hosts
      )
    );

  openwrt = import ./openwrt { inherit self lib openwrt-imagebuilder pkgs; };

  openwrt-packages = builtins.foldl' (images: hostName: images // {
    ${hostName} = pkgs.writeScriptBin "${hostName}.sh" (
      openwrt.sshScript hostName
    );
    "${hostName}-image" = openwrt.buildImage hostName;
  }) {} (
    builtins.attrNames (
      lib.filterAttrs (_: { role, ... }:
        role == "ap"
      ) config.site.hosts
    )
  );
in
rootfs-packages // lxc-configs // vm-packages
// (import ./device-templates.nix { inherit self lib pkgs; })
// openwrt-packages
// (import ./network-graphs.nix { inherit config lib pkgs; })
// (import ./network-cypher-graphs.nix { inherit config lib pkgs; })
// (import ./starlink { inherit pkgs; })
// (import ./subnetplans.nix { inherit self lib pkgs; })
// rec {
  encrypt-secrets = pkgs.writeShellScriptBin "encrypt-secrets" ''
    cd config
    exec ${lib.getExe pkgs.gnupg} --armor --batch --trust-model always \
      --encrypt -r 1F0F221A7483B5EF5D103D8B32EBADE870BAF886 \
      < secrets-production.nix \
      > secrets-production.nix.gpg
  '';

  export-config = pkgs.writeText "config.nix" (
    lib.generators.toPretty {} (
      lib.recursiveUpdate
        config
        { site.dns.localZones = self.lib.dns.localZones; }
    ));

  export-openwrt-models = pkgs.writeText "openwrt-models.nix" (
    lib.generators.toPretty {} self.lib.openwrtModels
  );

  decrypt-secrets = pkgs.writeShellScriptBin "decrypt-secrets" ''
    cd config
    [ -e secrets-production.nix ] && \
      mv secrets-production.nix secrets-production.nix.old
    exec ${pkgs.gnupg}/bin/gpg -d \
      > secrets-production.nix \
      < secrets-production.nix.gpg
  '';

  dns-slaves = import ./dns-slaves.nix { inherit self lib pkgs; };

  gateway-report = import ./gateway-report.nix { inherit self lib pkgs; };

  network-homepage = pkgs.callPackage ./network-homepage { inherit self; };

  switch-report = import ./switch-report.nix { inherit self lib pkgs; };

  switch-to-production = pkgs.writeShellScriptBin "decrypt-secrets" ''
    ${lib.getExe decrypt-secrets}

    cd config
    cp secrets-production.nix secrets.nix
  '';

  vlan-report = import ./vlan-report.nix { inherit self lib pkgs; };
}
