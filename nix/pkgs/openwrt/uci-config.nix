{ self, pkgs, hostName }:
with pkgs;
with lib;
let
  inherit (self.lib) config;
  hostConfig = config.site.hosts.${hostName};

  uciDeleteAll = key: ''
    while uci -q delete ${key}[-1]; do :; done
  '';

  openwrtModel =
    # OpenWrt v18 misses the info, and in newer releases eth0 and eth1
    # are swapped for TL-WR841Nv8.
    if builtins.match "tl-wr[78].*" hostConfig.model != null
    then {
      ports = {
        "0" = {
          index = "0";
          interface = "eth1";
          switch = "switch0";
          type = "host";
        };
        "1" = {
          index = "1";
          port = "lan:4";
          switch = "switch0";
          type = "port";
        };
        "2" = {
          index = "2";
          port = "lan:1";
          switch = "switch0";
          type = "port";
        };
        "3" = {
          index = "3";
          port = "lan:2";
          switch = "switch0";
          type = "port";
        };
        "4" = {
          index = "4";
          port = "lan:3";
          switch = "switch0";
          type = "port";
        };
        eth1 = {
          interface = "eth0";
          port = "wan";
          type = "phys";
        };
      };
    }
    else
      self.lib.getOpenwrtModel hostConfig.model;

  hasSwitch =
    if hostConfig.model == "ubnt_unifiac-mesh"
      # ours don't come with a switch.
    then false
    else
      openwrtModel ? ports
      &&
      any ({ switch ? null, ... }: switch != null)
        (builtins.attrValues openwrtModel.ports);
  hasDSA = (
    all ({ switch ? null, ... }:
      switch == null
    ) (builtins.attrValues openwrtModel.ports or {})
    &&
    any ({ port ? null, interface ? null, ... }:
      port != null &&
      interface != null
    ) (builtins.attrValues openwrtModel.ports or {})
  ) || hostConfig.model == "ubnt_unifi-usg";

  portsDoc =
    let
      portByIndex = builtins.foldl' (result: port:
        let
          key = if port ? index
                then port.index
                else if port ? interface
                then port.interface
                else "How to identify port ${generators.toPretty {} port}?";
        in result // {
          "${key}" = port;
        }
      ) {} (builtins.attrValues openwrtModel.ports);
    in
      concatMapStringsSep ", " (index:
        "${index}:${
          if portByIndex.${index} ? port
          then portByIndex.${index}.port
          else if portByIndex.${index} ? interface
          then portByIndex.${index}.interface
          else throw "${hostName}: What is port ${generators.toPretty {} portByIndex.${index}.port}?"
        }"
      ) (
        builtins.sort builtins.lessThan (
          builtins.attrNames portByIndex
        )
      );

  switchHostInterface =
    let
      hostPorts = sort builtins.lessThan (
        map ({ interface, ... }: interface) (
          builtins.attrValues (
            filterAttrs (_: { type, ... }: type == "host")
              openwrtModel.ports
          )
        )
      );
    in if hostPorts == []
      then throw "${hostName}: No host ports found for OpenWRT model ${hostConfig.model}"
      else builtins.head hostPorts;

  switchPortIndices = f:
    map ({ index, ... }: index) (
      builtins.attrValues (
        filterAttrs (_: port: port ? index && f port)
          openwrtModel.ports
      )
    );

  trunked = map (index: "${index}t");

  # OpenWRT switch ports string ("0t 1t 2 3 4") for a network (VLAN)
  switchPortsConfig = net:
    concatStringsSep " " (
      # Host interface
      trunked (switchPortIndices ({ interface ? null, ... }: interface == switchHostInterface))
      ++
      # Access networks
      optionals (hostConfig.links ? ${net}) (
        builtins.concatMap (port':
          switchPortIndices ({ port ? null, ... }: port == port')
        ) hostConfig.links.${net}.ports
      )
      ++
      # Trunk ports
      builtins.concatMap (port':
        trunked (switchPortIndices ({ port ? null, ... }: port == port'))
      ) (
        builtins.concatMap ({ ports, ... }: ports) (
          builtins.attrValues (
            filterAttrs (_: { trunk, ... }:
              trunk
            ) hostConfig.links
          ))
      )
    );

  dsaPorts = net:
    unique (
      concatMap ({ ports, ... }: ports) (
        builtins.filter ({ nets, ... }: builtins.elem net nets)
          (builtins.attrValues hostConfig.links)
      ));

  dsaPortType = net: port:
    if any ({ ports, trunk, ... }: trunk && builtins.elem port ports) (
      builtins.attrValues hostConfig.links
    ) || hostConfig.links.${net}.trunk or true
    then "t"
    else "u*";

  networkInterfaces = net:
    let
      inherit (config.site.net.${net}) vlan;
    in unique (
      builtins.concatMap ({ trunk, ports, ... }:
        builtins.concatMap (port:
          builtins.concatMap (portData:
            lib.optionals (portData ? port && port == portData.port) [
              ((
                if portData ? switch
                then switchHostInterface
                else
                if portData ? interface
                then portData.interface
                else throw "${hostName}: Cannot find interface for ${port} on OpenWRT model ${hostConfig.model}"
              ) + lib.optionalString (trunk || portData.switch != null) ".${toString vlan}")
            ]
          ) (builtins.attrValues openwrtModel.ports)
          ++
          optionals (hostConfig.interfaces ? ${port} && vlan != null) [ "${port}.${toString vlan}" ]
        ) ports
      ) (
        builtins.attrValues (
          filterAttrs (link: { nets, ... }:
            link == net || builtins.elem net nets
          ) hostConfig.links
        )
      )
    );

  mgmtInterface =
    if hasDSA
    then "br0.${toString config.site.net.mgmt.vlan}"
    else
      let
        mgmtInterfaces = networkInterfaces "mgmt";
      in if builtins.length mgmtInterfaces == 1
        then builtins.head mgmtInterfaces
        else "br-mgmt";

in
''
  # Set root password
  echo -e '${hostConfig.password}\n${hostConfig.password}' | passwd

  # add ssh pubkeys
  ${concatMapStrings (sshPubKey: ''
    echo "${sshPubKey}" > /etc/dropbear/authorized_keys
  '') config.site.sshPubKeys}

  # System configuration
  ${uciDeleteAll "network.@switch_vlan"}
  ${uciDeleteAll "wireless.@wifi-iface"}

  uci set system.@system[0].hostname=${hostName}
  uci set dhcp.@dnsmasq[0].enabled=0
  uci set system.@system[0].log_ip=${config.site.net.mgmt.hosts4.logging}
  uci set system.@system[0].log_proto=udp

  ${optionalString hasSwitch ''
    # Switch config
    # Ports ${portsDoc}
    ${concatMapStrings (net: ''
      uci add network switch_vlan
      uci set network.@switch_vlan[-1]=switch_vlan
      uci set network.@switch_vlan[-1].device='switch0'
      uci set network.@switch_vlan[-1].vlan='${toString config.site.net.${net}.vlan}'
      uci set network.@switch_vlan[-1].ports='${switchPortsConfig net}'
      uci set network.@switch_vlan[-1].comment='${net}'
    '') (
      sort (net1: net2:
        config.site.net.${net1}.vlan < config.site.net.${net2}.vlan
      ) (
        unique (
          builtins.concatMap ({ nets, ... }: nets)
            (builtins.attrValues hostConfig.links)
        )
      )
    )}
  ''}
  ${optionalString hasDSA ''
    # DSA
    ${uciDeleteAll "network.@device"}
    uci add network device
    uci set network.@device[-1].name='br0'
    uci set network.@device[-1].type='bridge'
    ${concatMapStrings (port: ''
      uci add_list network.@device[-1].ports='${
        if (openwrtModel.ports.${port} or {}).type or null == "phys"
        then openwrtModel.ports.${port}.interface
        else port
      }'
    '') (
      unique (
        builtins.concatMap ({ ports, ... }: ports)
          (builtins.attrValues hostConfig.links)
      )
    )}
    uci set network.br0='interface'
    uci set network.br0.proto='none'
    uci set network.br0.device='br0'

    ${concatMapStrings (net: ''
      uci add network bridge-vlan
      uci set network.@bridge-vlan[-1].device='br0'
      uci set network.@bridge-vlan[-1].vlan='${toString config.site.net.${net}.vlan}'
      ${concatMapStrings (port: ''
        uci add_list network.@bridge-vlan[-1].ports='${
          if (openwrtModel.ports.${port} or {}).type or null == "phys"
          then openwrtModel.ports.${port}.interface
          else port
        }:${dsaPortType net port}'
      '') (dsaPorts net)}
    '') (
      sort (net1: net2:
        config.site.net.${net1}.vlan < config.site.net.${net2}.vlan
      ) (
        unique (
          builtins.concatMap ({ nets, ... }: nets)
            (builtins.attrValues hostConfig.links)
        )
      )
    )}
  ''}

  # mgmt network
  uci set network.mgmt=interface
  ${if hasDSA
    then ''
      uci set network.mgmt.device='br0.${toString config.site.net.mgmt.vlan}'
    '' else ''
      uci set network.mgmt.ifname='${
        if builtins.length (networkInterfaces "mgmt") > 0
        then concatStringsSep " " (networkInterfaces "mgmt")
        else throw "${hostName}: No interface for mgmt"
      }'
    ''}
  uci set network.mgmt.proto=static
  ${optionalString (hostConfig.interfaces.mgmt.type == "bridge") ''
    uci set network.mgmt.type=bridge
  ''}
  uci set network.mgmt.ipaddr=${config.site.net.mgmt.hosts4.${hostName}}
  uci set network.mgmt.netmask=${self.lib.netmasks.${toString config.site.net.mgmt.subnet4Len}}
  uci set network.mgmt.gateway=${config.site.net.mgmt.hosts4.mgmt-gw}
  uci set network.mgmt.ip6addr=${config.site.net.mgmt.hosts6.dn42.${hostName}}/64
  uci set network.mgmt.ip6gw=${config.site.net.mgmt.hosts6.dn42.mgmt-gw}
  uci -q delete network.mgmt.dns || true
  uci add_list network.mgmt.dns=${config.site.net.serv.hosts4.dnscache}
  uci add_list network.mgmt.dns=${config.site.net.serv.hosts6.dn42.dnscache}

  uci -q delete network.globals.ula_prefix || true
  # delete unused networks
  ${concatMapStrings (net:
    optionalString (! hostConfig.interfaces ? ${net}) ''
      uci -q delete network.${net} || true
    ''
  ) ([ "lan" "wan" "wan6" ] ++ builtins.attrNames config.site.net)}

  # bridged and static networks
  ${concatMapStrings (net:
    let
      iface = hostConfig.interfaces.${net};
    in optionalString (net != "mgmt" && builtins.elem iface.type ["bridge" "phys"]) ''
      uci set network.${net}=interface
      ${optionalString (iface.type == "bridge") ''
        uci set network.${net}.type=bridge
        uci add network device
        uci set network.@device[-1].name='${net}'
        uci set network.@device[-1].type='bridge'
      ''}
      uci set network.${net}.proto=static
      ${if hasDSA
        then ''
          uci set network.${net}.device='br0.${toString config.site.net.${net}.vlan}'
        '' else ''
          uci set network.${net}.ifname='${concatStringsSep " " (networkInterfaces net)}'
        ''}
      ${optionalString (config.site.net.${net}.mtu != null) ''
        uci set network.${net}.mtu=${toString config.site.net.${net}.mtu}
      ''}


      ${optionalString (config.site.net.${net}.hosts4 ? ${hostName}) ''
        # address in net
        uci set network.${net}.ipaddr=${config.site.net.${net}.hosts4.${hostName}}
        uci set network.${net}.netmask=${self.lib.netmasks.${toString config.site.net.${net}.subnet4Len}}
      ''}
      ${concatMapStrings (hosts6: optionalString (hosts6 ? ${hostName}) ''
        uci set network.${net}.ip6addr=${hosts6.${hostName}}/64
      '') (builtins.attrValues config.site.net.${net}.hosts6)}
    '') (builtins.attrNames hostConfig.interfaces)
  }

  # vxlan trunks
  ${concatMapStrings (name:
    let
      iface = hostConfig.interfaces.${name};
    in optionalString (iface.type == "vxlan") ''
      uci set network.${name}=interface
      uci set network.${name}.proto=vxlan6
      uci set network.${name}.peer6addr='${iface.vxlan.peer}'
      uci set network.${name}.port=4789
      uci set network.${name}.rxcsum=0
      uci set network.${name}.txcsum=0
      uci set network.${name}.delegate=0
    '') (builtins.attrNames hostConfig.interfaces)
  }

  ${uciDeleteAll "wireless.radio"}
  uci -q delete wireless.default_radio0 || true
  uci -q delete wireless.default_radio1 || true
  ${concatStrings (imap0 (index: path:
    let
      radioConfig = hostConfig.wifi.${path};
      ifPrefix = if radioConfig.channel < 15 then "wlan2" else "wlan5";
    in ''
      uci set wireless.radio${toString index}=wifi-device
      uci set wireless.radio${toString index}.type=mac80211
      uci set wireless.radio${toString index}.country=DE
      uci set wireless.radio${toString index}.band=${radioConfig.band}
      uci set wireless.radio${toString index}.channel=${toString radioConfig.channel}
      uci set wireless.radio${toString index}.path=${path}
      uci set wireless.radio${toString index}.htmode=${radioConfig.htmode}
      uci set wireless.radio${toString index}.noscan=1
      uci -q delete wireless.radio${toString index}.disabled || true

      ${concatMapStrings (ssid:
        let
          ssidConfig = radioConfig.ssids.${ssid};
          netConfig = config.site.net.${ssidConfig.net};

          # mapping our option to openwrt/hostapd setting
          encryption = {
            none = "none";
            owe = "owe";
            wpa2 = "psk2";
            wpa3 = "sae-mixed";
          }.${ssidConfig.encryption};

          ifname =
            if ssidConfig.ifname != null
            then ssidConfig.ifname
            else "${ifPrefix}-${ssidConfig.net}";

          pad = len: prefix: s:
            if builtins.stringLength s < len
            then pad len prefix "${prefix}${s}"
            else s;

        in ''
          uci add wireless wifi-iface
          uci set wireless.@wifi-iface[-1].ifname=${ifname}
          uci set wireless.@wifi-iface[-1].device=radio${toString index}
          uci set wireless.@wifi-iface[-1].ssid='${ssid}'
          uci set wireless.@wifi-iface[-1].mode=${ssidConfig.mode}
          uci set wireless.@wifi-iface[-1].network=${ssidConfig.net}
          uci set wireless.@wifi-iface[-1].mcast_rate=18000
          uci set wireless.@wifi-iface[-1].hidden=${if ssidConfig.hidden then "1" else "0"}
          uci set wireless.@wifi-iface[-1].encryption='${encryption}'
          ${if (ssidConfig.psk != null)
            then ''
              uci set wireless.@wifi-iface[-1].key='${ssidConfig.psk}'
            ''
            else ''
              uci -q delete wireless.@wifi-iface[-1].key || true
            ''}
          ${lib.optionalString (!ssidConfig.disassocLowAck) ''
            uci set wireless.@wifi-iface[-1].disassoc_low_ack='0'
          ''}

          ${lib.optionalString (netConfig.wifi.ieee80211rKey != null) ''
            # for usteerd
            # see https://www.libe.net/en-wlan-roaming#client-steering
            # https://openwrt.org/docs/guide-user/network/wifi/usteer#configure_80211k_and_80211v_on_all_ap-nodes
            uci set wireless.@wifi-iface[-1].bss_transition=1
            uci set wireless.@wifi-iface[-1].wnm_sleep_mode=1
            uci set wireless.@wifi-iface[-1].time_advertisement=2
            uci set wireless.@wifi-iface[-1].time_zone=GMT0
            uci set wireless.@wifi-iface[-1].ieee80211k=1
            uci set wireless.@wifi-iface[-1].rrm_neighbor_report=1
            uci set wireless.@wifi-iface[-1].rrm_beacon_report=1


            # breaks Apple devices connecting to wifi when used together with wpa2/wpa3 mixed mode (sae-mixed)
            # uci set wireless.@wifi-iface[-1].ieee80211r=1
            # when unset derived from interface MAC
            uci set wireless.@wifi-iface[-1].nasid=${pad 12 "0" (toString ((lib.toInt (lib.removePrefix "ap" hostName)) * 65536 + index))}
            # when unset derived from the first 4 chars of the md5 hashed SSID
            uci set wireless.@wifi-iface[-1].mobility_domain=${pad 4 "0" (lib.toHexString (49920 + netConfig.vlan))}

            # https://github.com/openwrt/openwrt/issues/7907
            # https://github.com/openwrt/openwrt/commit/2984a0420649733662ff95b0aff720b8c2c19f8a
            uci set wireless.@wifi-iface[-1].ft_over_ds=0
            # as recommend in 7907 and seems to fairly often trigger while testing
            uci set wireless.@wifi-iface[-1].reassociation_deadline=20000

            # might be unused if ft_over_ds is not used
            uci set wireless.@wifi-iface[-1].ft_bridge=${mgmtInterface}

            # otherwise the r0kh/r1kh options below are not applied
            uci set wireless.@wifi-iface[-1].ft_psk_generate_local=0

            # do not just rely on the monility domain for increased security
            # https://forum.openwrt.org/t/802-11r-fast-transition-how-to-understand-that-ft-works/110920/81
            uci set wireless.@wifi-iface[-1].r0kh=ff:ff:ff:ff:ff:ff,\*,${netConfig.wifi.ieee80211rKey}
            uci set wireless.@wifi-iface[-1].r1kh=00:00:00:00:00:00,00:00:00:00:00:00,${netConfig.wifi.ieee80211rKey}
            uci set wireless.@wifi-iface[-1].pmk_r1_push=1
          ''}
        ''
      ) (builtins.attrNames radioConfig.ssids)}
    '') (builtins.attrNames hostConfig.wifi))}

  uci set usteer.@usteer[0].network=mgmt
  uci set usteer.@usteer[0].load_kick_enabled=1
  uci set usteer.@usteer[0].load_kick_threshold=67
  uci set usteer.@usteer[0].signal_diff_threshold=15
  uci set usteer.@usteer[0].load_balancing_threshold=8
  uci set usteer.@usteer[0].band_steering_threshold=16

  uci commit

  # Add hotfixes for MTU settings
  cat >/etc/hotplug.d/iface/99-mtu <<__MTU__
  #!/bin/sh

  ${concatMapStrings (net:
    optionalString (config.site.net ? ${net} &&
                    config.site.net.${net}.mtu != null) ''
      if [ "\\\$ACTION" = ifup -a "\\\$INTERFACE" = ${net} ]; then
        ip link set \\\$DEVICE mtu ${toString config.site.net.${net}.mtu}
      fi
    '') (builtins.attrNames hostConfig.interfaces)
  }
  __MTU__

  ${optionalString hostConfig.wifiOnLink.enable ''
    # Cronjob that makes sure WiFi is only visible when server with all
    # the gateways is reachable
    cat >/etc/crontabs/root <<__CRON__
    * * * * *	/usr/sbin/wifi-on-link.sh
    * * * * *	/usr/sbin/usteer-info.sh
    __CRON__
    cat >/usr/sbin/wifi-on-link.sh <<__SH__
    #!/bin/sh

    if (ping -c 1 -W 3 ${config.site.net.mgmt.hosts4.mgmt-gw}) ; then
      REACHABLE=y
    else
      REACHABLE=n
    fi

    if [ "\\\$(cat /sys/class/net/wlan2-pub/operstate)" == "up" ] ; then
      UP=y
    else
      UP=n
    fi

    if [ -e /sys/class/leds/blue:dome ] ; then
      ERROR_LED=/sys/class/leds/blue:dome/brightness
      [ \\\$REACHABLE = y ] && echo 0 > \\\$ERROR_LED
      [ \\\$REACHABLE = n ] && echo 1 > \\\$ERROR_LED
    fi

    [ \\\$REACHABLE = y ] && [ \\\$UP = n ] && wifi up
    [ \\\$REACHABLE = n ] && [ \\\$UP = y ] && wifi down

    exit 0
    __SH__
    chmod a+rx /usr/sbin/wifi-on-link.sh
    /etc/init.d/cron restart

    cat > /etc/collectd.conf <<COLLECTD
    Hostname "${hostName}"
    FQDNLookup false
    Interval 10

    BaseDir "/var/run/collectd"
    Include "/etc/collectd/conf.d"
    PIDFile "/var/run/collectd.pid"
    PluginDir "/usr/lib/collectd"
    TypesDB "/usr/share/collectd/types.db"

    LoadPlugin cpu
    LoadPlugin load
    LoadPlugin interface
    LoadPlugin iwinfo
    LoadPlugin network
    LoadPlugin exec
    <Plugin network>
      Server "${config.site.net.serv.hosts6.dn42.stats}" "25826"
    </Plugin>
    <Plugin exec>
      Exec "nobody" "/usr/bin/usteer-stats.sh"
    </Plugin>
    COLLECTD
  ''}
  chmod +x /usr/bin/usteer-stats.sh /usr/sbin/usteer-info.sh

  for svc in dnsmasq uhttpd ; do
    rm -f /etc/rc.d/*\$svc
    /etc/init.d/\$svc stop || true
  done
''
