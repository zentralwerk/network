{ self, lib, openwrt-imagebuilder, pkgs }:

let
  inherit (self.lib) config;
  uciConfig = hostName: import ./uci-config.nix { inherit self pkgs hostName; };

  modelPackages = {
    "tplink_archer-c7-v2" = [
      "-kmod-ath10k-ct" "-ath10k-firmware-qca988x-ct-full-htt" "-ath10k-firmware-qca988x-ct"
      "kmod-ath10k" "ath10k-firmware-qca988x"
    ];
    "tplink_archer-c7-v5" = [
      "-kmod-ath10k-ct" "-ath10k-firmware-qca988x-ct" "-ath10k-firmware-qca988x-ct-full-htt"
      "kmod-ath10k" "ath10k-firmware-qca988x"
    ];
    "ubnt_unifiac-lite" = [
      "-kmod-ath10k-ct" "-ath10k-firmware-qca988x-ct"
      "kmod-ath10k" "ath10k-firmware-qca988x"
    ];
    "ubnt_unifiac-mesh" = [
      "-kmod-ath10k-ct" "-ath10k-firmware-qca988x-ct"
      "kmod-ath10k" "ath10k-firmware-qca988x"
    ];
    "dir-615-d" = [
      # flash size reasons
      "-wpad-openssl"
      "-tcpdump"
      "wpad-wolfssl"
    ];
  };
in rec {
  sshScript = hostName:
    let
      address = config.site.net.mgmt.hosts4.${hostName};
    in ''
      #! ${pkgs.runtimeShell}
      set -eou pipefail

      ssh root@${address} "cat > /tmp/openwrt-image" < ${buildImage hostName}/openwrt-*-${hostName}-*-sysupgrade.bin
      ssh root@${address} "sysupgrade -n /tmp/openwrt-image" || true

      # ssh hostkey will have changed after boot
      ssh-keygen -R ${address}
    '';

  buildImage = hostName:
    let
      hostConfig = config.site.hosts.${hostName};

      hasVxlan = builtins.any ({ type, ... }:
        type == "vxlan"
      ) (builtins.attrValues hostConfig.interfaces);

      inherit (hostConfig) model;

      matches = (openwrt-imagebuilder.lib.profiles {
        inherit pkgs;
      }).identifyProfiles model;

      fallbackProfile =
        if model == "dir-615-d"
        then (openwrt-imagebuilder.lib.profiles {
          inherit pkgs;
          release = "19.07.10";
        }).identifyProfile model
        else if builtins.match "tl-wr[78].*" model != null
        then {
          release = "18.06.9";
          packagesArch = "mips_24kc";
          target = "ar71xx";
          variant = "tiny";
          profile = model;
          sha256 = "sha256-P7BJI6n6s53szYXKshnJRKL2fLIYgJLPiq/yd0oRKoE=";
          feedsSha256 = {
            base.sha256 = "sha256-IbND2snJ1UrDRhvGQIRxzGuSpftQ+AyiWqaVZqbGdHY=";
            packages.sha256 = "sha256-18UvzdUL98CranBtzAY7hoUlEvafUdssAQOuqDQi4BU=";
          };
        }
        else null;

      build = args:
        openwrt-imagebuilder.lib.build (args // {
          extraImageName = "zw-${hostName}";
          packages = [
            # remove unused default .ipk
            "-dnsmasq" "-firewall" "-firewall4"
            "-ppp" "-ppp-mod-pppoe" "-kmod-ppp" "-kmod-pppoe" "-kmod-pppox"
            "-iptables" "-ip6tables" "-kmod-ipt-offload"
            "-odhcp6c" "-odhcpd-ipv6only"
            "-wpad-basic-mbedtls"
            # monitoring
            "collectd"
            "collectd-mod-iwinfo" "collectd-mod-network"
            "collectd-mod-interface" "collectd-mod-load" "collectd-mod-cpu"
            "collectd-mod-exec"
          ] ++ (
            if args.variant != "tiny"
            then [
              # debugging
              "htop"
              "tcpdump"
              # wpa3
              "-wpad-basic-wolfssl" "-wpad-mini"
              "wpad-openssl"
              "usteer"
            ] else [
              # debugging
              "tcpdump-mini"
              # wpa3
              "-wpad-openssl" "-wpad-mini"
              "wpad-wolfssl"
            ]
          ) ++ lib.optionals hasVxlan [
            "vxlan" "kmod-vxlan"
          ] ++ modelPackages.${model} or [];
          disabledServices = [ "dnsmasq" "uhttpd" ];
          files = pkgs.runCommandNoCC "image-files" {} ''
            mkdir -p $out/etc/uci-defaults
            cat > $out/etc/uci-defaults/99-zentralwerk <<EOF
            ${uciConfig hostName}
            EOF
            mkdir -p $out/usr/{bin,sbin}
            cp ${./usteer-info.sh} $out/usr/sbin/usteer-info.sh
            cp ${./usteer-stats.sh} $out/usr/bin/usteer-stats.sh
            chmod +x $out/usr/bin/*.sh $out/usr/sbin/*.sh
          '';
        });

    in
      if matches == [] && fallbackProfile != null
      then build fallbackProfile

      else if matches == []
      then builtins.trace "${hostName} (${model}) not supported by OpenWRT"
        null

      else if builtins.length matches == 1
      then build (builtins.elemAt matches 0)

      else builtins.trace "${hostName} (${model}) has multiple models!" (
        build (builtins.elemAt matches 0)
      );

}
