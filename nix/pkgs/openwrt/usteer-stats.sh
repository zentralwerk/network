#! /bin/sh

HOSTNAME=`cat /proc/sys/kernel/hostname`
INTERVAL=60

[ -p /tmp/usteer-info ] || mkfifo /tmp/usteer-info

while true; do
    if [ ! -p /tmp/usteer-info ]; then
      echo "/tmp/usteer-info went missing!"
      exit 1
    fi

    DATA="$(cat /tmp/usteer-info)"
    cd /sys/class/net
    for iface in wlan*; do
        eval $( echo "$DATA" | jsonfilter  \
            -e 'LOAD=@["hostapd.'$iface'"].load' \
            -e 'NOISE=@["hostapd.'$iface'"].noise' \
            -e 'N_ASSOC=@["hostapd.'$iface'"].n_assoc' \
            -e 'FREQ=@["hostapd.'$iface'"].freq' \
            -e 'ROAM_SOURCE=@["hostapd.'$iface'"].roam_events.source' \
            -e 'ROAM_TARGET=@["hostapd.'$iface'"].roam_events.target'
        )
        echo "PUTVAL \"$HOSTNAME/usteer_local_info-$iface/stations-load\" interval=$INTERVAL N:$LOAD"
        echo "PUTVAL \"$HOSTNAME/usteer_local_info-$iface/signal_noise-noise\" interval=$INTERVAL N:$NOISE"
        echo "PUTVAL \"$HOSTNAME/usteer_local_info-$iface/stations-n_assoc\" interval=$INTERVAL N:$N_ASSOC"
        echo "PUTVAL \"$HOSTNAME/usteer_local_info-$iface/frequency-freq\" interval=$INTERVAL N:$FREQ"
        echo "PUTVAL \"$HOSTNAME/usteer_local_info-$iface/transitions-roam_source\" interval=$INTERVAL N:$ROAM_SOURCE"
        echo "PUTVAL \"$HOSTNAME/usteer_local_info-$iface/transitions-roam_target\" interval=$INTERVAL N:$ROAM_TARGET"
    done
done
