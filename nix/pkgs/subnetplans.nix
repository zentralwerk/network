{ self, lib, pkgs }:

let
  config = self.lib.config;
  subnetplan = pkgs.callPackage ./subnetplan { };
in
rec {
  subnetplan4 =
    pkgs.stdenv.mkDerivation {
      name = "subnetplan4.html";
      src = builtins.toFile "subnets4.txt" (
        lib.concatMapStringsSep "\n" (net:
          "${config.site.net.${net}.subnet4} ${net}"
        ) (builtins.attrNames (
          lib.filterAttrs (_: { subnet4, ... }: subnet4 != null)
          config.site.net
        ))
      );
      buildInputs = [ subnetplan ];
      buildCommand = ''
        subnetplan < $src > $out
      '';
    };

  subnetplan6 =
    pkgs.stdenv.mkDerivation {
      name = "subnetplan6.html";
      src = builtins.toFile "subnets6.txt" (
        lib.concatMapStrings (net:
          lib.concatMapStrings (ctx: ''
            ${config.site.net.${net}.subnets6.${ctx}} ${net}
          '') (builtins.attrNames config.site.net.${net}.subnets6)
        ) (builtins.attrNames config.site.net)
      );
      buildInputs = [ subnetplan ];
      buildCommand = ''
        subnetplan < $src > $out
      '';
    };

  subnetplans = pkgs.runCommand "subnetplans" {} ''
    DIR=$out/share/doc/subnetplans
    mkdir -p $DIR $out/nix-support

    ${lib.concatMapStrings (pkg: ''
      ln -s ${pkg} $DIR/${pkg.name}
      echo doc report $DIR/${pkg.name} >> $out/nix-support/hydra-build-products
    '') [ subnetplan4 subnetplan6 ]}
  '';
}
