{ self, lib, pkgs }:

let
  servConf = self.lib.config.site.net.serv;
  masterAddrs =
    [servConf.hosts4.dns] ++
    map (hosts6: hosts6.dns)
      (builtins.attrValues servConf.hosts6);
  mastersStr =
    builtins.foldl' (result: addr:
      "${result} ${addr};"
    ) "" masterAddrs;
in

pkgs.writeText "named.slave.conf" (
  lib.concatMapStringsSep "\n" ({ name, ... }: ''
    zone "${name}" IN {
      type slave;
      masters {${mastersStr} };
      file "/var/lib/bind/slave/${name}.zone";
      allow-notify { ${mastersStr} };
      allow-query { any; };
    };
  '') (
    # public zones only
    builtins.filter ({ ns, ... }:
      ns == self.lib.dns.publicNS
    ) self.lib.dns.localZones
  )
)
