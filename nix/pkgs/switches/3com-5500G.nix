{ self, hostName, config, hostConfig, sort, sortBy, sortNetsByVlan
, lib, expect, uucp, inetutils}:

''
  #! ${lib.getExe expect} -f

  ${if hostConfig.firstboot
    then ''
      spawn ${uucp}/bin/cu -s 19200 -l /dev/ttyUSB0
      send "\r"
    ''
    else ''
      spawn ${inetutils}/bin/telnet ${config.site.net.mgmt.hosts4.${hostName}}

      expect "Password:"
      send "${hostConfig.password}\r"
    ''
  }
  expect ">"
  send "system-view\r"
  expect "]"

  send "sysname ${hostName}\r"
  expect "]"

  send "user-interface vty 0 4\r"
  expect "ui-vty0-4]"
  send "screen-length 0\r"
  expect "ui-vty0-4]"
  send "user privilege level 3\r"
  expect "ui-vty0-4]"
  send "set authentication password simple ${hostConfig.password}\r"
  expect "ui-vty0-4]"
  send "quit\r"
  expect "${hostName}]"

  send "local-user admin\r"
  expect -- "-luser-admin]"
  send "password simple ${hostConfig.password}\r"
  expect -- "-luser-admin]"
  send "quit\r"
  expect "${hostName}]"

  # Enable logging
  send "info-center enable\r"
  expect "]"
  send "info-center loghost ${config.site.net.mgmt.hosts4.logging} channel loghost facility local6\r"
  expect "]"
  send "info-center source default channel loghost log level informational\r"
  expect "]"

  ${lib.concatMapStrings (net:
    let
      netConfig = config.site.net.${net};
      vlan = toString netConfig.vlan;
      inherit (config.site.net.${net}) hosts4;
      hostAddr4 = hosts4.${hostName};
      prefixLength = lib.elemAt (
        builtins.split "/" netConfig.subnet4
      ) 2;
      netmask = self.lib.netmasks.${prefixLength};
    in
      if netConfig.vlan != null
      then ''
        send "vlan ${vlan}\r"
        expect -- "-vlan${vlan}]"
        send "name ${net}\r"
        expect -- "-vlan${vlan}]"
        ${lib.optionalString (net == "mgmt") ''
          # Actually only used for mgmt_vlan, switches are not routers
          send "interface Vlan-interface ${vlan}\r"
          expect "]"
          ${lib.optionalString (hosts4 ? ${hostName}) ''
            send "ip address ${hostAddr4} ${netmask}\r"
            expect "]"
          ''}
        ''}
        send "quit\r"
        expect "${hostName}]"
      ''
      else ""
    ) (sortNetsByVlan (builtins.attrNames config.site.net))
  }

  ${lib.concatMapStrings (name:
    let
      linkConfig = hostConfig.links.${name};
      isAccess = config.site.net ? ${name};
      netConfig = config.site.net.${name};
      isTrunk = !isAccess;
      isBond = isTrunk && builtins.length linkConfig.ports > 1;
    in
      if isTrunk
      then ''
        ${lib.optionalString isBond ''
          send "link-aggregation group ${linkConfig.group} mode static\r"
          expect {
            "This aggregation will be modified to static mode. Continue ?" {
              send "Y\r"
            }
            "]" {}
          }
          send "link-aggregation group ${linkConfig.group} description ${name}\r"
          expect "]"
        ''}
        ${lib.concatMapStrings (port: ''
          send "interface ${port}\r"
          expect "]"
          send "undo stp edged-port\r"
          expect "]"
          ${if isBond
            then ''
              send "lacp enable\r"
              expect "]"
              send "undo port link-aggregation group\r"
              expect "]"
              send "port link-aggregation group ${linkConfig.group}\r"
            '' else ''
              send "undo lacp enable\r"
            ''}
          expect "]"
          send "jumboframe enable\r"
          expect "]"

          send "port link-type trunk\r"
          expect "]"
          # Set dummy default vlan
          send "port trunk pvid vlan 4094\r"
          expect "]"
          # Deconfigure all but mgmt vlan
          send "undo port trunk permit vlan 2 to 4094\r"
          expect "]"
          ${lib.concatMapStrings (vlan: ''
            send "port trunk permit vlan ${toString vlan}\r"
            expect "]"
          '') (sort linkConfig.vlans)}
          send "undo shutdown\r"
          expect "]"
          send "quit\r"
          expect "${hostName}]"
        '') (sort linkConfig.ports)}
      '' else
        lib.concatMapStrings (port: ''
          send "interface ${port}\r"
          expect "]"
          send "undo port link-aggregation group\r"
          expect "]"
          send "port link-type access\r"
          expect "]"
          ${if name == "mgmt"
            then ''
              send "undo port access vlan\r"
              expect "]"
            '' else ''
              send "port access vlan ${toString netConfig.vlan}\r"
              expect "]"
            ''}
          send "undo shutdown\r"
          expect "]"
          send "quit\r"
          expect "${hostName}]"
      '') (sort linkConfig.ports)
  ) (sortBy (link: hostConfig.links.${link}.ports)
    (builtins.attrNames hostConfig.links)
  )}

  send "save main\r"
  expect "Y/N]"
  send "YES\r"
  expect "press the enter key):"
  send "\r"
  expect "]"
  send "quit\r"
  expect ">"
  send "quit\r"
''
