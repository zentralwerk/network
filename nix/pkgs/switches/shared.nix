{ config, lib }:

{
  sort = builtins.sort (a: b:
    if builtins.isList a && builtins.isList b
    then lib.compareLists a b
    else a < b
  );
  sortBy = f: builtins.sort (a: b:
    let
      ra = f a;
      rb = f b;
    in
      if builtins.isList ra && builtins.isList rb
      then lib.compareLists lib.compare ra rb < 0
      else ra < rb
  );
  sortNetsByVlan = builtins.sort (net1: net2:
    let
      vlan1 = config.site.net.${net1}.vlan;
      vlan2 = config.site.net.${net2}.vlan;
    in if vlan1 == null
      then true
      else if vlan2 == null
      then false
      else vlan1 < vlan2
  );
}
