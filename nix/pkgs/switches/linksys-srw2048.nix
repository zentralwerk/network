# https://www.crc.id.au/real-console-on-linksys-srw2024-switch/
{ hostName, config, hostConfig, sort, sortBy, sortNetsByVlan
, lib, expect, inetutils }:

''
  #! ${lib.getExe expect} -f

  spawn ${inetutils}/bin/telnet ${config.site.net.mgmt.hosts4.${hostName}}
  expect "Password:"
  send "admin\t${hostConfig.password}\r"

  # ^z
  send "\x1A"
  expect ">"
  send "lcli\r"

  expect "User Name:"
  send "admin\r"
  expect "Password:"
  send "${hostConfig.password}\r"

  expect "# "
  send "configure\r"
  expect "(config)# "
  send "hostname ${hostName}\r"
  expect "(config)# "
  send "port jumbo-frame\r"
  expect "(config)# "
  send "management vlan 4094\r"
  expect "(config)# "
  send "vlan database\r"
  expect "(config-vlan)# "

  ${lib.concatMapStrings (net:
    let
      netConfig = config.site.net.${net};
    in
      if netConfig.vlan != null
      then ''
        send "vlan ${toString netConfig.vlan}\r"
        expect "(config-vlan)#"
      ''
      else ""
  ) (sortNetsByVlan (builtins.attrNames config.site.net))}
  send "exit\r"
  expect "(config)#"

  ${lib.concatMapStrings (net:
    let
      netConfig = config.site.net.${net};
    in
      lib.optionalString (netConfig.vlan != null) ''
        send "interface vlan ${toString config.site.net.${net}.vlan}\r"
        expect "(config-if)#"
        send "name ${net}\r"
        expect "(config-if)#"
        send "exit\r"
        expect "(config)#"
      ''
  ) (sortNetsByVlan (builtins.attrNames config.site.net))}

  ${lib.concatMapStrings (name:
    let
      linkConfig = hostConfig.links.${name};
      netConfig = config.site.net.${name};
      isTrunk = linkConfig.trunk;
      isBond = builtins.length linkConfig.ports > 1 &&
        # not named like a net
        (!config.site.net ? ${name});
      vlans = lib.concatStringsSep "," (map toString (sort linkConfig.vlans));
      ports = lib.concatStringsSep "," linkConfig.ports;
    in
      if isTrunk && isBond
      then ''
        send "interface range ethernet ${ports}\r"
        expect "(config-if)#"
        send "switchport trunk allowed vlan remove all\r"
        expect "(config-if)#"
        send "channel-group ${linkConfig.group} mode auto\r"
        expect "(config-if)#"
        send "interface port-channel ${linkConfig.group}\r"
        expect "(config-if)#"
        send "exit\r"

        send "interface port-channel ${linkConfig.group}\r"
        expect "(config-if)#"
        send "switchport mode trunk\r"
        expect "(config-if)#"
        send "switchport trunk allowed vlan add ${vlans}\r"
        expect "(config-if)#"

        send "exit\r"
        expect "(config)#"
      ''
      else if isTrunk
      then lib.concatMapStrings (port: ''
        send "interface ethernet ${port}\r"
        expect "(config-if)#"
        send "no channel-group\r"
        expect "(config-if)#"
        send "switchport mode trunk\r"
        expect "(config-if)#"
        send "switchport trunk allowed vlan add ${vlans}\r"
        expect "(config-if)#"
        send "exit\r"
        expect "(config)#"
      '') linkConfig.ports
      else if isBond
      then
        if builtins.length linkConfig.vlans != 1
        then throw "Cannot configure non-trunked port with multiple vlans"
        else ''
          send "interface range ethernet ${ports}\r"
          expect "(config-if)#"
          send "channel-group ${linkConfig.group} mode auto\r"
          expect "(config-if)#"
          send "interface port-channel ${linkConfig.group}\r"
          expect "(config-if)#"
          send "exit\r"

          send "interface port-channel ${linkConfig.group}\r"
          expect "(config-if)#"
          send "switchport trunk allowed vlan remove all\r"
          expect "(config-if)#"
          send "switchport mode access\r"
          expect "(config-if)#"
          send "switchport access vlan ${toString (builtins.head linkConfig.vlans)}\r"
          expect "(config-if)#"

          send "exit\r"
          expect "(config)#"
        ''
      else lib.concatMapStrings (port: ''
        send "interface ethernet ${port}\r"
        expect "(config-if)#"
        send "no channel-group\r"
        expect "(config-if)#"
        send "switchport mode access\r"
        expect "(config-if)#"
        send "switchport access vlan ${toString netConfig.vlan}\r"
        expect "(config-if)#"
        send "exit\r"
        expect "(config)#"
      '') (sort linkConfig.ports)
  ) (sortBy (link: hostConfig.links.${link}.ports)
    (builtins.attrNames hostConfig.links)
  )}
''
