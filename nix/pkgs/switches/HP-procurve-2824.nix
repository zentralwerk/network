# http://ftp.hp.com/pub/networking/software/2600-2800-4100-6108-MgmtConfig-Oct2005-59906023.pdf
{ self, hostName, config, hostConfig, sort, sortBy, sortNetsByVlan
, lib, expect, inetutils, uucp }:

''
  #! ${lib.getExe expect} -f

  ${if hostConfig.firstboot
    then ''
      spawn ${uucp}/bin/cu -s 9600 -l /dev/ttyUSB0
    ''
    else ''
      spawn ${inetutils}/bin/telnet ${config.site.net.mgmt.hosts4.${hostName}}
      expect "Press any key to continue"
      send "\r"
      expect "assword: "
      send "${hostConfig.password}\r"
      expect "#"
    ''
   }
  send "\rconfigure terminal\r"
  expect "(config)# "

  send "hostname ${hostName}\r"
  expect "(config)# "
  send "max-vlans 256\r"
  expect "(config)# "
  send "snmp-server location \"${hostConfig.location}\"\r"
  expect "(config)# "
  send "snmp-server contact \"noc@zentralwerk.org\"\r"
  expect "(config)# "
  send "password manager\r"
  expect "New password for Manager: "
  send "${hostConfig.password}\r"
  expect "Please retype new password for Manager: "
  send "${hostConfig.password}\r"
  expect "(config)# "

  # TODO: ssh, password

  # Enable Logging
  send "logging ${config.site.net.mgmt.hosts4.logging}\r"
  expect "(config)# "
  send "logging facility local6\r"
  expect "(config)# "

  # TODO ntp
  # timesync sntp
  # ip timep manual {#ntp#} interval 10

  ${lib.concatMapStrings (net:
    let
      netConfig = config.site.net.${net};
      vlan = toString netConfig.vlan;
      inherit (config.site.net.${net}) hosts4;
      hostAddr4 = hosts4.${hostName};
      prefixLength = lib.elemAt (
        builtins.split "/" netConfig.subnet4
      ) 2;
      netmask = self.lib.netmasks.${prefixLength};
    in
      lib.optionalString (netConfig.vlan != null) ''
        send "vlan ${vlan}\r"
        expect "(vlan-${vlan})#"

        send "name ${net}\r"
        expect "(vlan-${vlan})#"

        send "jumbo\r"
        expect "(vlan-${vlan})#"

        ${lib.optionalString (hosts4 ? ${hostName}) ''
          # Actually only used for mgmt_vlan, switches are not routers
          send "ip address ${hostAddr4} ${netmask}\r"
          expect "(vlan-${vlan})#"
        ''}

        send "exit\r"
        expect "(config)# "

        ${if net == "mgmt"
          then ''
            send "management-vlan ${vlan}\r"
            expect "(config)# "
          '' else ''
            # If not mgmt, reset all VLAN mappings
            send "no vlan ${vlan} tagged all\r"
            expect "(config)# "
            send "no vlan ${vlan} untagged all\r"
            expect "(config)# "
          ''}
      ''
    ) (sortNetsByVlan (builtins.attrNames config.site.net))
  }

  ${lib.concatMapStrings (name:
    let
      linkConfig = hostConfig.links.${name};
      isAccess = config.site.net ? ${name};
      netConfig = config.site.net.${name};
      isTrunk = !isAccess;
      port0 = builtins.head linkConfig.ports;
      isBond =
        builtins.length linkConfig.ports > 1
        ||
        lib.hasInfix "-" port0
        ||
        lib.hasInfix "," port0;
      ports = lib.concatStringsSep "," linkConfig.ports;
    in
      if isTrunk && isBond
      then ''
        send "interface ${ports} lacp active\r"
        expect "(config)# "
        send "trunk ${ports} trk${linkConfig.group} lacp\r"
        expect "(config)# "

        ${lib.concatMapStrings (vlan: ''
          send "vlan ${toString vlan} tagged trk${linkConfig.group}\r"
          expect "(config)# "
        '') (sort linkConfig.vlans)}
      ''

      else if isTrunk
      then ''
        send "no trunk ${ports}\r"
        expect "(config)# "
        send "no interface ${ports} lacp\r"
        expect "(config)# "

        ${lib.concatMapStrings (vlan: ''
          send "vlan ${toString vlan} tagged ${ports}\r"
          expect "(config)# "
        '') (sort linkConfig.vlans)}
      ''

      else ''
        send "no trunk ${ports}\r"
        expect "(config)# "

        send "vlan ${toString netConfig.vlan} untagged ${ports}\r"
        expect "(config)# "
      ''
  ) (sortBy (link: hostConfig.links.${link}.ports)
    (builtins.attrNames hostConfig.links)
  )}

  send "exit\r"
  expect "${hostName}# "

  send "write memory\r"
  expect "${hostName}# "
  send "exit\r"
  expect "${hostName}> "
  send "exit\r"
  expect "Do you want to log out "
  expect "y/n]? "
  send "y"
''
