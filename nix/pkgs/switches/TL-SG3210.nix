# http://static.tp-link.com/res/down/doc/TL-SG3210(UN)_V2.0_CLI_.pdf
{ self, hostName, config, hostConfig, sort, sortBy, sortNetsByVlan
, lib, expect, inetutils }:
''
  #! ${lib.getExe expect} -f

  spawn ${inetutils}/bin/telnet ${config.site.net.mgmt.hosts4.${hostName}}
  expect "Password:"
  send "${hostConfig.password}\r"
  expect ">"
  send "\r"
  expect ">"
  send "enable\r"
  expect "Password:"
  send "${hostConfig.password}\r"
  expect "#"
  send "configure\r"
  expect "(config)#"

  send "enable secret 0 ${hostConfig.password}\r"
  expect "(config)#"
  #send "enable password 0 ${hostConfig.password}\r"
  #expect "(config)#"
  send "service password-encryption\r"
  expect "(config)#"
  send "user name admin privilege admin secret 0 ${hostConfig.password}\r"
  expect "(config)#"

  send "hostname \"${hostName}\"\r"
  expect "(config)#"
  send "location \"${hostConfig.location}\"\r"
  expect "(config)#"

  send "logging host index 1 ${config.site.net.mgmt.hosts4.logging} 6\r"
  expect "(config)#"

  send "ip management-vlan ${toString config.site.net.mgmt.vlan}\r"
  expect "(config)#"
  send "ip ssh server\r"
  expect "(config)#"
  send "telnet enable\r"
  expect "(config)#"
  send "line vty 0 15\r"
  expect "(config-line)#"
  send "password 0 ${hostConfig.password}\r"
  expect "(config-line)#"
  send "exit\r"
  expect "(config)#"

  ${lib.concatMapStrings (net:
    let
      netConfig = config.site.net.${net};
      vlan = toString netConfig.vlan;
      inherit (config.site.net.${net}) hosts4;
      hostAddr4 = hosts4.${hostName};
      prefixLength = lib.elemAt (
        builtins.split "/" netConfig.subnet4
      ) 2;
      netmask = self.lib.netmasks.${prefixLength};
    in
      lib.optionalString (netConfig.vlan != null) ''
        ${lib.optionalString (net != "mgmt") ''
          send "vlan ${vlan}\r"
          expect "(config-vlan)#"
          send "name \"${net}\"\r"
          expect "(config-vlan)#"
          send "exit\r"
          expect "(config)#"
        ''}

        send "interface vlan ${vlan}\r"
        expect "(config-if)#"
        ${lib.optionalString (hosts4 ? ${hostName}) ''
          # Actually only used for mgmt_vlan, switches are not routers
          send "ip address ${hostAddr4} ${netmask}\r"
          expect "(config-if)#"
        ''}
        send "exit\r"
        expect "(config)#"
      ''
  ) (sortNetsByVlan (builtins.attrNames config.site.net))}

  ${lib.concatMapStrings (name:
    let
      linkConfig = hostConfig.links.${name};
      isAccess = config.site.net ? ${name};
      netConfig = config.site.net.${name};
      isTrunk = !isAccess;
      port0 = builtins.head linkConfig.ports;
      isBond =
        builtins.length linkConfig.ports > 1
        ||
        lib.hasInfix "-" port0;
      vlans = lib.concatStringsSep "," (map toString (sort linkConfig.vlans));
      ports = lib.concatMapStringsSep "," (port:
        "1/0/${port}"
      ) linkConfig.ports;
    in
      if isTrunk
      then ''
        send "interface range gigabitEthernet ${ports}\r"
        expect "(config-if-range)#"
        send "switchport mode trunk\r"
        expect "(config-if-range)#"
        ${if isBond
          then ''
            send "channel-group ${linkConfig.group} mode active\r"
            expect "(config-if-range)#"
            #send "port-channel load-balance src-dst-ip\r"
            #expect "(config-if-range)#"
          '' else ''
            send "no channel-group\r"
            expect "(config-if-range)#"
          ''}
        send "switchport trunk allowed vlan ${vlans}\r"
        expect "(config-if-range)#"
        send "exit\r"
        expect "(config)#"
      '' else ''
        send "interface range gigabitEthernet ${ports}\r"
        expect "(config-if-range)#"
        send "switchport mode access\r"
        expect "(config-if-range)#"
        send "switchport access vlan ${toString netConfig.vlan}\r"
        expect "(config-if-range)#"
        send "exit\r"
        expect "(config)#"
      ''
  ) (sortBy (link: hostConfig.links.${link}.ports)
    (builtins.attrNames hostConfig.links)
  )}

  send "exit\r"
  expect "#"
  send "copy running-config startup-config\r"
  expect "#"
  send "exit\r"
  expect ">"
  send "exit\r"
''
