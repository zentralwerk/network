{ self, lib, pkgs }:

let
  inherit (self.lib) config;
in
builtins.mapAttrs (hostName: hostConfig@{ model, ... }:
  pkgs.writeScriptBin "${hostName}.sh" (
    let
      args = {
        inherit config hostConfig hostName self;
      };
    in pkgs.callPackage (./switches + "/${model}.nix") (
      args // (import ./switches/shared.nix { inherit config lib; })
    )
  )
) (
  lib.filterAttrs (_: { role, model, ... }:
    role == "switch" && model != "dumb"
  ) config.site.hosts
)
