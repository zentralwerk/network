{ self, lib, pkgs }:
let
  config = self.lib.config;
in
pkgs.writeText "gateway-report.md" ''
  # Gateway Report

  ${lib.concatMapStrings (net:
    let
      netConfig = config.site.net.${net};

      routers4 = builtins.filter (hostName:
        config.site.hosts ? ${hostName} &&
        config.site.hosts.${hostName}.isRouter
      ) (
        builtins.attrNames netConfig.hosts4
      );

      routers6 = builtins.filter (hostName:
        config.site.hosts ? ${hostName} &&
        config.site.hosts.${hostName}.isRouter
      ) (lib.unique (
          builtins.concatMap builtins.attrNames (builtins.attrValues netConfig.hosts6)
      ));

      upstreamAt = l: n:
        if n < builtins.length l
        then
          let
            hostName = builtins.elemAt l n;
            hostConfig = config.site.hosts.${hostName};
            providers =
              map ({ upstream, ... }: upstream.provider) (
                builtins.filter ({ upstream, ... }:
                  upstream.provider or null != null
                ) (builtins.attrValues hostConfig.interfaces)
              );
          in
            "${hostName}${lib.optionalString (providers != []) " (${lib.concatStringsSep ", " providers})"}"
        else "";

    in
      lib.optionalString (net != "core" && (routers4 != [] || routers6 != [])) ''
        ## Network ${net}

        ${lib.optionalString (routers4 != []) ''
          ### IPv4 `${netConfig.subnet4}`

          | Address | Name | Primary Uplink | Fallback Uplink |
          |-|-|-|-|
          ${lib.concatMapStrings (hostName:
            let
              hostConfig = config.site.hosts.${hostName};
              isDhcpDefault = hostName == netConfig.dhcp.router or null;
              emphasize = s:
                if isDhcpDefault
                then "**${s}**"
                else s;
              upstream4a = upstreamAt hostConfig.ospf.allowedUpstreams 0;
              upstream4b = upstreamAt hostConfig.ospf.allowedUpstreams 1;
            in ''
              |${emphasize netConfig.hosts4.${hostName}}|${emphasize hostName}|${upstream4a}|${upstream4b}|
            ''
          ) (lib.naturalSort routers4)}
        ''}

        ${lib.optionalString (routers6 != {}) ''
          ### IPv6 ${lib.concatStringsSep " " (
            map (s: "`${s}`") (
              builtins.attrValues netConfig.subnets6
            )
          )}

          | Address | Name | Primary Uplink | Fallback Uplink |
          |---------|------|----------------|-----------------|
          ${lib.concatMapStrings (hostName:
            let
              hostConfig = config.site.hosts.${hostName};
              isIpv6Router = hostName == netConfig.ipv6Router;
              emphasize = s:
                if isIpv6Router
                then "**${s}**"
                else s;
              upstream6a = upstreamAt hostConfig.ospf.allowedUpstreams6 0;
              upstream6b = upstreamAt hostConfig.ospf.allowedUpstreams6 1;
            in ''
              |${
                emphasize (
                  lib.concatMapStringsSep " " (ctx:
                    "`${netConfig.hosts6.${ctx}.${hostName}}`"
                  ) (builtins.filter (ctx:
                    netConfig.hosts6.${ctx} ? ${hostName}
                  ) (builtins.attrNames netConfig.hosts6))
                )
              }|${emphasize hostName}|${upstream6a}|${upstream6b}|
            ''
          ) (lib.naturalSort routers6)}
        ''}

      ''
  ) (lib.naturalSort (builtins.attrNames config.site.net))}
''
