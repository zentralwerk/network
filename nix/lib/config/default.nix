{ self, lib, pkgs }:

let
  result = lib.evalModules {
    modules = [
      (
        { lib, ... }:
        with lib;
        {
          config._module.args = {
            inherit self pkgs;
          };
          options.assertions = mkOption {
            type = with types; listOf unspecified;
            internal = true;
          };
          options.warnings = mkOption {
            type = types.listOf types.str;
            default = [];
            internal = true;
          };
        }
      )
      ./options.nix
      # TODO: make configurable
      ../../../config
    ];
  };

  inherit (result) config;

  warn = result:
    if builtins.length config.warnings > 0
    then builtins.trace ''
      Warnings:

      ${self.lib.concatStringsSep "\n" config.warnings}
    '' result
    else result;

  error = result:
    let
      failed = builtins.filter ({ assertion, ... }:
        !assertion
      ) config.assertions;
    in
      if failed != []
      then throw ''
        Errors:

        ${self.lib.concatMapStringsSep "\n" ({ message, ... }: message) failed}
      ''
      else result;

in warn (error ({
  inherit (result) options;

  config = builtins.removeAttrs config [ "assertions" "warnings" ];
}))
