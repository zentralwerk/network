{ lib }:

lib.mkOption {
  type = with lib.types; listOf (submodule {
    options = {
      name = lib.mkOption {
        description = "DNS label";
        type = str;
      };
      type = lib.mkOption {
        type = enum  [ "A" "AAAA" "CNAME" "DNSKEY" "DS" "MX" "NS" "PTR" "SRV" "TXT" ];
      };
      data = lib.mkOption {
        type = oneOf [ str (attrsOf (oneOf [ int str (attrsOf bool) ])) ];
      };
    };
  });
  default = [ ];
}
