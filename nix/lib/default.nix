{ self, lib, openwrt, pkgs }:

rec {
  inherit (import ./config { inherit self lib pkgs; }) config;

  netmasks = import ./netmasks.nix;

  subnet = import ./subnet { inherit pkgs; };

  dns = import ./dns.nix { inherit config lib; };

  openwrtModels = import ./openwrt-models.nix { inherit self lib openwrt; };

  getOpenwrtModel = wantedModel:
    let
      models =
        builtins.filter ({ models, ... }:
          self.lib.any ({ model, vendor, ... }:
            model == wantedModel ||
            "${vendor}_${model}" == wantedModel
          ) models
        ) openwrtModels;
      result =
        builtins.foldl' (result: { data, ... }:
          self.lib.recursiveUpdate result data
        ) {} models;
    in
      if builtins.length models > 0
      then result
      else builtins.trace "No data found for OpenWRT model ${wantedModel}"
        {};
}
