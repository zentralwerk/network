{ config, lib }:

rec {
  # TODO: move to config
  ns = "dns.serv.zentralwerk.org";
  internalNS = [ ns ];
  # public servers (slaves)
  # TODO: move to config
  publicNS = [
    "ns.c3d2.de"
    "ns.spaceboyz.net"
    "ns1.supersandro.de"
  ];

  publicIPv4 = config.site.hosts.upstream4.interfaces.up4-pppoe.upstream.staticIpv4Address;

  # TODO: move to config
  dynamicReverseZones4 = [
    "72.20.172.in-addr.arpa"
    "73.20.172.in-addr.arpa"
    "74.20.172.in-addr.arpa"
    "75.20.172.in-addr.arpa"
    "76.20.172.in-addr.arpa"
    "77.20.172.in-addr.arpa"
    "78.20.172.in-addr.arpa"
    "79.20.172.in-addr.arpa"
    "99.22.172.in-addr.arpa"
    "22.10.in-addr.arpa"
  ];
  dynamicReverseZones6 = [
    "2.0.0.0.c.2.0.8.1.8.0.0.a.2.ip6.arpa"
    "4.1.b.a.c.a.2.8.3.5.f.0.a.2.ip6.arpa"
    "5.0.2.d.3.c.2.4.0.0.3.2.d.f.ip6.arpa"
  ];

  mapI = start: end: f:
    if start >= end
    then []
    else [ (f start) ] ++ mapI (start + 1) end f;
  isRfc1918Reverse = reverse:
    builtins.any (suffix: lib.hasSuffix suffix reverse) ([
      # TODO: move to config
      "10.in-addr.arpa"
      "168.192.in-addr.arpa"
    ] ++ mapI 0 32 (i:
      "${toString (16 + i)}.172.in-addr.arpa"
    ));

  localZones =
    let
      # ip6.arpa aggregation size in CIDR bits
      reverseZone6Size = 56;

      hosts4Records = hosts4:
        builtins.attrValues (
          builtins.mapAttrs (name: addr: {
            inherit name;
            type = "A";
            data = addr;
          }) hosts4
        );
      hosts6Records = hosts6:
        builtins.attrValues (
          builtins.mapAttrs (name: addr: {
            inherit name;
            type = "AAAA";
            data = addr;
          }) hosts6
        );

      # generate zones only for nets with hosts
      namedNets = lib.filterAttrs (_name: { hosts4, hosts6, dynamicDomain, ... }:
        hosts4 != {} ||
        hosts6 != {} ||
        dynamicDomain
      ) config.site.net;

      # converts an IPv4 address to its reverse DNS form
      ipv4ToReverse = ipv4:
        builtins.concatStringsSep "." (
          lib.reverseList (
            builtins.filter builtins.isString (
              builtins.split "\\." ipv4
            )
          )
        ) + ".in-addr.arpa";

      # `{ "1,0.0.127.in-addr.arpa" = "lo.core.zentralwerk.dn42"; }`
      reverseHosts4 = builtins.foldl' (result: { hosts4, domainName, ... }:
        builtins.foldl' (result: host: result // {
          "${ipv4ToReverse hosts4.${host}}" = "${host}.${domainName}";
        }) result (builtins.attrNames hosts4)
      ) {} (builtins.attrValues namedNets);

      # `[ "0.0.127.in-addr.arpa" ]`
      reverseZones4 = lib.unique (
        builtins.attrNames (
          builtins.foldl' (result: rname:
            let
              zone = builtins.head (
                builtins.match "[[:digit:]]+\\.(.+)" rname
              );
            in result // {
              "${zone}" = true;
            }
          ) {} (builtins.attrNames reverseHosts4)
        ) ++ dynamicReverseZones4
      );

      # turns `::` into `0000:0000:0000:0000:0000:0000:0000:0000`
      expandIpv6 = ipv6:
        if lib.hasPrefix "::" ipv6
        then expandIpv6 "0${ipv6}"

        else if lib.hasSuffix "::" ipv6
        then expandIpv6 "${ipv6}0"

        else let
          words = builtins.filter builtins.isString (
            builtins.split ":" ipv6
          );
          fillWordCount = 8 - builtins.length words;
          fillWords = n:
            if n >= 0
            then [ "0000" ] ++ fillWords (n - 1)
            else [];
          words' = builtins.concatMap (word:
            if word == ""
            then fillWords fillWordCount
            else [ word ]
          ) words;
          leftPad = padding: target: s:
            if builtins.stringLength s < target
            then leftPad padding target "${padding}${s}"
            else s;
          words'' = map (leftPad "0" 4) words';
        in
          builtins.concatStringsSep ":" words'';

      # turns `::1` into `1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa`
      ipv6ToReverse = ipv6:
        builtins.concatStringsSep "." (
          lib.reverseList (
            lib.stringToCharacters (
              builtins.replaceStrings [":"] [""] (expandIpv6 ipv6)
            )
          )
        ) + ".ip6.arpa";

      # `{ dn42 = { "...ip6.arpa" = "lo.core.zentralwerk.dn42"; }; }`
      reverseHosts6 = builtins.foldl' (result: net: lib.recursiveUpdate result (
        builtins.mapAttrs (ctx: hosts:
          builtins.foldl' (result: host:
            let
              domain =
                if ctx == "dn42"
                then "${net}.zentralwerk.dn42"
                else namedNets.${net}.domainName;
            in
              lib.recursiveUpdate result {
                "${ipv6ToReverse hosts.${host}}" = "${host}.${domain}";
              }
          ) {} (builtins.attrNames hosts)
        ) namedNets.${net}.hosts6
      )) {} (builtins.attrNames namedNets);

      # `{ dn42 = [ "....ip6.arpa" ]; }`
      reverseZones6 = builtins.mapAttrs (_ctx: reverseHosts6ctx:
        builtins.attrNames (
          builtins.foldl' (result: rname:
              result // {
                "${builtins.substring ((128 - reverseZone6Size) / 2) (72 - ((128 - reverseZone6Size) / 2)) rname}" = true;
              }) {} (builtins.attrNames reverseHosts6ctx)
          )
      ) reverseHosts6;

      dnskeyDefault = {
        flags = {
          secureEntryPoint = true;
          zoneSigningKey = true;
        };
        algorithm = 13;
      };

    # TODO: use dns.nix directly and then inject c3d2-dns.packages.${pkgs.stdenv.system}.template.mail here
    # TODO: move to config
    in [ {
      name = "zentralwerk.org";
      ns = publicNS;
      records = [ {
        name = "@";
        type = "A";
        data = publicIPv4;
      } {
        name = "www";
        type = "A";
        data = publicIPv4;
      } {
        name = "@";
        type = "AAAA";
        data = config.site.net.serv.hosts6.up4.public-access-proxy;
      } {
        name = "www";
        type = "AAAA";
        data = config.site.net.serv.hosts6.up4.public-access-proxy;
      } {
        name = "c3d2";
        type = "DS";
        data = {
          keyTag = 30381;
          algorithm = 13;
          digestType = 2;
          digest = "3a90b6dbff35057424f4dacac0bfe893a9d16bb2011fa67c95c0f734ae0f2e1d";
        };
      } {
        name = "c3d2";
        type = "DS";
        data = {
          keyTag = 30381;
          algorithm = 13;
          digestType = 4;
          digest = "28342672a0ee40bca098749e7422499b7b18f1a1c44338c8432ddeaef9069f72dd9b814526eb7078ccf673db0af6d5c4";
        };
      } {
        name = "c3d2";
        type = "DNSKEY";
        data = dnskeyDefault // {
          publicKey = "y9B7fKNg0HkXPHxvEOyu/Mi96L9z2zj0EwcFmxg8BKNfwKOnhu+BFw+u8+6mlLSpFKk+1n7d+2a5NE8EYsZ5xw==";
        };
      } {
        name = "cluster";
        type = "DS";
        data = {
          keyTag = 51965;
          algorithm = 13;
          digestType = 2;
          digest = "006586b09c6e5ef7d0484bad45499dcdc8a7f4295095359cd5a91355b2995f82";
        };
      } {
        name = "cluster";
        type = "DS";
        data = {
          keyTag = 51965;
          algorithm = 13;
          digestType = 4;
          digest = "44eea172937acc13c27d01dc33371670c0da6a93f6a9144f06128b58f7f8a8fde3335e0b5fc93ba3a423a04bce742f94";
        };
      } {
        name = "cluster";
        type = "DNSKEY";
        data = dnskeyDefault // {
          publicKey = "q1EjxC+MertLIDlDWYEcJbgQvJD+INTpOz9QFXlfI5gLBjhL9BZ0qdOLBlEs/wSNXaSrQkDBkFBq8FA2QXVVgA==";
        };
      } {
        name = "flpk";
        type = "DS";
        data = {
          keyTag = 19614;
          algorithm = 13;
          digestType = 2;
          digest = "43df86e5e9962c7c9ec135e0979f731468b229b22006ff2832e9498fac2922a4";
        };
      } {
        name = "flpk";
        type = "DS";
        data = {
          keyTag = 19614;
          algorithm = 13;
          digestType = 4;
          digest = "24b5ce69c7fb83821baf9cd49d2f060a53f5ad11a0c286cab90486772399fcb025205d3ec19b6e71b0a51d9d95f5b80a";
        };
      } {
        name = "flpk";
        type = "DNSKEY";
        data = dnskeyDefault // {
          publicKey = "/H3+8KHvJ+ytQeuVdir/oz+vH5V9465ZLpBOUZ3fTSSyJJ46Vhn3dnhAVY5AKhpvqAqZEsnRbcg9FDdw0vj6qw==";
        };
      } {
        name = "serv";
        type = "DS";
        data = {
          keyTag = 56493;
          algorithm = 13;
          digestType = 2;
          digest = "94820fca41e86e9d2558ff3da364dcd97f32e0cf29d58437f80f90314789814f";
        };
      } {
        name = "serv";
        type = "DS";
        data = {
          keyTag = 56493;
          algorithm = 13;
          digestType = 4;
          digest = "ad72ed4b8d1764b832fad550f16d6de6963059a2e67e7d111c665e17391d254b8c901cd0c16d493087409f3841d62ecf";
        };
      } {
        name = "serv";
        type = "DNSKEY";
        data = dnskeyDefault // {
          publicKey = "R09CiPnudgRJ5pIDDPl+fxJLDTbQrTG/A/Puw1gCIwMgx7Y7Ik9zIDxdndR+b7BIP81U8LCL6ZiNVor70Hdavw==";
        };
      } {
        name = "default._domainkey";
        type = "TXT";
        data = "p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA35GZw+zxNUV8on8Ili6Y1JdyK55OLMkGj2sJtMHGMn9szk9vMHRUyQ4OBlayCEPKqVzi9gJw4QYxNzP8afmvpjSbbCveLC0iWv9roMNXrmvJsURKOcwlfx39ju3uT5pbSY8Di25ug/gF1Gkiwb2ve1mG7/ySiPWFy5Ka52qSVc97clQPygHm+lDORa+pbOIMQJPPcxI8wtOxo+/7OOY30w0acferduQmZYaC2y5SrDtvDL+pYI0jXdSyMA8gw0ovaSx2AdSf/EzyaQvSKMP7R0XnOBxaNOVJMvSzGfRba9aZEd1Ms6QUEXUHPikZcQMPY4i1uVlhbEbGOH1YP3LgHwIDAQAB";
      } ]
      ++
      builtins.concatMap (net:
        if lib.hasSuffix ".zentralwerk.org" config.site.net.${net}.domainName
        then map (ns: {
          name = net;
          type = "NS";
          data = "${ns}.";
        }) publicNS
        else []
      ) (builtins.attrNames config.site.net);
    } {
      name = "zentralwerk.dn42";
      ns = internalNS;
      records = [ ];
    } {
      name = "dyn.zentralwerk.org";
      ns = publicNS;
      records = [ {
        name = "upstream4";
        type = "A";
        data = publicIPv4;
      } ];
    } ]
    ++
    builtins.concatLists (
      builtins.attrValues (
        builtins.mapAttrs (net: { dynamicDomain, hosts4, hosts6, extraRecords, ... }: [
          {
            name = "${net}.zentralwerk.dn42";
            ns = internalNS;
            records =
              lib.optionals (hosts6 ? dn42) (hosts6Records hosts6.dn42) ++
              extraRecords;
          }
          {
            name = "${net}.zentralwerk.org";
            ns = publicNS;
            records =
              hosts4Records hosts4 ++
              lib.optionals (hosts6 ? up4) (hosts6Records hosts6.up4) ++
              lib.optionals (hosts6 ? flpk) (hosts6Records hosts6.flpk) ++
              extraRecords;
            dynamic = dynamicDomain;
          }
        ]) namedNets
      )
    )
    ++
    map (zone: {
      name = zone;
      ns =
        if isRfc1918Reverse zone
        then internalNS
        else publicNS;
      records =
        map (reverse: {
          name = builtins.head (
            builtins.match "([[:digit:]]+)\\..*" reverse
          );
          type = "PTR";
          data = "${reverseHosts4.${reverse}}.";
        }) (
          builtins.filter (lib.hasSuffix ".${zone}")
            (builtins.attrNames reverseHosts4)
        );
      dynamic = builtins.elem zone dynamicReverseZones4;
    }) reverseZones4
    ++
    builtins.concatMap (ctx:
      map (zone: {
        name = zone;
        ns =
          if ctx == "dn42"
          then internalNS
          else publicNS;
        records =
          map (reverse: {
            name = builtins.substring 0 ((128 - reverseZone6Size) / 2 - 1) reverse;
            type = "PTR";
            data = "${reverseHosts6.${ctx}.${reverse}}.";
          }) (
            builtins.filter (lib.hasSuffix ".${zone}")
              (builtins.attrNames reverseHosts6.${ctx})
          );
        dynamic = builtins.elem zone dynamicReverseZones6;
      }) reverseZones6.${ctx}
    ) (builtins.attrNames reverseZones6);
}
