let
  pow2 = n:
    if n <= 0
    then 1
    else 2 * (pow2 (n - 1));
  octet = n:
    if n == 0
    then 0
    else (pow2 (8 - n)) + (octet (n - 1));

  netmask = len:
    builtins.concatStringsSep "." (
      map (byte:
        toString (
          if 8 * (byte + 1) <= len
          then 255
          else if 8 * byte <= len
          then octet (len - (8 * byte))
          else 0
        )
      ) [0 1 2 3]
    );
  
  keys = n:
    if n < 33
    then [n] ++ keys (n + 1)
    else [];
in
builtins.foldl' (netmasks: n:
  netmasks // {
    "${toString n}" = netmask n;
  }
) {} (keys 0)
