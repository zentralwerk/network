{ lib, pkgs }:

{
  # List of all IP addresses in a subnet
  range = subnet:
    import (
      pkgs.runCommandLocal "subnet-${subnet}.nix" {
        nativeBuildInputs = [ pkgs.nmap ];
      } ''
        (
          echo "["
          for i in $(nmap -sL ${lib.optionalString (builtins.match ".*:.*" subnet != null) "-6"} ${subnet} -n -oG - | grep Status: | cut -d " " -f 2); do
            echo \"$i\"
          done
          echo "]"
        ) > $out
      ''
    );
}
