{ hostName, self, config, lib, pkgs, ... }:

let
  hostRole = config.site.hosts.${hostName}.role;
  networkPort = 25826;

  upstreamTypesDb = pkgs.stdenv.mkDerivation {
    name = "types.db";
    src = config.services.collectd.package.src;
    installPhase = "cp src/types.db $out";
  };

  customTypesDb = builtins.toFile "types.db" ''
    stations                value:GAUGE:0:U
  '';

  inherit (config.services.collectd) user;

  execUser =
    if user == "root"
    then "nobody"
    else user;

  hasStarlink =
    builtins.any ({ upstream, ... }:
      if upstream == null
      then false
      else upstream.provider == "starlink"
    ) (builtins.attrValues config.site.hosts.${hostName}.interfaces);
in
{
  services.collectd = lib.mkMerge [ {
    enable = true;
    buildMinimalPackage = true;

    extraConfig = ''
      TypesDB "${upstreamTypesDb}" "${customTypesDb}"
    '';

    plugins = {
      interface = "";
      conntrack = "";
    };
  }
  # TODO: move to stats container
  (lib.optionalAttrs (hostName == "stats") {
    plugins.network = ''
      Listen "::" "${toString networkPort}"
      Forward true
      Server "${config.site.net.serv.hosts4.spaceapi}" "${toString networkPort}"
      Server "${config.site.net.serv.hosts4.grafana}" "${toString networkPort}"
      Server "${config.site.net.serv.hosts4.prometheus}" "${toString networkPort}"
    '';
    plugins.mqtt = ''
      <Publish "broker">
        Host "${config.site.mqttServer.host}"
        User "${config.site.mqttServer.user}"
        Password "${config.site.mqttServer.password}"
        ClientId "collectd-${hostName}"
      </Publish>
    '';
  }) (lib.optionalAttrs (hostName != "stats") {
    plugins.network = ''
      Server "${config.site.net.serv.hosts6.dn42.stats}" "${toString networkPort}"
    '';
  }) (lib.optionalAttrs (hostRole == "server") {
    plugins = {
      irq = "";
      cpu = "";
      load = "";
      memory = "";
      swap = "";
      entropy = "";
      disk = "";
      df = "";
      processes = "";
      hddtemp = "";
      sensors = "";
      thermal = "";
    };
  }) (let
    monitoringTargets = config.site.hosts.${hostName}.services.monitoring.target;
  in lib.optionalAttrs (monitoringTargets != []) {
    user = "root";
    plugins.ping = ''
      Interval 10
      Timeout 1
      Size 0
      MaxMissed 100
    '' + lib.concatMapStringsSep "\n" (target: ''Host "${target}"'') monitoringTargets;
  }) (lib.optionalAttrs config.services.kea.dhcp4.enable {
    plugins.exec =
      let
        maxTimeout = builtins.foldl' (maxTimeout: net:
          let
            dhcpConf = config.site.net.${net}.dhcp;
          in
            if dhcpConf != null &&
              dhcpConf.server == hostName &&
              dhcpConf.time > maxTimeout
            then dhcpConf.time
            else maxTimeout
        ) 180 (builtins.attrNames config.site.net);
      in ''
        Exec "${execUser}" "/run/wrappers/bin/collectd-dhcpcount" "${toString maxTimeout}"
      '';
  }) (lib.optionalAttrs hasStarlink {
    plugins.exec = ''
      Exec "nobody" "${lib.getExe self.packages.${pkgs.targetPlatform.system}.starlink-stats}" "192.168.100.1:9200"
    '';
  }) ];


  systemd.services.collectd = lib.mkIf config.services.kea.dhcp4.enable {
    after = [ "kea-dhcp4-server.service" ];
  };

  security.wrappers = lib.mkIf config.services.kea.dhcp4.enable {
    collectd-dhcpcount =
      let
        dhcpcount = pkgs.runCommand "dhcpcount" {
          src = ./dhcpcount.rb;
          buildInputs = [ pkgs.ruby ];
          meta.mainProgram = "dhcpcount";
        } /* ruby */ ''
          cp $src dhcpcount.rb
          patchShebangs dhcpcount.rb
          mkdir -p $out/bin
          cp dhcpcount.rb $out/bin/dhcpcount
        '';
      in {
        setuid = true;
        owner = "root";
        group = "root";
        source = lib.getExe dhcpcount;
      };
  };
}
