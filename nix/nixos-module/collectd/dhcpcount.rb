#!/usr/bin/env ruby

require 'csv'

INTERVAL = 60
TIMEOUT = ARGV[0].to_i  # TODO: now unused
hostname = CSV::readlines("/proc/sys/kernel/hostname").join.strip
STDOUT.sync = true

loop do
  seen = {}
  count = 0
  now = Time.now.to_i

  CSV::readlines("/var/lib/kea/kea-leases4.csv", headers: true).each do |rec|
    h = rec.to_h
    addr = h["hwaddr"]
    next unless addr
    last = h["expire"].to_i
    elapsed = now - last
    next if elapsed >= TIMEOUT

    unless seen[addr]
      count += 1
      seen[addr] = true
    end
  end
  puts "PUTVAL \"#{hostname}/exec-dhcpd/current_sessions-leases\" interval=#{INTERVAL} N:#{count}"

  sleep INTERVAL
end
