# Pulls together NixOS configuration modules according to the
# name/role of the host to be built.
{ hostName, lib, ... }:

let
  inherit (lib) optionals;

  hostConfig = lib.config.site.hosts.${hostName};
in {
  inherit (lib.config) site;

  imports = [
    ../lib/config/options.nix
    ./defaults.nix
    ./network.nix
    ./firewall.nix
    ./collectd
  ] ++
  optionals (hostConfig.role == "server") [
    ./server/default.nix
  ] ++
  optionals (hostConfig.role == "container") [
    ./container/lxc-config.nix
    ./container/defaults.nix
    ./container/dhcp-server.nix
    ./container/wireguard.nix
    ./container/dns.nix
    ./container/dnscache.nix
  ] ++
  optionals lib.config.site.hosts.${hostName}.isRouter [
    ./container/bird.nix
  ] ++
  optionals (
    builtins.match "upstream.*" hostName != null ||
    hostName == "flpk-gw"
  ) [
    ./container/upstream.nix
    ./container/pppoe.nix
  ] ++
  optionals (hostName == "mgmt-gw") [
    ./container/mgmt-gw.nix
  ] ++
  optionals (hostName == "vpn-gw") [
    ./container/vpn.nix
  ];
}
