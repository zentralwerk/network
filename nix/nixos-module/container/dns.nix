{ c3d2-dns, config, dns-nix, hostName, lib, pkgs, self, ... }:

let
  serial = builtins.substring 0 10 self.lastModifiedDate;

  generateZoneFile = let
    util = dns-nix.util.${pkgs.system};
  in { name, ns, records, ... }: (util.writeZone name (lib.recursiveUpdate
  # TODO: move to nix/lib/dns.nix
  (lib.optionalAttrs (name == "zentralwerk.org") c3d2-dns.packages.${pkgs.stdenv.system}.template.mail) {
    #
    # When changing the defaults, make sure to delete the copied zones from /var/lib/knot/zones
    # or otherwise they are never updated
    #
    useOrigin = true;
    TTL = 60*60;
    SOA = {
      nameServer = "${lib.dns.ns}.";
      adminEmail = config.site.data.contact;
      serial = lib.toInt serial;
      refresh = 1*60*60;
      retry = 5*60;
      expire = 2*60*60;
      minimum = 1*60;
    };
    NS = map (a: a+".") ns;
    subdomains = let
      toAttrs = map ({ name, type, data }: {
        ${name}.${type} = [ data ];
      });
      combineZones = lib.foldAttrs (item: acc: [item] ++ acc) [];
      combineRecords = lib.mapAttrs (zone: content: lib.zipAttrsWith (record: data: lib.concatLists data) content);
    in combineRecords (combineZones (toAttrs records));
  })).overrideAttrs (_: {
    checkPhase = ''
      echo "Checking $target ..."
      # we must provide the zone name as kzonecheck cannot always infer it from the file name
      ${pkgs.knot-dns}/bin/kzonecheck --origin ${name}. "$target"
    '';
  });
in
{
  options =
    with lib;
    let
      zoneOpts = {
        name = mkOption {
          description = "DNS FQDN w/o trailing dot";
          type = types.str;
        };
        ns = mkOption {
          type = with types; listOf str;
        };
        records = import ../../lib/config/record-opts.nix { inherit lib; };
        dynamic = mkOption {
          type = types.bool;
          default = false;
        };
      };

    in {
      site.dns.localZones = mkOption {
        type = with types; listOf (submodule {
          options = zoneOpts;
        });
      };
    };

  config = {
    site.dns.localZones = lib.dns.localZones;

    services.knot = lib.mkIf config.site.hosts.${hostName}.services.dns.enable (
      let
        generateZone = zone@{ name, dynamic, ... }: {
          domain = name;
          template = "zentralwerk";
          # TODO: move to config
          acl = [ "zone_xfr" ]
            ++ lib.optional (lib.hasSuffix ".arpa" name) "dn42"
            ++ lib.optional dynamic "dyndns"
            ++ lib.optional (name == "serv.zentralwerk.org") "portunus";
          file = if dynamic
            then "/var/lib/knot/zones/${name}.zone"
            else generateZoneFile zone;
          notify = [ "all" ];
        };

      in {
        enable = true;
        # TODO: move to config
        settings = {
          acl = [
            {
              id = "dyndns";
              key = "dyndns";
              action = "update";
            }
            {
              id = "portunus";
              key = "portunus";
              action = "update";
              update-owner = "name";
              update-owner-match = "sub-or-equal";
              update-owner-name = [ "auth.serv.zentralwerk.org." ];
            }
            {
              id = "zone_xfr";
              address = with config.site.net.serv; [
                # ns.c3d2.de
                hosts4.knot hosts6.dn42.knot hosts6.up4.knot
                "2a00:8180:2c00:282:2041:cbff:fe0c:8516"
                "fd23:42:c3d2:582:2041:cbff:fe0c:8516"
                # ns.spaceboyz.net
                "172.22.24.4" "37.27.116.148" "2a01:4f9:3070:2728::4"
                # ns1.supersandro.de
                "188.34.196.104" "2a01:4f8:1c1c:1d38::1"
              ];
              action = "transfer";
            }
            {
              id = "dn42";
              address = [ "fd42:180:3de0:30::1" ];
            }
          ];

          key = [ {
            id = "dyndns";
            algorithm  = "hmac-sha256";
            secret = config.site.dyndnsKey;
          } {
            id = "portunus";
            algorithm = "hmac-sha512";
            secret = config.site.portunusKey;
          }];

          log = [ {
            any = "notice";
            target = "syslog";
          } ];

          mod-stats = [ {
            id = "default";
            query-type = "on";
          } ];

          remote = let
            via = with config.site.net.serv; [ hosts4.dns hosts6.up4.dns ];
          in [
            {
              id = "ns.c3d2.de";
              address = with config.site.net.serv; [ hosts4.knot hosts6.dn42.knot hosts6.up4.knot ];
              inherit via;
            } {
              id = "ns.spaceboyz.net";
              address = [
                # "172.22.24.4" # DN42 is broken
                # "37.27.116.148" # not allowed....
                "2a01:4f9:3070:2728::4"
              ];
              inherit via;
            } {
              id = "ns1.supersandro.de";
              address = [ /*"188.34.196.104"*/ "2a01:4f8:1c1c:1d38::1" ];
              inherit via;
            } {
              id = "b.master.delegation-servers.dn42";
              address = [ "fd42:180:3de0:30::1" ];
            }
          ];

          remotes = [
            {
              id = "all";
              remote = [ "ns.c3d2.de" "ns.spaceboyz.net" "ns1.supersandro.de" ];
            }
            {
              id = "dn42";
              remote = [ "b.master.delegation-servers.dn42" ];
            }
          ];

          server = {
            answer-rotation = true;
            automatic-acl = true;
            identity = "dns.serv.zentralwerk.org";
            listen = with config.site.net; [
              "127.0.0.1" "::1"
              serv.hosts4.dns serv.hosts6.up4.dns serv.hosts6.dn42.dns
            ];
            tcp-fastopen = true;
            version = null;
          };

          template = [
            {
              # default is a magic name and is always loaded.
              # Because we want to use catalog-role/catalog-zone settings for all zones *except* the catalog zone itself, we must split the templates
              id = "default";
              global-module = [ "mod-stats" ];
            }
            {
              id = "zentralwerk";
              catalog-role = "member";
              catalog-zone = "zentralwerk.";
              dnssec-signing = true;
              journal-content = "all"; # required for zonefile-load=difference-no-serial and makes cold starts like zone reloads
              module = "mod-stats/default";
              semantic-checks = true;
              serial-policy = "increment";
              storage = "/var/lib/knot/zones";
              zonefile-load = "difference-no-serial";
            }
          ];

          zone = [ {
            acl = "zone_xfr";
            catalog-role = "generate";
            domain = "zentralwerk.";
            notify = [ "ns1.supersandro.de" "ns.spaceboyz.net" ];
            storage = "/var/lib/knot/catalog";
          } ] ++ map generateZone config.site.dns.localZones;
        };
      });

      systemd.services = {
        create-dynamic-zones = {
          description = "Creates dynamic zone files";
          requiredBy = [ "knot.service" ];
          before = [ "knot.service" ];
          serviceConfig.Type = "oneshot";
          script = ''
            mkdir -p /var/lib/knot/zones

            ${lib.concatMapStringsSep "\n" (zone@{ name, ... }: ''
              [ -e /var/lib/knot/zones/${name}.zone ] || \
                cp ${generateZoneFile zone} /var/lib/knot/zones/${name}.zone
              chown -R knot /var/lib/knot/zones
              chmod -R u+rwX /var/lib/knot/zones
            '') (builtins.filter ({ dynamic, ... }: dynamic) config.site.dns.localZones)}
          '';
        };

      update-dynamic-zones = {
        description = "Creates initial records in dynamic zone files";
        requiredBy = [ "knot.service" ];
        after = [ "knot.service" ];
        serviceConfig.Type = "oneshot";
        path = [ pkgs.dnsutils ];
        script = lib.concatMapStrings (zone: ''
          nsupdate -v -y "hmac-sha256:dyndns:${config.site.dyndnsKey}" <<EOF
          server localhost

          ${lib.concatMapStringsSep "\n" ({ name, type, data }: ''
            delete ${name}.${zone.name}. IN ${type}
            add ${name}.${zone.name}. 3600 IN ${type} ${data}
          '') zone.records}

          send
          EOF
        '') (builtins.filter ({ dynamic, ... }: dynamic) config.site.dns.localZones);
      };
    };
  };
}
