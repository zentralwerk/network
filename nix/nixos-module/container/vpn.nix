{ config, pkgs, ... }:

let
  privateKeyFile = ifName:
    "/run/wireguard-keys/${ifName}.key";
  ifName = "vpn";
in
{
  systemd.services = {
    "wireguard-key-${ifName}" = {
      description = "Create key file for wireguard interface '${ifName}'";
      requiredBy = [ "systemd-networkd.service" ];
      before = [ "systemd-networkd.service" ];
      serviceConfig.Type = "oneshot";
      script = ''
        F=${privateKeyFile ifName}
        mkdir -p -m 0700 $(dirname $F)
        chown systemd-network:systemd-network $(dirname $F)
        rm -f $F
        cat >$F <<EOF
        ${config.site.vpn.wireguard.privateKey}
        EOF
        chmod 0400 $F
        chown systemd-network:systemd-network $F
      '';
    };
  };

  systemd.network.netdevs.vpn = {
    netdevConfig = {
      Name = ifName;
      Kind = "wireguard";
    };
    wireguardConfig = {
      PrivateKeyFile = privateKeyFile ifName;
      ListenPort = config.site.vpn.wireguard.port;
    };
    wireguardPeers = map ({ publicKey, allowedIPs }: {
      wireguardPeerConfig = {
        PublicKey = publicKey;
        AllowedIPs = allowedIPs ++ [ "fe80::/64" "ff02::/16" ];
      };
    }) config.site.vpn.wireguard.peers;
  };

  systemd.network.networks.vpn.addresses = [ {
    Address = "fe80::1/64";
    Scope = "link";
  } ];

  environment.systemPackages = [
    pkgs.wireguard-tools
  ];
}
