# Routing daemon configuration
{ hostName, config, lib, pkgs, ... }:

let
  hostNameEscaped = builtins.replaceStrings [ "-" ] [ "_" ] hostName;

  hostConf = config.site.hosts.${hostName};

  upstreamInterfaces = lib.filterAttrs
    (_: { upstream, ... }:
      upstream != null
    )
    hostConf.interfaces;

  isUpstream = upstreamInterfaces != { };

  ipv6RouterNets = builtins.attrNames (
    lib.filterAttrs
      (net: { ipv6Router, ... }:
        ipv6Router == hostName
      )
      config.site.net
  );

  enumerate = n: list:
    if list == [ ]
    then [ ]
    else [{
      n = n;
      x = builtins.head list;
    }] ++ (enumerate (n + 1) (builtins.tail list));
in
{
  services.bird2 = lib.mkMerge [
    {
      enable = true;
      routerId = config.site.net.core.hosts4.${hostName};
      protocols = {
        kernel.K4 = ''
          learn;
          ipv4 {
            ${if isUpstream
              then ''
                # Install all routes but the default route on upstreams
                export where net != 0.0.0.0/0;
                # Learn the upstream default route
                import where net = 0.0.0.0/0;
              ''
              else ''
                export all;
              ''}
          };
        '';
        kernel.K6 = ''
          learn;
          ipv6 {
            ${if isUpstream
              then ''
                # Install all routes but the default route on upstreams
                export where net != ::/0;
                # Learn the upstream default route
                import where net = ::/0;
              ''
              else ''
                export all;
            ''}
          };
        '';
        device."" = ''
          scan time 10;
        '';
        # Import address ranges of upstream interfaces so that
        # internal traffic to local public services take no detours
        # if the default router takes another upstream gateway.
        direct."" = ''
          ipv4 {
            ${lib.optionalString isUpstream ''
              # No RFC1918, RFC6598
              import where net !~ [ 100.64.0.0/10 ] && net !~ [ 10.0.0.0/8 ] && net !~ [ 172.16.0.0/12 ] && net !~ [ 192.168.0.0/16 ];
            ''}
          };
          ipv6;
          interface ${lib.concatMapStringsSep ", " (iface:
            ''"${iface}"''
          )(builtins.attrNames hostConf.interfaces)};
          check link yes;
        '';
      };
    }
    (lib.optionalAttrs
      (
        builtins.match "anon.*" hostName != null ||
        hostName == "flpk-gw"
      )
      {
        # BIRD routing table for Wireguard transport
        config = ''
          ipv4 table vpn_table;
        '';

        # Kernel routing table for Wireguard transport
        protocols.kernel.VPN = ''
          # "vpn_table" configured on anon routers
          kernel table 100;
          ipv4 {
            export all;
            table vpn_table;
          };
        '';
      })
    (lib.optionalAttrs (ipv6RouterNets != [ ]) {
      # Router advertisements
      protocols.radv."" = ''
        rdnss ${config.site.net.serv.hosts6.dn42.dnscache};

        ${lib.concatMapStrings (net: ''
          interface "${net}" {
            min ra interval 10;
            max ra interval 60;
            solicited ra unicast yes;
            ${if (config.site.net.${net}.dhcp.server or null) == null
              then ''
                # Do not use DHCP6.
                managed no;
              '' else ''
                # Use DHCP6 for DynDNS.
                managed yes;
              ''}

        # can this be merged?
            ${builtins.concatStringsSep "\n" (
              map (subnet6: ''
                prefix ${subnet6} {
                  preferred lifetime 600;
                  valid lifetime 1800;
                };
              '') (builtins.attrValues config.site.net.${net}.subnets6)
            )}

            dnssl "${config.site.net.${net}.domainName}";
          };
        '') ipv6RouterNets}
      '';
    })
    {
      # OSPFv2 for site-local IPv4
      protocols.ospfv2."ZW4" = ''
        ipv4 {
          import all;
          # OSPF is self-contained
          export none;
        };
        area 0 {
          ${builtins.concatStringsSep "\n" (
              builtins.attrValues (
                builtins.mapAttrs (net: _:
                  # Enable OSPF only on networks with a secret.
                  if config.site.net ? "${net}" && config.site.net.${net}.ospf.secret != null
                  then ''
                    interface "${net}" {
                      hello 10;
                      wait 20;

                      authentication cryptographic;
                      password "${config.site.net.${net}.ospf.secret}";
                    };
                  ''
                  else ''
                    interface "${net}" {
                      stub yes;
                      cost 10;
                    };
                  ''
                ) hostConf.interfaces
              )
          )}
          ${builtins.concatStringsSep "\n" (
            map (stubnet4: ''
              # Advertise additional route
              stubnet ${stubnet4} {};
            '') hostConf.ospf.stubNets4
          )}
        };
      '';
    }
    (lib.optionalAttrs isUpstream {
      # OSPFv2 to advertise my default route
      protocols.ospfv2."ZW4_${hostNameEscaped}" = ''
        ipv4 {
          export where net = 0.0.0.0/0;
        };
        area 0 {
          ${builtins.concatStringsSep "\n" (
              builtins.attrValues (
                builtins.mapAttrs (net: _:
                  # Enable OSPF only on interfaces with a secret.
                  lib.optionalString (config.site.net.${net}.ospf.secret != null) ''
                    interface "${net}" instance ${toString hostConf.ospf.upstreamInstance} {
                      # Become the designated router
                      priority 10;
                      hello 10;
                      wait 20;

                      authentication cryptographic;
                      password "${config.site.net.${net}.ospf.secret}";
                    };
                  ''
                ) hostConf.physicalInterfaces
              )
          )}
        };
      '';
    })
    {
      protocols = lib.mkMerge (builtins.foldl'
        ({ protocols, n }: upstream: {
          protocols = protocols ++ [{
            # OSPFv2 to receive a default route from ${upstream}
            ospfv2."ZW4_${
              builtins.replaceStrings [ "-" ] [ "_" ] upstream
            }" = ''
              ipv4 {
                import filter {
                  preference = preference + ${toString (100 - n)};
                  accept;
                };
                ${lib.optionalString (
                  builtins.match "anon.*" hostName != null ||
                  hostName == "flpk-gw"
                ) ''
                  table vpn_table;
                ''}
              };
              area 0 {
                ${builtins.concatStringsSep "\n" (
                    builtins.attrValues (
                      builtins.mapAttrs (net: _:
                        # Enable OSPF only on interfaces with a secret.
                        lib.optionalString (config.site.net.${net}.ospf.secret != null) ''
                          interface "${net}" instance ${
                            builtins.replaceStrings [ "-" ] [ "_" ] (
                              toString config.site.hosts.${upstream}.ospf.upstreamInstance
                            )
                          } {
                            hello 10;
                            wait 20;
                            authentication cryptographic;
                            password "${config.site.net.${net}.ospf.secret}";
                          };
                        ''
                      ) hostConf.physicalInterfaces
                    )
                )}
              };
            '';
          }];
          n = n + 1;
        })
        { protocols = [ ]; n = 0; }
        hostConf.ospf.allowedUpstreams
      ).protocols;
    }
    {

      # OSPFv3 for site-local IPv6
      protocols.ospfv3."ZW6" = ''
        ipv6 {
          import all;
          # OSPF is self-contained
          export none;
        };
        area 0 {
          ${builtins.concatStringsSep "\n" (
              builtins.attrValues (
                builtins.mapAttrs (net: _:
                  # Enable OSPF only on networks with a secret.
                  if config.site.net.${net}.ospf.secret != null
                  then ''
                    interface "${net}" {
                      hello 10;
                      wait 20;

                      authentication cryptographic;
                      password "${config.site.net.${net}.ospf.secret}";
                    };
                  ''
                  else ''
                    interface "${net}" {
                      stub yes;
                      cost 10;
                    };
                  ''
                ) hostConf.physicalInterfaces
              )
          )}
          ${builtins.concatStringsSep "\n" (
            map (stubnet6: ''
              # Advertise additional route
              stubnet ${stubnet6} {};
            '')
              hostConf.ospf.stubNets6
          )}
        };
      '';
    }
    (lib.optionalAttrs isUpstream {
      # OSPFv3 to advertise my default route
      protocols.ospfv3."ZW6_${hostNameEscaped}" = ''
        ipv6 {
          export where net = ::/0;
        };
        area 0 {
          ${builtins.concatStringsSep "\n" (
              builtins.attrValues (
                builtins.mapAttrs (net: _:
                  # Enable OSPF only on interfaces with a secret.
                  lib.optionalString (config.site.net.${net}.ospf.secret != null) ''
                    interface "${net}" instance ${toString hostConf.ospf.upstreamInstance} {
                      # Become the designated router
                      priority 10;
                      hello 10;
                      wait 20;

                      authentication cryptographic;
                      password "${config.site.net.${net}.ospf.secret}";
                    };
                  ''
                ) hostConf.physicalInterfaces
              )
          )}
        };
      '';
    })
    (lib.optionalAttrs (builtins.match "anon.*" hostName == null) {
      protocols = lib.mkMerge (
        builtins.foldl'
          ({ protocols, n }: upstream: {
            protocols = protocols ++ [{
              # OSPFv3 to receive a default route from ${upstream}
              ospfv3."ZW6_${
              builtins.replaceStrings [ "-" ] [ "_" ] upstream
            }" = ''
                ipv6 {
                  import filter {
                    preference = preference + ${toString (100 - n)};
                    accept;
                  };
                };
                area 0 {
                  ${builtins.concatStringsSep "\n" (
                      builtins.attrValues (
                        builtins.mapAttrs (net: _:
                          # Enable OSPF only on interfaces with a secret.
                          lib.optionalString (config.site.net.${net}.ospf.secret != null) ''
                            interface "${net}" instance ${
                              builtins.replaceStrings [ "-" ] [ "_" ] (
                                toString config.site.hosts.${upstream}.ospf.upstreamInstance
                              )
                            } {
                              hello 10;
                              wait 20;
                              authentication cryptographic;
                              password "${config.site.net.${net}.ospf.secret}";
                            };
                          ''
                        ) hostConf.physicalInterfaces
                      )
                  )}
                };
              '';
            }];
            n = n + 1;
          })
          { protocols = [ ]; n = 0; }
          hostConf.ospf.allowedUpstreams6
      ).protocols;
    })
    {
      protocols.static = {
        # Zentralwerk DN42
        static4 = ''
          ipv4;
          # Own network
          route ${config.site.aggregates.zw} unreachable;
          # RFC 1122 'this' network
          route 0.0.0.0/8 unreachable;
          # RFC 1918 private space
          route 10.0.0.0/8 unreachable;
          route 172.16.0.0/12 unreachable;
          route 192.168.0.0/16 unreachable;
          # RFC 6598 Carrier grade nat space
          route 100.64.0.0/10 unreachable;
          # RFC 3927 link local
          route 169.254.0.0/16 unreachable;
          # RFC 5737 TEST-NET-1
          route 192.0.2.0/24 unreachable;
          # RFC 2544 benchmarking
          route 198.18.0.0/15 unreachable;
          # RFC 5737 TEST-NET-2
          route 198.51.100.0/24 unreachable;
          # RFC 5737 TEST-NET-3
          route 203.0.113.0/24 unreachable;
          # multicast
          route 224.0.0.0/4 unreachable;

          # can this be merged?
        '';

        static6 = ''
          ipv6;
          # Own networks
          route fd23:42:c3d2:580::/57 unreachable;
          route 2a00:8180:2c00:200::/56 unreachable;
          route 2a0f:5382:acab:1400::/56 unreachable;
          # RFC 4291 IPv4-compatible, loopback, et al
          route ::/8 unreachable;
          # RFC 6666 Discard-Only
          route 0100::/64 unreachable;
          # RFC 5180 BMWG
          route 2001:2::/48 unreachable;
          # RFC 4843 ORCHID
          route 2001:10::/28 unreachable;
          # RFC 3849 documentation
          route 2001:db8::/32 unreachable;
          # RFC 7526 6to4 anycast relay
          route 2002::/16 unreachable;
          # RFC 3701 old 6bone
          route 3ffe::/16 unreachable;
          # RFC 4193 unique local unicast
          route fc00::/7 unreachable;
          # RFC 4291 link local unicast
          route fe80::/10 unreachable;
          # RFC 3879 old site local unicast
          route fec0::/10 unreachable;
          # RFC 4291 multicast
          route ff00::/8 unreachable;
        '';
      };

      templates.bgp.bgppeer = lib.mkIf (hostConf.bgp != null) ''
        local as ${toString hostConf.bgp.asn};

        ipv4 {
          import all;
          export where source=RTS_STATIC;
        };
        ipv6 {
          import all;
          export where source=RTS_STATIC;
        };
      '';

      protocols.bgp = lib.mkIf (hostConf.bgp != null) (lib.mkMerge (
        map
          ({ n, x }:
            let
              peer = x;
              peerConf = hostConf.bgp.peers.${peer};
            in
            {
              "bgp_${toString n} from bgppeer" = ''
                neighbor ${peer} as ${toString peerConf.asn};
              '';
            }
          )
          (enumerate 1 (builtins.attrNames hostConf.bgp.peers))));
    }
  ];

  # Script that pings internet hosts every few minutes to determine if
  # the upstream actually works. The associated OSPF instance will be
  # enabled/disabled on state change.
  systemd.services =
    let
      interval = 5;
      targets = {
        ipv4 = [
          # inbert.c3d2.de
          "217.197.83.184"
          # ccc.de
          "195.54.164.39"
          # Cloud DNS services
          "9.9.9.9"
          "8.8.8.8"
          "1.1.1.1"
        ];
        ipv6 = [
          # inbert.c3d2.de
          "2001:67c:1400:2240::1"
          # ccc.de
          "2001:67c:20a0:2:0:164:0:39"
          # Cloud DNS services
          "2620:fe::9"
          "2606:4700:4700::1111"
          "2001:4860:4860::8888"
        ];
      };
      instance = {
        ipv4 = "ZW4_${hostNameEscaped}";
        ipv6 = "ZW6_${hostNameEscaped}";
      };
      checkService = addressFamily: {
        description = "Check connectivity for ${addressFamily}";
        after = [ "network.target" ];
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          User = "bird2";
          Group = "bird2";
        };
        path = with pkgs; [ bird2 iputils ];
        script = ''
          STATE=unknown

          while true; do
            NEW_STATE=unknown
            false \
          ${lib.concatMapStrings (target:
              "  || ping -n -s 0 -c 1 -w 1 ${target} 2>/dev/null >/dev/null \\\n"
            ) targets.${addressFamily}}  \
            && NEW_STATE=up \
            || NEW_STATE=down

            if [ $STATE != $NEW_STATE ]; then
              echo "Connectivity change from $STATE to $NEW_STATE"
              if [ $NEW_STATE = up ]; then
                birdc enable ${instance.${addressFamily}}
              else
                birdc disable ${instance.${addressFamily}}
              fi
            fi

            STATE=$NEW_STATE
            sleep ${toString interval}
          done
        '';
      };
    in
    lib.mkIf isUpstream {
      check-upstream-ipv4 = checkService "ipv4";
      check-upstream-ipv6 = checkService "ipv6";
    };
}
