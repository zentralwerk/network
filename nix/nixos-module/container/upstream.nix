{ hostName, config, lib, pkgs, ... }:

let
  hostConf = config.site.hosts.${hostName};

  upstreamInterfaces =
    lib.filterAttrs (_: { upstream, ... }: upstream != null)
      hostConf.interfaces;

  firstUpstreamInterface =
    if builtins.length (builtins.attrNames upstreamInterfaces) > 0
    then builtins.head (
      builtins.attrNames upstreamInterfaces
    )
    else null;

  enabled = firstUpstreamInterface != null;

  inherit (upstreamInterfaces.${firstUpstreamInterface}.upstream) staticIpv4Address;
in
{
  systemd.network.networks = {
    core = {
      # systemd-networkd only requests Prefix Delegation via DHCPv6 on
      # the upstream interface if another interface is configured for it.
      # without this, the static ipv6 subnet won't be routed to us.
      networkConfig.DHCPPrefixDelegation = true;
      dhcpV6PrefixDelegationConfig = {
        SubnetId = "81";
        # because we have static addresses, we don't actually use this
        Assign = false;
      };
    };
  } // builtins.mapAttrs (_: { upstream, ... }:
    # DHCP
    lib.optionalAttrs (hostName != "flpk-gw") {
      DHCP = "yes";
      networkConfig.IPv6AcceptRA = true;
      dhcpV6Config.PrefixDelegationHint = "::/56";
    }
    //
    # Traffic Shaping
    {
      extraConfig = ''
        [CAKE]
        Parent = root
        ${lib.optionalString (upstream.provider == "vodafone") ''
          # DOCSIS overhead
          OverheadBytes = 18
        ''}
        ${lib.optionalString (upstream.provider == "dsi") ''
          # PPPoE overhead
          OverheadBytes = 34
        ''}
        ${lib.optionalString (upstream.upBandwidth != null) ''
          Bandwidth = ${toString upstream.upBandwidth}K
        ''}
      '';
    }
  ) upstreamInterfaces;

  networking.nat = lib.optionalAttrs enabled {
    enable = true;
    enableIPv6 = true;
    internalInterfaces = [ "core" ];
    externalInterface = firstUpstreamInterface;
    externalIP = staticIpv4Address;
    extraCommands = ''
      # Add workaround for upstreams with wonky MTU
      iptables -t mangle -A FORWARD \
        -p tcp --tcp-flags SYN,RST SYN \
        -j TCPMSS --clamp-mss-to-pmtu
      ip6tables -t mangle -A FORWARD \
        -p tcp --tcp-flags SYN,RST SYN \
        -j TCPMSS --clamp-mss-to-pmtu

      # Prohibit SMTP except for servers
      iptables -N fwd_smtp || \
        iptables -F fwd_smtp
      iptables -A fwd_smtp --source ${config.site.net.serv.subnet4} -j RETURN
      iptables -A fwd_smtp --dest ${config.site.net.serv.subnet4} -j RETURN
      iptables -A fwd_smtp --source ${config.site.net.flpk.subnet4} -j RETURN
      iptables -A fwd_smtp --dest ${config.site.net.flpk.subnet4} -j RETURN
      iptables -A fwd_smtp -j REJECT
      iptables -I FORWARD -p tcp --dport 25 -j fwd_smtp

      ip6tables -N fwd_smtp || \
        ip6tables -F fwd_smtp
      ${lib.concatMapStrings (subnet6: ''
        ip6tables -A fwd_smtp --source ${subnet6} -j RETURN
        ip6tables -A fwd_smtp --dest ${subnet6} -j RETURN
      '') (builtins.concatMap builtins.attrValues [
        config.site.net.serv.subnets6
        config.site.net.flpk.subnets6
      ])}
      ip6tables -A fwd_smtp -j REJECT
      ip6tables -I FORWARD -p tcp --dport 25 -j fwd_smtp

      ${lib.optionalString (staticIpv4Address != null) ''
        # Allow connections to ${staticIpv4Address} from other hosts behind NAT
        ${lib.concatMapStrings (fwd: let
            m                = builtins.match "([0-9.]+):([0-9-]+)" fwd.destination;
            destinationIP    = if m == null then throw "bad ip:ports `${fwd.destination}'" else lib.elemAt m 0;
            destinationPorts = if m == null then throw "bad ip:ports `${fwd.destination}'" else builtins.replaceStrings ["-"] [":"] (lib.elemAt m 1);
        in ''
          iptables -t nat -A nixos-nat-pre \
            -d ${staticIpv4Address} -p ${fwd.proto} \
            --dport ${builtins.toString fwd.sourcePort} \
            -j DNAT --to-destination ${fwd.destination}

          iptables -t nat -A nixos-nat-post \
            -d ${destinationIP} -p ${fwd.proto} \
            --dport ${destinationPorts} \
            -s ${config.site.aggregates.zw} -j MASQUERADE
          iptables -t nat -A nixos-nat-post \
            -d ${destinationIP} -p ${fwd.proto} \
            --dport ${destinationPorts} \
            -s ${config.site.net.c3d2.subnet4} -j MASQUERADE
        '') config.networking.nat.forwardPorts}
      ''}

      # Do not NAT our public IPv4 addresses
      ${lib.concatMapStringsSep "\n" (net:
        lib.concatMapStrings (subnet: ''
          iptables -t nat -I nixos-nat-post \
            -o ${net} \
            -s ${subnet} \
            -j RETURN
        '') upstreamInterfaces.${net}.upstream.noNat.subnets4 or []
      ) (builtins.attrNames hostConf.interfaces)}

      # Provide IPv6 upstream for everyone, using NAT66 when not from
      # our static prefixes
      ${lib.concatMapStringsSep "\n" (net:
          lib.concatMapStrings (subnet: ''
            ip6tables -t nat -I nixos-nat-post \
              -o ${net} \
              -s ${subnet} \
              -j RETURN
          '') upstreamInterfaces.${net}.upstream.noNat.subnets6
      ) (builtins.attrNames upstreamInterfaces)}

      # There just have been moments without a complete ruleset. Flush
      # out invalid conntrack states!
      ${pkgs.conntrack-tools}/bin/conntrack -F
    '';

    extraStopCommands = ''
      iptables -F FORWARD 2>/dev/null || true
      ip6tables -F FORWARD 2>/dev/null || true

      ip6tables -t nat -F POSTROUTING 2>/dev/null || true
    '';

    forwardPorts = map ({ destination, sourcePort, reflect, ... }@forwardedPort:
      removeAttrs forwardedPort ["reflect"] // {
        destination =
          if builtins.match ".*:.*" destination != null
          then destination
          else "${destination}:${toString sourcePort}";
        loopbackIPs =
          if reflect
          then [ config.site.net.core.hosts4.${hostName} ]
          else [];
      }
    ) hostConf.forwardPorts;
  };
}
