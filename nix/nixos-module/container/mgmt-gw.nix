{
  # (IPv4-only) NAT the mgmt net
  networking.nat = {
    enable = true;
    externalInterface = "core";
  };
}
