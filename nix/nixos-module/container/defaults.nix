{ config, lib, modulesPath, pkgs, ... }:

{
  imports = [
    (modulesPath + "/profiles/minimal.nix")
    (modulesPath + "/virtualisation/lxc-container.nix")
  ];

  environment = {
    etc."machine-id".text = builtins.substring 0 8 (builtins.hashString "sha256" config.networking.hostName);
    systemPackages = with pkgs; [
      ripgrep
    ];
  };

  nix = {
    settings = {
      sandbox = false;
      max-jobs = lib.mkDefault 4;
      cores = lib.mkDefault 4;
    };
  };

  systemd.services =
    let
      noNestOpts.serviceConfig = {
        PrivateTmp = lib.mkOverride 0 false;
      };
    in {
      nscd = noNestOpts;
      systemdLogind = noNestOpts;

      nix-daemon.enable = false;
    };
  systemd.sockets.nix-daemon.enable = false;

  services.openssh.enable = false;
}
