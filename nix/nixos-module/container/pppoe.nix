{ hostName, config, lib, ... }:

let
  hostConf = config.site.hosts.${hostName};

  pppoeInterfaces =
    lib.filterAttrs (_: { type, ... }: type == "pppoe")
      hostConf.interfaces;

in lib.mkIf (pppoeInterfaces != {}) {
  boot.postBootCommands = ''
    if [ ! -c /dev/ppp ]; then
      mknod -m 600 /dev/ppp c 108 0
    fi
  '';

  environment.etc."ppp/pap-secrets".text = lib.concatMapStrings (ifName:
    let
      inherit (pppoeInterfaces.${ifName}.upstream) user password;
    in ''
      "${user}" * "${password}"
    '') (builtins.attrNames pppoeInterfaces);
  services.pppd = {
    enable = true;
    peers = builtins.mapAttrs (ifName: { upstream, ... }: {
      enable = true;
      autostart = true;
      config = ''
        plugin pppoe.so
        nic-${upstream.link}
        ifname ${ifName}
        # Login settings. (PAP)
        name "${upstream.user}"
        noauth
        hide-password
        # Connection settings.
        persist
        # Max connection attempts (0 = no limit)
        maxfail 0
        # Seconds between reconnection attempts
        holdoff 1

        # LCP settings.
        lcp-echo-interval 5
        lcp-echo-failure 6

        # PPPoE compliant settings.
        noaccomp
        default-asyncmap
        mtu 1492
        # IP settings.
        defaultroute
        +ipv6
        defaultroute6
        # Increase debugging level
        debug
      '';
    }) pppoeInterfaces;
  };

  systemd.network.networks =
    builtins.foldl' (networks: ifName: let
      iface = pppoeInterfaces.${ifName};
    in networks // {
      "${ifName}" = {
        matchConfig.Name = "${ifName}";
        linkConfig = lib.optionalAttrs (ifName == "core") {
          # interface is stuck in "routable (configuring)" and blocks systemd-networkd-wait-online
          # TODO: figure out why exactly
          RequiredForOnline = false;
        };
        networkConfig = {
          DHCP = lib.mkForce "ipv6";
          # accept config set by pppd
          KeepConfiguration = "yes";
        };
        dhcpV6Config = {
          PrefixDelegationHint = "::/56";
        };
      };
      "${iface.upstream.link}".networkConfig = {
        ConfigureWithoutCarrier = true;
        LinkLocalAddressing = "no";
      };
    }) {} (builtins.attrNames pppoeInterfaces);

}
