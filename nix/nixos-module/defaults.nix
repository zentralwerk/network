{ hostName, inputs, lib, pkgs, ... }:

{
  boot.kernelParams = [
    # No server/router runs any untrusted user code
    "mitigations=off"
    # Prevents automatic creation of interface bond0 by the kernel
    "bonding.max_bonds=0"
  ];
  boot.tmp.useTmpfs = true;

  # no persistent logs
  services.journald.extraConfig = /* ini */''
    RuntimeMaxUse=32M
    Storage=volatile
  '';

  nix = {
    registry = {
      nixpkgs.flake = inputs.nixpkgs;
    };

    settings = {
      experimental-features = [ "nix-command" "flakes" ];
      substituters = lib.mkBefore [ "https://hydra.hq.c3d2.de" ];
      trusted-public-keys = [
        "nix-serve.hq.c3d2.de:KZRGGnwOYzys6pxgM8jlur36RmkJQ/y8y62e52fj1ps=%"
      ];
    };
  };

  environment.systemPackages = with pkgs; [
    bmon
    bridge-utils
    conntrack-tools
    dhcpcd
    dhcpdump
    dig
    ethtool
    git
    htop
    iftop
    iperf
    iptables
    iptraf-ng
    iputils
    mtr
    psmisc
    screen
    speedtest-cli
    tcpdump
    tmux
    traceroute
    tree
    vim
    wget
  ];

  networking.hostName = hostName;

  programs = {
    fzf.keybindings = true;
    git = {
      enable = true;
      config = {
        alias = {
          co = "checkout";
          lg = "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)'";
          remote = "remote -v";
          st = "status";
          undo = "reset --soft HEAD^";
        };
        pull.rebase = true;
        rebase.autoStash = true;
      };
    };
    nano.enable = false;
    vim = {
      enable = true;
      defaultEditor = true;
      rememberCursorPosition = true;
    };
  };

  slim.enable = true;

  users.users.root.initialHashedPassword = "";

  system.stateVersion = "20.09";
}
