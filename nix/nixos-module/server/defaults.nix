{ pkgs, ... }:
{
  boot = {
    kernelModules = [ "kvm-intel" "pppoe" ];
    kernelParams = [ "nomodeset" ];
    zfs.recommendedDefaults = true;
  };

  hardware.cpu.intel.updateMicrocode = true;

  time.timeZone = "Europe/Berlin";

  environment.systemPackages = with pkgs; [
    inetutils # telnet
    ipmitool
    liboping # noping
    screen
    vim
    wget
  ];

  services.openssh = {
    enable = true;
    fixPermissions = true;
    recommendedDefaults = true;
    regenerateWeakRSAHostKey = true;
    settings.PermitRootLogin = "prohibit-password";
  };

  # additional config for bare metal
  services.collectd.plugins.ipmi = "";

  users.users.root.openssh.authorizedKeys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICAHp0RGv1T5jxEe1cNARNHoxng1dRH2T03fIwq706OC antrares@medienkiste"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICO6Ofmnd+uzFG/Tzp0rOH+SAZpJyYAhq08LwYAKsG69 nek0@miskatonic"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAID2ILx8LXED0CkVu4GguT8QckavMlB5A9CKS8BH+CmFc 2023@poelzi.org"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFidD6Snqgd8J7avxHvdDd81rdi0zNZWSilBe3eaTIlv sandro@magnesium"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGJJTSJdpDh82486uPiMhhyhnci4tScp5uUe7156MBC8 astro"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK0g2eARHuuxFJj/0/syHz/8iSPnTflmXnvugL+wpIbL markus.schmidl@mailbox.tu-dresden.de"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILJOEt/gZPIilGzNrJVjJthMuRRG9NaPh/FNV49Bfb0e juja@JLenovo"
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDC6Io8mHskJhkUh+vaSo95pi1E/gAoesQ0v+s+7DCTgjpOkB+W6vdJ8U6rblFxrETaWFAIVfkg+I/ZYvNWqCAxu1iWXaZ3IEK2ZiP5Vg0HevAP0ratfIHw50V8wfsyA8/lLVGdpX76xqexdY3G1SYZUcedq6AqWx6FpyoKGVOL2+jlJhmxCoEYfOJe6HbTi02UtAw1qavaD2acvuLksHOiwRAq1+ijPo/OmU3LmaErheceiPC97Wn4H/a98HdnWXJ3AXZCpPzp784/gUxOd/fvKRQPv0Lza9dytmpkAVc9efLMAQZm60w9InpIY0VxJRu2iFDc6msMF/iJp1UXSJfk4hTxUvXL8rPXc4GYKDSQlWO4UXoKd2gZEmCdcsIN/re6VR1lJWcm4eKxI9zJAQRZDrYHZP3ALBJrBY+7pJUHGSB+jCdZ73zkvkiNWUHZ9Wwp4RvdFoCR9qT+AoDU2SMiBMn8/hNMZRUs6RKjUzzn2vhCbZh19QIDxivaFg3DOKq7CCI3XNR3M781MFdmeTXKBLnv2YEVXy5XDIMvucQaZIUoD14fSF2wnncuP9h0gs2H1zG7nQfMagGpE+ro56FO3rBQqfRzz/U528yuq8uf/6TD9u3jTu7ngZ0YpDvAwAh8yG3b2KGFbrcYc3N1zEQOz7IqKixmIt/f3VOOjQ3Yww== antrares@c3d2.de"
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDdvlNFOnABycrb8lgwAr81o95UpMxNaViO7+1CxNc8kuGbGRpCCZ/FbjBBDDj2IeyATLNjo9+iUE8AQ+7VrxKHIigQ0UGE1iSvyk0iETKMQmRMRlT0SgJ0dKhrWNS5H5XrhdrvwkXPeOylQmwN+L6mWeB1Tdm4T783fxAs8LmLMMh5MRexK1sycydf3wjsKPm1c17IGmn6xn+hSNZzAoCw7kG0YH7ogI0BAdCSLrBEU3kUDpTNlaYp3oNQWzB4gWtYgiqaZIgKL4WqegOu3JPxtQ7p/qOgGMaBHyls62zvjjaJgNN4l6xKgEWdn79E9MNdGXKyOHXoaSQ5VXHDaXNDjYBIB0Sdxi8j3zTZ5fkun2jP4OEtEPwBWWvsbOX2bjPEsU0CYvTebGsRxzt5DS8pHfgFYf8XpL3zATWuh8QITVVrkZjqR1zHYN6aJUu089w6dMfZIWA38/plgneC4Df6F48vVB2srBTfZgFeXJ43Ei55hwU9gEwMOCtBHljmqVpi+SmVP+GlWf+/1p59PJIfUWRds0/F0t7xk+LsdKkMo3q+1SprfgLUK3qkFBod8WIEFnnAqZxgbEwUq11HW4L8n3pbEn4595h4AX/K4aBr4jhheY0vhTkPS2KdycfFA+RPcuQ+E+lquCyWmYS+dpJieON6DVnWoL/laxhOKR3c4w== jan@beatrice"
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC6NLB8EHnUgl2GO2uaojdf3p3YpsHH6px6CZleif8klhLN+ro5KeFK2OXC2SO3Vo4qgF/NySdsoInV9JEsssELZ2ttVbeKxI6f76V5dZgGI7qoSf4E0TXIgpS9n9K2AEmRKr65uC2jgkSJuo/T1mF+4/Nzyo706FT/GGVoiBktgq9umbYX0vIQkTMFAcw921NwFCWFQcMYRruaH01tLu6HIAdJ9FVG8MAt84hCr4D4PobD6b029bHXTzcixsguRtl+q4fQAl3WK3HAxT+txN91CDoP2eENo3gbmdTBprD2RcB/hz5iI6IaY3p1+8fTX2ehvI3loRA8Qjr/xzkzMUlpA/8NLKbJD4YxNGgFbauEmEnlC8Evq2vMrxdDr2SjnBAUwzZ63Nq+pUoBNYG/c+h+eO/s7bjnJVe0m2/2ZqPj1jWQp4hGoNzzU1cQmy6TdEWJcg2c8ints5068HN3o0gQKkp1EseNrdB8SuG+me/c/uIOX8dPASgo3Yjv9IGLhhx8GOGQxHEQN9QFC4QyZt/rrAyGmlX342PBNYmmStgVWHiYCcMVUWGlsG0XvG6bvGgmMeHNVsDf6WdMQuLj9luvxJzrd4FlKX6O0X/sIaqMVSkhIbD2+vvKNqrii7JdUTntUPs89L5h9DoDqQWkL13Plg1iQt4/VYeKTbUhYYz1lw== revo-xut@plank"
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCou/7YU2kbeWbZv/F3kjWJLyLeZ5SGGMNr03rWjqZcliJCqEZGO4gz7jdizg/h+j7YWTV3Gn+03LY+tlfhuI7Okxe1YLphuPb4qb38QUprpdg9QTdREGUUpKeaXUOXASoC5EHAkx5GYcQ9uZAx70ZHdggwNvQOVcOfbSIv+MPTaEq4MTwf/Y5MhFvCUrQecTvaoukAPS3PEOWptz5hDDH7jjiJmDwHeICMhHK9YvesFjIsc/iQHScCDWBg+WbQAeLYSbJkmnzFz/7jbdF34Wmz/7FlUiOqqzkZ5Ykr78ae4NgbSz09QjkZ/W0wVIH+UAVHn3OQ+7aRukkve9w48lEb1XJvMo3Y1sGRY6AUOHw0B4xa9ZgXQiuAH4ExjaDSArNkUWjQrKkUvyl30j7t6HRA2Y+W5BzodYKO/JBGqaGneTvlXV2e7lFP2kmnf17dnkJmwTi2p0CQJrpsnifuj5gNDA/qZkXPK5DOPe+asW2Vc2panSbXosZG9Gk20JeahZ54gVn2UvRVk41GhQdCAuVWeuXF9+rtSyjtx2NSrQLIyi/59n6STL/hS1135wrEifP+xTCoI+8yxTB8BSd5JSQ9GeUGkevZp9asmwKOA/WkTzsESECbCrbgOstTCSsKPfQITLu45zIrLHn9cLjrwby06mNhp2B28GlAmvcDBC95NQ== mail@oxapentane.com"
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDP6xE2ey0C8XXfvniiiHiqXsCC277jKI9RXEA+s2LQLUI5zl7v350i3Oa8H3NCcPj39lfMreqE6ncxcOhqYyzahPrrMkOqgbPAoRvq8H3ophLK+56O3xdHoKwLBwRD1yoGACjqG4UTiTrmnN2ateENgYcnTEY1e4vDw1qMj1drUXCsZ/6mkBBmHJiFfCaR4yCMt1r4gGi/dAC7ifnBP3oSyV/lJEwPxYYkGlbOBIvX/7Ar98pJS6xYPB3jHs9gwyNNON63d0fNYrwBojXPPCnGGaRZNOkBTzex3zZYp12ThINQ2xl8tRp9D8qpZ7vrLjhTD6AXkOBRzmDj+NsCeEaeTuWajqUM93iKncYUI+JxR1t7q8gA2pBMFzLesMXnx7R+5Kw7QDtSJM7a4GMIfsocPwf64BH6rzxEz68rXFE3P+J77PPM9CuaYw90JXHo3z220zYw2nMQ/1qjATVZw/hiVrLmQMVfmFJIufnGjTBs2sy3IoNyzvYm/oDeNNg1cdSV9gyyRKZhK08fxjXN5GSf9vZkfZa9tHtqaZ99HI40GQBHUVx1K2/NQJY8TVTSA+v16SFnJK8BIbmp/WFCuvDcMkgLIbqiYtDASe7P2mKIib86uOENT+P820egeLiTQ06kFw/gfUa8t69d5qEcjiQZ+lxCeYIs/E9KrEXHvRUWew== cardno:16 811 339"
  ];
}
