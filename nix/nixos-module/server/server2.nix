{ ... }:

{
  boot.initrd.availableKernelModules = [ "uhci_hcd" "ehci_pci" "ahci" "usbhid" "sd_mod" "sr_mod" ];
  boot.initrd.kernelModules = [ ];
  nixpkgs.config.allowBroken = true;
  boot.supportedFilesystems = [ "zfs" ];
  boot.initrd.supportedFilesystems = [ "zfs" ];
  boot.extraModulePackages = [ ];
  fileSystems."/" =
    { device = "server2/root";
      fsType = "zfs";
    };

  fileSystems."/var" =
    { device = "server2/root/var";
      fsType = "zfs";
    };

  fileSystems."/nix" =
    { device = "server2/root/nix";
      fsType = "zfs";
    };

  fileSystems."/nix/store" =
    { device = "server2/root/nix/store";
      fsType = "zfs";
    };

  fileSystems."/nix/var" =
    { device = "server2/root/nix/var";
      fsType = "zfs";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/29BF-1E56";
      fsType = "vfat";
    };

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";

  networking.hostName = "server2"; # Define your hostname.
  networking.hostId = "52525252";
}
