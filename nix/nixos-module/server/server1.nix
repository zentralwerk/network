{ config, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "server1"; # Define your hostname.
  networking.hostId = "01010101";

  boot.initrd.availableKernelModules = [ "uhci_hcd" "ehci_pci" "ata_piix" "hpsa" "usb_storage" "usbhid" "sd_mod" "sr_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.extraModulePackages = [ ];
  nixpkgs.config.allowBroken = true;
  boot.supportedFilesystems = [ "zfs" ];
  boot.initrd.supportedFilesystems = [ "zfs" ];
  # Required for Broadcom NICs
  hardware.enableRedistributableFirmware = true;

  fileSystems."/" =
    { device = "server1/root";
      fsType = "zfs";
    };

  fileSystems."/var" =
    { device = "server1/root/var";
      fsType = "zfs";
    };

  fileSystems."/nix" =
    { device = "server1/root/nix";
      fsType = "zfs";
    };

  fileSystems."/nix/store" =
    { device = "server1/root/nix/store";
      fsType = "zfs";
    };

  fileSystems."/nix/var" =
    { device = "server1/root/nix/var";
      fsType = "zfs";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-label/ESP";
      fsType = "vfat";
    };

  swapDevices = [ ];
}
