# Options for running under qemu (vm-packages)
{ inputs, lib, options, ... }:
{
  # Get internet from qemu user networking
  systemd.network = lib.optionalAttrs (options.virtualisation ? qemu) {
    networks."00-qemu" = {
      matchConfig.Name = "en*";
      networkConfig.DHCP = "yes";
    };
    netdevs.bond0.bondConfig.MinLinks = 0;
  };

  # for vm-packages
  virtualisation = lib.optionalAttrs (options.virtualisation ? qemu) {
    # larger than the defaults
    memorySize = 8192;
    cores = 2;
    diskSize = 8192;
    # 9P performance optimization that quelches a qemu warning
    msize = 65536;
    # allow building packages
    writableStore = true;
    # keep the store paths built inside the VM across reboots
    writableStoreUseTmpfs = false;
    qemu.options = [ "-enable-kvm" ];
  };

  # Let the nix registry point to the state of your local checkout
  # when running the virtual machine.
  nix.registry = lib.optionalAttrs (options.virtualisation ? qemu) {
    zentralwerk-network.flake = inputs.self;
  };

  systemd.services.nix-daemon.serviceConfig.LimitNOFILE = lib.mkOverride 10 40960;
}
