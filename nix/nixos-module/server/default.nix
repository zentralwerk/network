{ hostName, ... }:

{
  imports = [
    ./defaults.nix
    ./network.nix
    ./lxc-containers.nix
    ./qemu.nix
    # host-specific configuration
    ./${hostName}.nix
  ];
}
