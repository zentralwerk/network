{ hostName, config, lib, ... }:

let
  # pick an address for a net's gateway
  findGw6 = net: gw6:
    let
      inherit (config.site.net.${net}) hosts6;
    in
      builtins.foldl' (result: ctx:
        let
          h = hosts6.${ctx};
        in
          if result == null && h ? ${hostName} && h ? ${gw6}
          then h.${gw6}
          else result
      ) null (builtins.attrNames hosts6);
in
{
  networking = {
    firewall.enable = lib.mkDefault false;
    useDHCP = false;
    useNetworkd = true;
  };

  systemd.network = {
    enable = true;
    config.networkConfig.IPv6Forwarding = config.site.hosts.${hostName}.isRouter;
    networks =
      builtins.mapAttrs (ifName: { gw4, gw6, ... }:
        let
          netConfig = config.site.net.${ifName};
        in lib.mkIf (config.site.net ? ${ifName}) {
          matchConfig.Name = ifName;

          networkConfig = {
            IPv4Forwarding = config.site.hosts.${hostName}.isRouter;
            IPv6Forwarding = config.site.hosts.${hostName}.isRouter;
            IPv6AcceptRA = lib.mkDefault false;
            LLDP = true;
            EmitLLDP = true;
          };

          addresses =
            let
              address = netConfig.hosts4.${hostName};
              prefixLen = netConfig.subnet4Len;
            in
              lib.optional (netConfig.hosts4 ? ${hostName}) {
                Address = "${address}/${toString prefixLen}";
              } ++
              builtins.concatMap (hosts6:
                lib.optional (hosts6 ? ${hostName}) {
                  Address = "${hosts6.${hostName}}/64";
                }
              ) (builtins.attrValues netConfig.hosts6);

          gateway = with lib;
            optional (gw4 != null) config.site.net.${ifName}.hosts4.${gw4} ++
            optional (gw6 != null) (findGw6 ifName gw6);

        }) config.site.hosts.${hostName}.interfaces;
  };

  # DNS settings
  networking.useHostResolvConf = false;
  services.resolved.enable = false;
  environment.etc."resolv.conf".text = ''
    nameserver 172.20.73.8
    nameserver 9.9.9.9
  '';
}
