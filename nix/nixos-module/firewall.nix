{ hostName, config, lib, ... }:

let
  hostConfig = config.site.hosts.${hostName};

in {
  networking.firewall = lib.mkIf hostConfig.firewall.enable {
    enable = true;
    extraCommands = ''
      ${lib.optionalString hostConfig.isRouter ''
        ip46tables -I nixos-fw -p ospfigp -j ACCEPT
      ''}

      ip46tables -A FORWARD -i core -m state --state ESTABLISHED,RELATED -j ACCEPT
      ${lib.concatMapStrings (net: ''
        iptables -A FORWARD -i core --source ${config.site.net.${net}.subnet4} -j ACCEPT
        ${lib.concatMapStrings (subnet6: ''
          ip6tables -A FORWARD -i core --source ${subnet6} -j ACCEPT
        '') (builtins.attrValues config.site.net.${net}.subnets6)}
      '') hostConfig.firewall.allowedNets}
      ip46tables -A FORWARD -i core -j REJECT
    '';
    extraStopCommands = ''
      ip46tables -F FORWARD
    '';
  };
}
