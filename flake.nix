{
  description = "Zentralwerk network";

  inputs = {
    bird = {
      url = "github:NuschtOS/bird.nix";
      inputs = {
        flake-utils.follows = "nixos-modules/flake-utils";
        nixpkgs.follows = "nixpkgs";
      };
    };
    c3d2-dns = {
      url = "git+https://gitea.c3d2.de/c3d2/dns.git";
      inputs = {
        dns-nix.follows = "dns-nix";
        nixpkgs.follows = "nixpkgs";
      };
    };
    dns-nix = {
      url = "github:nix-community/dns.nix";
      inputs = {
        flake-utils.follows = "nixos-modules/flake-utils";
        nixpkgs.follows = "nixpkgs";
      };
    };
    nixos-modules = {
      url = "github:NuschtOS/nixos-modules";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixpkgs.url = "git+https://github.com/NuschtOS/nuschtpkgs.git?shallow=1&ref=backports-24.11";
    openwrt = {
      url = "git+https://git.openwrt.org/openwrt/openwrt.git?ref=openwrt-23.05";
      flake = false;
    };
    openwrt-imagebuilder = {
      url = "github:astro/nix-openwrt-imagebuilder";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        systems.follows = "nixos-modules/flake-utils/systems";
      };
    };
  };

  outputs = inputs@{ self, c3d2-dns, dns-nix, nixos-modules, nixpkgs, openwrt, openwrt-imagebuilder, bird }:
    let
      system = "x86_64-linux";
      systems = [ system ];
      forAllSystems = nixpkgs.lib.genAttrs systems;

      nixosConfig = name:
        self.lib.nixosSystem {
          inherit system;
          modules = [ self.nixosModule nixos-modules.nixosModule bird.nixosModules.bird ];
          specialArgs = {
            hostName = name;
            inherit (self) lib;
            inherit inputs c3d2-dns dns-nix self;
          };
        };
    in {
      # Config, and utilities
      lib = nixpkgs.lib.extend (_final: _prev:
        import ./nix/lib {
          inherit self openwrt;
          inherit (nixpkgs.legacyPackages.x86_64-linux) lib pkgs;
        });

      # Everything that can be built locally outside of NixOS
      packages = forAllSystems (system:
        import ./nix/pkgs {
          inherit (self.lib) config;
          inherit self nixpkgs system openwrt-imagebuilder;
        }
      );

      # Configuration for nixosConfigurations
      # (see nix/nixos-module/default.nix)
      nixosModule = import ./nix/nixos-module;

      # NixOS host systems (servers, and containers)
      nixosConfigurations =
        builtins.mapAttrs (hostName: _: nixosConfig hostName) (
          nixpkgs.lib.filterAttrs (_: { role, ... }:
            builtins.elem role [ "server" "container" ]
          ) self.lib.config.site.hosts
        );

      # For `nix flake check`, and Hydra
      checks = self.packages;

      hydraJobs =
        with self.lib;
        let
          exportFileWrapper = pkg:
            hydraJob (
              nixpkgs.legacyPackages.${system}.runCommand "${pkg.name}-exported" {
                src = pkg;
              } ''
                NAME="${pkg.name}"
                mkdir -p $out/nix-support
                ln -s ${pkg} $out/$NAME
                echo file source-dist $out/$NAME >> $out/nix-support/hydra-build-products
              ''
            );
          wrappers = {
            "export-config" = exportFileWrapper;
            "switch-.*" = exportFileWrapper;
            "ap.*-image" = exportFileWrapper;
          };
        in
          builtins.mapAttrs (name: pkg:
            builtins.foldl' (result: wrapperName:
              if builtins.match wrapperName name != null
              then wrappers.${wrapperName} pkg
              else result
            ) pkg (builtins.attrNames wrappers)
          ) self.packages.${system};
    };
}
