## Haus B

### Patchpanel Haus B Souterrain Mitte

Alle Stecker im Haus sind in Schema A gecrimpt.

[ri]: https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Decrease.svg/16px-Decrease.svg.png "red"
[yi]: https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/YellowDwn.svg/16px-YellowDwn.svg.png "yellow"
[gi]: https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Decrease_Positive.svg/16px-Decrease_Positive.svg.png "green"

| Panel D               | Panel C               | Panel B                   | Panel A                | Port  |
| --------------------- | --------------------- | ------------------------- | ---------------------- | ----- |
| ![][ri] B 3.01.01 *v* | ![][gi] B 2.01.01     | ![][yi] UVB 1.01          | ![][gi] Brücke Ost 1   | 01    |
| ![][ri] B 3.02.01 *v* | ![][gi] B 2.02.01     | ![][yi] UVB 1.02          | ![][gi] Brücke Ost 2   | 02    |
| ![][ri] B 3.03.02     | ![][gi] B 2.03.02     | ![][gi] B 1.03.03 *v*     | ![][gi] Brücke Ost 3   | 03    |
| ![][ri] B 3.04.02     | ![][gi] B 2.03.05     | ![][gi] B 1.05.05 *v*     | ![][gi] Brücke Ost 4   | 04    |
| ![][ri] B 3.05.03 *v* | ![][ri] B 2.03.03 (1) | ![][yi] B 1.06            | ![][gi] Brücke West 1  | 05    |
| ![][ri] B 3.05.02 *v* | ![][ri] B 2.03.03 (2) | ![][yi] B 1.04.01 (1) *v* | ![][gi] Brücke West 2  | 06    |
| ![][ri] B 3.06.02 *v* | ![][ri] B 2.03.03 (3) | ![][yi] B 1.04.01 (2) *v* | ![][gi] Brücke West 3  | 07    |
| ![][ri] B 3.08.02     | ![][ri] B 2.03.03 (4) | ![][yi] B 1.04.01 (3) *v* | ![][gi] Brücke West 4  | 08    |
| ![][ri] B 3.07.01 *v* | ![][gi] UVB 2.04      | ![][gi] B 1.04.01 (4) *v* |                        | 09    |
| ![][ri] B 3.09.03     | ![][gi] B 2.03.04 *v* | ![][gi] UVB 1.07          |                        | 10    |
| ![][ri] B 3.10.01     | ![][gi] UVB 2.07      | ![][gi] B 1.05.07 *v*     |                        | 11    |
| ![][ri] B 3.11.01     | ![][gi] B 2.05.01 *v* | ![][gi] B 1.11 *v*        |                        | 12    |
| ![][ri] B 3.12.01     | ![][gi] B 2.05.04     | ![][gi] UVB 1.08 *v*      |                        | 13    |
|                       | ![][gi] B 2.05.02     | ![][gi] UVB 1.09          |                        | 14    |
| ![][ri] B 4.02.01 *v* | ![][gi] B 2.05.05     | ![][gi] UVB 1.10          |                        | 15    |
| ![][ri] B 4.01.01 *v* | ![][gi] B 2.05.06     | ![][gi] 1.06              |                        | 16    |
| ![][ri] B 4.03.01 *v* | ![][gi] B 2.05.03 *v* | ![][gi] 1.16 *v*          |                        | 17    |
| ![][ri] B 4.04.01 *v* | ![][gi] B 2.05.07 *v* |                           |                        | 18    |
| ![][ri] B 4.05.02 *v* | ![][gi] B 2.06        |                           |                        | 19    |
| ![][ri] B 4.06.01 *v* | ![][ri] B 2.07        |                           |                        | 20    |
| ![][ri] B 4.07.05 *v* |                       |                           |                        | 21    |
| ![][ri] B 4.08.01     |                       |                           |                        | 22    |
| ![][ri] B 4.09.01 *v* |                       |                           |                        | 23    |
| ![][ri] B 4.10.01 *v* |                       |                           |                        | 24    |
*v*: verified

### Switch Haus B Souterrain Mitte

## Haus C
Turm C

### Patchpanel Haus C Keller
Zustand 2021-04-11

| Panel            | Port  |
|------------------|-------|
| ECCE             | 01    |
| Turm C 1. Etage  | 02    |
| Turm C 2. Etage  | 03    |
|                  | 04    |
|                  | 05    |
| Turm C ganz oben | 06    |
|                  | 07    |
|                  | 08    |
|                  | 09    |
|                  | 10    |
|                  | 11    |
|                  | 12    |
|                  | 13    |
|                  | 14    |
|                  | 15    |
|                  | 16    |
|                  | 17    |
|                  | 18    |
|                  | 19    |
| Antenne Dach     | 20    |
| A5 (Haus B)      | 21    |
| A6 (Haus B)      | 22    |
| A7 (Haus B)      | 23    |
| A8 (Haus B)      | 24    |
| Saal A Foyer     | ??    |

### Switch Haus C Keller
Zustand 2021-04-11

| Switch C1                 |       |
|---------------------------|-------|
| ![][gi] Modem Netzbiotop  | 01    |
| ![][gi] Modem Zentralwerk | 02    |
|                           | 03    |
|                           | 04    |
|                           | 05    |
| ![][gi] Panel 20          | 06    |
| ![][gi] Panel 01          | 07    |
|                           | 08    |
| ![][gi] Modem Azur        | 09    |
| ![][gi] Modem Krull       | 10    |
| ![][gi] Modem coloRadio   | 11    |
|                           | 12    |
|                           | 13    |
|                           | 14    |
| ![][ri] Saal A Bühne      | 15    |
| ![][gi] Saal A Bühne      | 16    |
| ![][gi] Panel 06          | 17    |
| ![][gi] Panel 02          | 18    |
| ![][gi] Panel 03          | 19    |
| ![][ri] Panel ??          | 20    |<!-- Saal A Foyer -->
| ![][gi] Panel 21          | 21    |
| ![][gi] Panel 22          | 22    |
| ![][gi] Panel 23          | 23    |
| ![][gi] Panel 24          | 24    |

## Haus D
Turm D

### Patchpanel Haus D Souterrain

| Panel            | Port  |
|------------------|-------|

### Switch Haus D Souterrain

| Switch D1                 |       |
|---------------------------|-------|
| ?                         | 01    |
| ?                         | 02    |
| ?                         | 03    |
| ?                         | 04    |
| ?                         | 05    |
| ?                         | 06    |
| ?                         | 07    |
| ?                         | 08    |
