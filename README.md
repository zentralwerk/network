# Netzwerk im [Zentralwerk](http://www.zentralwerk.de/)

[Begrüßung von Menschen im Haus, die für ihren Anschluss Zugang zum Netzwerk brauchen](doc/hello.md)

## Zweck

Das Zentralwerk ist ein Gebäude, das bestmöglich vernetzt sein soll. Das beginnt mit der Bereitstellung vom Zugang zum Internet, reicht über das isolierte Zusammenführen von einzelnen Anschlüssen und endet mit Angebot von zeitgemäßen Diensten für alle, die das Netz nutzen wollen.

Anstatt irgendwie rumzukonfigurieren, soll es eine Nachvollziehbarkeit (Reproduzierbarkeit) der gemachten Einstellungen geben.
Mit der Transparenz soll es möglich sein, dass das Netzwerk möglichst einfach verwaltet werden kann. (Stichworte sind: Zusammenarbeit, Wiederherstellbarkeit, Teilhabe, Mitmachen, Nachahmen, …)

## Öffentlichkeit

Warum ist das alles einsehbar? Für maximale Transparenz!
Wir sind kein Unternehmen das Geheimnisse schützen muss. Stattdessen wollen wir freie Kommunikation für alle!

Wie werden die (wenigen) persönlichen Daten "öffentlich" verwaltet? Mit entsprechender Verschlüsselung!
Wir, ein kleiner Kreis von Menschen die das Netzwerk im Zentralwerk betreuen, haben dafür einen Geheimschlüssel. Im Übrigen behandeln wir gleichermaßen sicher die "heiligen" Zugangsdaten für die Geräte (und andere für den Betrieb wichtigen Instanzen).

## Informationen für Lebewesen, die das Netzwerk mit verwalten

### Inhaltsverzeichnis

* [Verkabelung Patchpanel Haus B](cabling.md)
  * TODO: tuerme, saal
* (verschlüsselte) [Kontakte zu den Personen mit den einzelnen Anschlüssen](contact.md.gpg)
* Dokumentation
  * [WiFi-Provisionierung](doc/wifi-provisioning.md) und Erstellung von Privatnetzen
* `flake.nix` und Unterverzeichnis [nix/](nix): Konfigurationsskripte

### Development Setup

Get Nix with Flakes first:
```
nix-shell -p nixFlakes
```

To test-drive a server, run in a checkout of this repository:
```
nix run .#server2-vm
```

Login as `root` with an empty password. Use `build-container $(ls -1
/etc/lxc/containers)` to build and start the required containers.

If you encounter `too many open files` errors, bump limits for virtio
with `ulimit -n 524288` before starting QEMU.

For development purposes `build-container` can pick up container
rootfs built outside QEMU when the container's `prebuilt` option is
set.

#### Gerätekonfigurationen

```bash
nix run .#switch-b2 && nix run .#ap3
```

### Server Setup

```
ssh root@server2.cluster.zentralwerk.org
```

Ein Checkout dieses Repositorys liegt in `~/network`. Dorthin zeigt
auch `/etc/nixos` so dass `nixos-rebuild switch` problemlos
klappt. Ausserdem ist dieser lokale Checkout in der `nix registry`
eingetragen, was von bspw. von `build-container` verwendet wird.

Ausserdem werden dort immer wieder `nix run .#switch-to-production`
ausgeführt.

### LXC-Containers auf Server

Warum Container? Mehrere Routing-Tabellen verteile auf virtuelle
Router statt Policy Routing oder Virtual Routing Functions. Container
sind einfach zwischen Servern migrierbar.

Ein Server erwartet die für ihn (`location = hostName`) konfigurierten
Container. systemd versucht sie zu starten. Das wird erst nach
`build-container` funktionieren, welches das Rootfs anlegt.

Management per systemd-Service `lxc@.service`. Beispiel:
```
systemctl start lxc@pub-gw
```

```
lxc-attach -n pub-gw
```

Innerhalb eines Containers gibts erneut `systemctl`, `journalctl`,
`networkctl`

### Nix Flake

#### Site Config

Wir verwenden das Module-System von NixOS eigenständig in
`nix/lib/config` weil:

* Checks gegen options
* Nutzbarkeit in Paketen (device-scripts) ausserhalb eines
  `nixos-rebuild`
* Konfigurationsdaten über mehr als 1 Host

#### nixosConfigurations

Systemkonfigurationen für alle Hosts mit der `role` `"server"` oder
`"container"`

#### nixosModule

Alle NixOS-Einstellungen

#### Secrets

```bash
nix run .#decrypt-secrets
$EDITOR config/secrets-production.nix
nix run .#encrypt-secrets
```

### server1 als Cold Standby

Was ein Server kann, kann ein anderer auch. Er sollte gelegentlich
gebootet und aufgefrischt werden.

Damit die LXC-Container ganz kontrolliert nur auf einem gestartet
werden, muss die Datei `/etc/start-containers` *vorhanden* sein. Zum
Umgang damit gibt es die zwei handlichen Befehle `enable-containers`
und `disable-containers`.


#### IP Subnet Plans

`nix build .#`[subnetplan4](https://hydra.hq.c3d2.de/job/c3d2/zentralwerk-network/subnetplans/latest/download/1)

`nix build .#`[subnetplan6](https://hydra.hq.c3d2.de/job/c3d2/zentralwerk-network/subnetplans/latest/download/2)

#### Network Maps

![Physikalische Vernetzung](https://hydra.hq.c3d2.de/job/c3d2/zentralwerk-network/network-graphs/latest/download/2)
![Logische Vernetzung](https://hydra.hq.c3d2.de/job/c3d2/zentralwerk-network/network-graphs/latest/download/1)
