{ lib, ... }:

{
  imports = [
    ./data.nix

    # Secrets
    ./secrets.nix

    # Hardware
    ./vlan.nix
    ./switch.nix
    ./ap.nix
    ./server.nix

    # IP networks
    ./network.nix
  ] ++ lib.filesystem.listFilesRecursive ./nets;
}
