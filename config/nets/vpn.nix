{
  site.net.vpn = {
    vlan = null;
    domainName = "core.zentralwerk.org";
    ipv6Router = "vpn-gw";
    hosts4 = {
      vpn-gw = "172.20.76.225";
    };
    hosts6 = {
      dn42 = {
        vpn-gw = "fd23:42:c3d2:585::1";
      };
      up4 = {
        vpn-gw = "2a00:8180:2c00:285::1";
      };
    };
    subnet4 = "172.20.76.224/28";
    subnets6 = {
      dn42 = "fd23:42:c3d2:585::/64";
      up4 = "2a00:8180:2c00:285::/64";
    };
  };

  site.hosts.vpn-gw = {
    role = "container";
    interfaces = {
      core = {
        hwaddr = "0A:14:42:01:26:01";
        type = "veth";
      };
      vpn = {
        type = "wireguard";
      };
    };
    ospf = {
      allowedUpstreams = [ "flpk-gw" "anon1" "freifunk" ];
    };
  };
}
