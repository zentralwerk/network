{ lib, ... }:
{
  site.net.serv = {
    domainName = "serv.zentralwerk.org";
    subnet4 = "172.20.73.0/25";
    hosts4 = {
      serv-gw = "172.20.73.1";
      dns = "172.20.73.2";
      stats = "172.20.73.3";
      dresden-zone = "172.20.73.4";
      taler = "172.20.73.5"; # datenspuren 2024
      nixtaler = "172.20.73.6"; # datenspuren 2024
      tlms-elastic = "172.20.73.7"; # tlms
      dnscache = "172.20.73.8";
      tlms-ctfd = "172.20.73.9"; # tlms
      buzzrelay = "172.20.73.15";
      matemat = "172.20.73.21";
      spaceapi = "172.20.73.25";
      minecraft = "172.20.73.26";
      mucbot = "172.20.73.27";
      scrape = "172.20.73.32";
      pretalx = "172.20.73.33";
      vaultwarden = "172.20.73.34";
      uranus = "172.20.73.37"; # tlms
      tram-borzoi = "172.20.73.38"; # tlms
      borken-data-hoarder = "172.20.73.39"; # tlms
      matrix = "172.20.73.40";
      luulaatsch-asterisk = "172.20.73.42";
      grafana = "172.20.73.43";
      engel = "172.20.73.44";
      public-access-proxy = "172.20.73.45";
      marenz = "172.20.73.46";
      lora = "172.20.73.47";
      home-assistant = "172.20.73.48";
      hydra = "172.20.73.49";
      owncast = "172.20.73.50";
      nfsroot = "172.20.73.51";
      ticker = "172.20.73.52";
      gitea = "172.20.73.53";
      stream = "172.20.73.54";
      jabber = "172.20.73.55";
      mobilizon = "172.20.73.56";
      radiobert = "172.20.73.57";
      nixos-misc = "172.20.73.58";
      chirp = "172.20.73.59";
      sdrweb = "172.20.73.60";
      knot = "172.20.73.61";
      blogs = "172.20.73.62";
      staging-data-hoarder = "172.20.73.64"; # tlms
      oparl = "172.20.73.65";
      hedgedoc = "172.20.73.66";
      mediawiki = "172.20.73.67";
      gnunet = "172.20.73.68";
      data-hoarder = "172.20.73.69"; # tlms
      broker = "172.20.73.70";
      ftp = "172.20.73.71";
      auth = "172.20.73.72";
      doubleblind-science = "172.20.73.73";
      mediagoblin = "172.20.73.74";
      prometheus = "172.20.73.75";
      drone = "172.20.73.77";
      # FILL IN THE HOLES BEFORE APPENDING!
    };
    ipv6Router = "serv-gw";
    subnets6.dn42 = "fd23:42:c3d2:582::/64";
    subnets6.up4 = "2a00:8180:2c00:282::/64";
    hosts6.dn42 = {
      knot = "fd23:42:c3d2:582:cd7:56ff:fe69:6366";
      blogs = "fd23:42:c3d2:582:b8a8:7dff:fee8:5ac2";
      dns = "fd23:42:c3d2:582:2:0:0:2";
      dnscache = "fd23:42:c3d2:582:f096:dbff:fee8:427d";
      gitea = "fd23:42:c3d2:582:702a:daff:fe35:83be";
      grafana = "fd23:42:c3d2:582:4042:fbff:fe4b:2de8";
      engel = "fd23:42:c3d2:582:4042:fbff:fe4b:2de9";
      hydra = "fd23:42:c3d2:582:e2cb:4eff:fe3b:f94b";
      jabber = "fd23:42:c3d2:582:b869:ccff:fe46:902a";
      # mail = "fd23:42:c3d2:582:88c0:41ff:fe70:d6cd";
      matemat = "fd23:42:c3d2:582:f82b:1bff:fedc:8572";
      mobilizon = "fd23:42:c3d2:582:48d1:5cff:fea7:1676";
      mongo = "fd23:42:c3d2:582:14ec:c8ff:fe0a:fc5c";
      mucbot = "fd23:42:c3d2:582:28db:dff:fe6b:e89a";
      radiobert = "fd23:42:c3d2:582:e65f:1ff:fe5d:1679";
      radius = "fd23:42:c3d2:582:2:0:0:4";
      sdrweb = "fd23:42:c3d2:582:3078:bbff:fe76:e9ef";
      serv-gw = "fd23:42:c3d2:582::1";
      spaceapi = "fd23:42:c3d2:582:1457:adff:fe93:62e9";
      stats = "fd23:42:c3d2:582:2:0:0:3";
      staging-data-hoarder = "fd23:42:c3d2:582:2de:5bff:fef9:e23d";
      oparl = "fd23:42:c3d2:582:2de:9aff:fece:3879";
      gnunet = "fd23:42:c3d2:582::44";
      broker = "fd23:42:c3d2:582::46";
      ftp = "fd23:42:c3d2:582::47";
      nixos-misc = "fd23:42:c3d2:582::48";
      # _ = "fd23:42:c3d2:582::2f";
      home-assistant = "fd23:42:c3d2:582::31";
      owncast = "fd23:42:c3d2:582::32";
      prometheus = "fd23:42:c3d2:582::4b";
      oxigraph = "fd23:42:c3d2:582::4c";
      mediagoblin = "fd23:42:c3d2:582::4d";
      buzzrelay = "fd23:42:c3d2:582::f";
      luulaatsch-asterisk = "fd23:42:c3d2:582::2a";
      stream = "fd23:42:c3d2:583:dc91:c7ff:fe51:d1c5";
      taler = "fd23:42:c3d2:583:7851:c1f1:9ff8:234f"; # datenspuren 2024
      nixtaler = "fd23:42:c3d2:583::1:6"; # datenspuren 2024
    };
    hosts6.up4 = {
      knot = "2a00:8180:2c00:282:cd7:56ff:fe69:6366";
      blogs = "2a00:8180:2c00:282:b8a8:7dff:fee8:5ac2";
      dns = "2a00:8180:2c00:282:2:0:0:2";
      dnscache = "2a00:8180:2c00:282:f096:dbff:fee8:427d";
      gitea = "2a00:8180:2c00:282:702a:daff:fe35:83be";
      grafana = "2a00:8180:2c00:282:4042:fbff:fe4b:2de8";
      engel = "2a00:8180:2c00:282:4042:fbff:fe4b:2de9";
      hydra = "2a00:8180:2c00:282:e2cb:4eff:fe3b:f94b";
      jabber = "2a00:8180:2c00:282:b869:ccff:fe46:902a";
      # mail = "2a00:8180:2c00:282:88c0:41ff:fe70:d6cd";
      matemat = "2a00:8180:2c00:282:f82b:1bff:fedc:8572";
      mobilizon = "2a00:8180:2c00:282:48d1:5cff:fea7:1676";
      mucbot = "2a00:8180:2c00:282:28db:dff:fe6b:e89a";
      nixos-misc = "2a00:8180:2c00:282:f09b:78ff:fe47:41e2";
      public-access-proxy = "2a00:8180:2c00:282:1024:5fff:febd:9be7";
      radiobert = "2a00:8180:2c00:282:e65f:1ff:fe5d:1679";
      radius = "2a00:8180:2c00:282:2:0:0:4";
      scrape = "2a00:8180:2c00:282:e073:50ff:fef5:eb6e";
      sdrweb = "2a00:8180:2c00:282:3078:bbff:fe76:e9ef";
      spaceapi = "2a00:8180:2c00:282:1457:adff:fe93:62e9";
      stats = "2a00:8180:2c00:282:2:0:0:3";
      minecraft = "2a00:8180:2c00:282:2:0:0:5";
      stream = "2a00:8180:2c00:282:dc91:c7ff:fe51:d1c5";
      ticker = "2a00:8180:2c00:282:b407:40ff:fec1:81f2";
      staging-data-hoarder = "2a00:8180:2c00:282:2de:5bff:fef9:e23d";
      oparl = "2a00:8180:2c00:282:2de:9aff:fece:3879";
      taler = "2a00:8180:2c00:282:7851:c1f1:9ff8:234f"; # datenspuren 2024
      nixtaler = "2a00:8180:2c00:282::1:6"; # datenspuren 2024

      serv-gw = "2a00:8180:2c00:282::1";
      luulaatsch-asterisk = "2a00:8180:2c00:282::2a";
      drone = "2a00:8180:2c00:282::2b";
      pretalx = "2a00:8180:2c00:282::2c";
      matrix = "2a00:8180:2c00:282::2d";
      # _ = "2a00:8180:2c00:282::2f";
      vaultwarden = "2a00:8180:2c00:282::31";
      owncast = "2a00:8180:2c00:282::32";
      mediawiki = "2a00:8180:2c00:282::43";
      gnunet = "2a00:8180:2c00:282::44";
      data-hoarder = "2a00:8180:2c00:282::45";
      broker = "2a00:8180:2c00:282::46";
      ftp = "2a00:8180:2c00:282::47";
      auth = "2a00:8180:2c00:282::48";
      dresden-zone = "2a00:8180:2c00:282::49";
      prometheus = "2a00:8180:2c00:282::4b";
      oxigraph = "2a00:8180:2c00:282::4c";
      hedgedoc = "2a00:8180:2c00:282::6";
      mediagoblin = "2a00:8180:2c00:282::7";
      buzzrelay = "2a00:8180:2c00:282::f";
    };
  };

  site.hosts =
    let
      makeGateway = lib.recursiveUpdate {
        interfaces = {
          serv = {
            gw4 = "serv-gw";
            gw6 = "serv-gw";
            type = "veth";
          };
        };
        role = "container";
      };
    in {
    dns = makeGateway {
      interfaces.serv.hwaddr = "0A:14:48:01:23:00";
      services.dns.enable = true;
    };
    dnscache = makeGateway {
      services.dnscache.enable = true;
    };
    serv-gw = makeGateway {
      interfaces = {
        core = {
          type = "veth";
          hwaddr = "0A:14:48:01:06:01";
          gw4 = null;
          gw6 = null;
        };
        serv = {
          hwaddr = "0A:14:48:01:06:00";
          gw4 = null;
          gw6 = null;
        };
      };
      ospf.allowedUpstreams = [ "upstream4" "upstream3" "anon1" "freifunk" ];
    };

    stats = makeGateway {
      interfaces.serv.hwaddr = "0A:14:48:01:15:00";
    };

    hydra = {
      role = "client";
      model = "nixos";
      interfaces.serv.type = "phys";
    };
  };
}
