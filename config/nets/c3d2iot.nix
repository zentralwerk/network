{
  site.net.c3d2iot = {
    dhcp = {
      start = "10.22.0.2";
      end = "10.22.255.253";
      router = "iot-gw";
      server = "iot-gw";
      # devices don't often change and a missing DNS record causes trouble
      time = 3600;
      max-time = 24 * 3600;
    };
    dynamicDomain = true;
    domainName = "c3d2iot.zentralwerk.org";
    hosts4 = {
      iot-gw = "10.22.0.1";
    };
    hosts6 = {
      dn42 = {
        iot-gw = "fd23:42:c3d2:587:ffff:ffff:ffff:ffff";
      };
    };
    subnet4 = "10.22.0.0/16";
    subnets6 = {
      dn42 = "fd23:42:c3d2:587::/64";
      up4 = "2a00:8180:2c00:287::/64";
    };
  };

  site.hosts.iot-gw = {
    # TODO: needs to be done more granular, aka allow c3d2 and serv network
    # firewall.enable = true;
    interfaces = {
      core = {
        hwaddr = "0A:22:48:01:24:01";
        type = "veth";
      };
      c3d2iot = {
        hwaddr = "0A:22:48:01:24:00";
        type = "veth";
      };
    };
    ospf = {
      allowedUpstreams = [ "upstream4" "upstream3" "anon1" ];
    };
    role = "container";
  };
}
