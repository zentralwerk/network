{ lib, ... }:
let
  privCount = 49;
  seq = n: max:
    if n <= max
    then [ n ] ++ seq (n + 1) max
    else [];
in
lib.mkMerge (
  map (n:
    let
      toHex = n: with lib; toLower (toHexString n);
      subnetUp4 = "2a00:8180:2c00:${toHex (703 + n)}::";
      subnetDn42 = "fd23:42:c3d2:${toHex (1471 + n)}::";
    in {
      site.net."priv${toString n}" = {
        dhcp = {
          server = "priv${toString n}-gw";
          time = 300;
          max-time = 60 * 24 * 3600;
          router = "priv${toString n}-gw";
        };
        domainName = "priv${toString n}.zentralwerk.org";
        dynamicDomain = true;
        hosts6 = {
          dn42 = { "priv${toString n}-gw" = "${subnetDn42}1"; };
          up4 = { "priv${toString n}-gw" = "${subnetUp4}1"; };
        };
        subnets6 = {
          dn42 = "${subnetDn42}/64";
          up4 = "${subnetUp4}/64";
        };
      };

      site.hosts."priv${toString n}-gw" = {
        role = "container";
        interfaces = {
          core.type = "veth";
          "priv${toString n}".type = "veth";
        };
        ospf.allowedUpstreams = [ "upstream4" "upstream3" "anon1" "freifunk" ];
      };
    }
  ) (seq 1 privCount)
  ++
  [ {
    site.net = {
      priv1 = {
        subnet4 = "172.20.74.0/28";
        hosts4 = { priv1-gw = "172.20.74.1"; };
        dhcp = {
          end = "172.20.74.14";
          start = "172.20.74.2";
        };
      };
      priv2 = {
        hosts4 = { priv2-gw = "172.20.75.1"; };
        subnet4 = "172.20.75.0/27";
        dhcp = {
          start = "172.20.75.2";
          end = "172.20.75.30";
          fixed-hosts = {
            "172.20.75.2" = "ac:1f:6b:dc:93:8e";
            "172.20.75.3" = "ac:1f:6b:dc:95:de";
            "172.20.75.9" = "ac:1f:6b:dc:95:df";
            "172.20.75.7" = "60:33:4b:0b:cd:fc";
          };
        };
      };
      priv3 = {
        hosts4 = { priv3-gw = "172.20.74.129"; };
        subnet4 = "172.20.74.128/28";
        dhcp = {
          start = "172.20.74.130";
          end = "172.20.74.142";
        };
      };
      priv4 = {
        hosts4 = { priv4-gw = "172.20.75.129"; };
        subnet4 = "172.20.75.128/28";
        dhcp = {
          start = "172.20.75.130";
          end = "172.20.75.142";
        };
      };
      priv5 = {
        hosts4 = { priv5-gw = "172.20.74.65"; };
        subnet4 = "172.20.74.64/28";
        dhcp = {
          start = "172.20.74.66";
          end = "172.20.74.78";
        };
      };
      priv6 = {
        hosts4 = { priv6-gw = "172.20.74.193"; };
        subnet4 = "172.20.74.192/28";
        dhcp = {
          start = "172.20.74.194";
          end = "172.20.74.206";
        };
      };
      priv7 = {
        hosts4 = { priv7-gw = "172.20.75.65"; };
        subnet4 = "172.20.75.64/28";
        dhcp = {
          start = "172.20.75.66";
          end = "172.20.75.78";
        };
      };
      priv8 = {
        hosts4 = { priv8-gw = "172.20.75.193"; };
        subnet4 = "172.20.75.192/28";
        dhcp = {
          start = "172.20.75.194";
          end = "172.20.75.206";
        };
      };
      priv9 = {
        hosts4 = { priv9-gw = "172.20.74.33"; };
        subnet4 = "172.20.74.32/28";
        dhcp = {
          start = "172.20.74.34";
          end = "172.20.74.46";
        };
      };
      priv10 = {
        hosts4 = { priv10-gw = "172.20.74.97"; };
        subnet4 = "172.20.74.96/28";
        dhcp = {
          start = "172.20.74.98";
          end = "172.20.74.110";
        };
      };
      priv11 = {
        hosts4 = { priv11-gw = "172.20.74.161"; };
        subnet4 = "172.20.74.160/28";
        dhcp = {
          start = "172.20.74.162";
          end = "172.20.74.174";
        };
      };
      priv12 = {
        hosts4 = { priv12-gw = "172.20.74.225"; };
        subnet4 = "172.20.74.224/28";
        dhcp = {
          start = "172.20.74.226";
          end = "172.20.74.238";
        };
      };
      priv13 = {
        hosts4 = { priv13-gw = "172.20.75.33"; };
        subnet4 = "172.20.75.32/28";
        dhcp = {
          start = "172.20.75.34";
          end = "172.20.75.46";
        };
      };
      priv14 = {
        hosts4 = { priv14-gw = "172.20.75.97"; };
        subnet4 = "172.20.75.96/28";
        dhcp = {
          start = "172.20.75.98";
          end = "172.20.75.110";
        };
      };
      priv15 = {
        hosts4 = { priv15-gw = "172.20.75.161"; };
        subnet4 = "172.20.75.160/28";
        dhcp = {
          start = "172.20.75.162";
          end = "172.20.75.174";
        };
      };
      priv16 = {
        hosts4 = { priv16-gw = "172.20.75.225"; };
        subnet4 = "172.20.75.224/28";
        dhcp = {
          start = "172.20.75.226";
          end = "172.20.75.238";
        };
      };
      priv17 = {
        hosts4 = {
          priv17-gw = "172.20.73.129";
          priv17-gw-up3 = "172.20.73.130";
        };
        subnet4 = "172.20.73.128/27";
        dhcp = {
          start = "172.20.73.131";
          end = "172.20.73.158";
        };
      };
      priv18 = {
        hosts4 = { priv18-gw = "172.20.74.49"; };
        subnet4 = "172.20.74.48/28";
        dhcp = {
          start = "172.20.74.50";
          end = "172.20.74.62";
        };
      };
      priv19 = {
        hosts4 = { priv19-gw = "172.20.73.193"; };
        subnet4 = "172.20.73.192/26";
        dhcp = {
          start = "172.20.73.194";
          end = "172.20.73.254";
        };
      };
      priv20 = {
        hosts4 = { priv20-gw = "172.20.74.113"; };
        subnet4 = "172.20.74.112/28";
        dhcp = {
          start = "172.20.74.114";
          end = "172.20.74.126";
        };
      };
      priv21 = {
        hosts4 = { priv21-gw = "172.20.74.145"; };
        subnet4 = "172.20.74.144/28";
        dhcp = {
          start = "172.20.74.146";
          end = "172.20.74.158";
        };
      };
      priv22 = {
        hosts4 = { priv22-gw = "172.20.74.177"; };
        subnet4 = "172.20.74.176/28";
        dhcp = {
          start = "172.20.74.178";
          end = "172.20.74.190";
        };
      };
      priv23 = {
        hosts4 = { priv23-gw = "172.20.73.161"; };
        subnet4 = "172.20.73.160/27";
        dhcp = {
          start = "172.20.73.165";
          end = "172.20.73.190";
          fixed-hosts = {
            "172.20.73.162" = "da:2c:3a:2c:87:22"; # homeassistant
            "172.20.73.163" = "b8:27:eb:16:31:61"; # phoscon
            "172.20.73.164" = "ca:71:c4:90:3e:c7"; # homemattic
            "172.20.73.165" = "3c:ec:ef:47:b7:f4"; # jupiter
          };
          time = lib.mkForce 900;
        };
      };
      priv24 = {
        hosts4 = { priv24-gw = "172.20.74.241"; };
        subnet4 = "172.20.74.240/28";
        dhcp = {
          start = "172.20.74.242";
          end = "172.20.74.254";
        };
      };
      priv25 = {
        # ZW stage
        hosts4 = { priv25-gw = "192.168.16.1"; };
        subnet4 = "192.168.16.0/24";
        dhcp = {
          start = "192.168.16.2";
          end = "192.168.16.254";
        };
      };
      priv26 = {
        hosts4 = { priv26-gw = "172.20.75.49"; };
        subnet4 = "172.20.75.48/28";
        dhcp = {
          start = "172.20.75.50";
          end = "172.20.75.62";
        };
      };
      priv27 = {
        hosts4 = { priv27-gw = "172.20.75.81"; };
        subnet4 = "172.20.75.80/28";
        dhcp = {
          start = "172.20.75.82";
          end = "172.20.75.94";
        };
      };
      priv28 = {
        hosts4 = { priv28-gw = "172.20.75.113"; };
        subnet4 = "172.20.75.112/28";
        dhcp = {
          start = "172.20.75.114";
          end = "172.20.75.126";
        };
      };
      priv29 = {
        hosts4 = { priv29-gw = "172.20.75.145"; };
        subnet4 = "172.20.75.144/28";
        dhcp = {
          start = "172.20.75.146";
          end = "172.20.75.158";
        };
      };
      priv30 = {
        hosts4 = { priv30-gw = "172.20.75.177"; };
        subnet4 = "172.20.75.176/28";
        dhcp = {
          start = "172.20.75.178";
          end = "172.20.75.190";
        };
      };
      priv31 = {
        hosts4 = { priv31-gw = "172.20.75.209"; };
        subnet4 = "172.20.75.208/28";
        dhcp = {
          start = "172.20.75.210";
          end = "172.20.75.221";
          fixed-hosts = {
            # zw-ev-vm
            "172.20.75.222" = "92:a8:00:49:a6:61";
          };
        };
      };
      priv32 = {
        hosts4 = { priv32-gw = "172.20.75.241"; };
        subnet4 = "172.20.75.240/28";
        dhcp = {
          start = "172.20.75.242";
          end = "172.20.75.254";
        };
      };
      priv33 = {
        hosts4 = { priv33-gw = "172.20.74.17"; };
        subnet4 = "172.20.74.16/28";
        dhcp = {
          start = "172.20.74.18";
          end = "172.20.74.30";
        };
      };
      priv34 = {
        hosts4 = { priv34-gw = "172.20.74.209"; };
        subnet4 = "172.20.74.208/28";
        dhcp = {
          start = "172.20.74.210";
          end = "172.20.74.222";
        };
      };
      priv35 = {
        hosts4 = { priv35-gw = "172.20.76.1"; };
        subnet4 = "172.20.76.0/28";
        dhcp = {
          start = "172.20.76.2";
          end = "172.20.76.14";
        };
      };
      priv36 = {
        hosts4 = { priv36-gw = "172.20.76.65"; };
        subnet4 = "172.20.76.64/28";
        dhcp = {
          start = "172.20.76.66";
          end = "172.20.76.78";
        };
      };
      priv37 = {
        hosts4 = { priv37-gw = "172.20.76.129"; };
        subnet4 = "172.20.76.128/28";
        dhcp = {
          start = "172.20.76.130";
          end = "172.20.76.142";
        };
      };
      priv38 = {
        hosts4 = { priv38-gw = "172.20.76.193"; };
        subnet4 = "172.20.76.192/28";
        dhcp = {
          start = "172.20.76.194";
          end = "172.20.76.206";
        };
      };
      priv39 = {
        hosts4 = { priv39-gw = "172.20.77.129"; };
        subnet4 = "172.20.77.128/28";
        dhcp = {
          start = "172.20.77.130";
          end = "172.20.77.142";
        };
      };
      priv40 = {
        hosts4 = { priv40-gw = "172.20.77.65"; };
        subnet4 = "172.20.77.64/28";
        dhcp = {
          start = "172.20.77.66";
          end = "172.20.77.78";
        };
      };
      priv41 = {
        hosts4 = { priv41-gw = "172.20.77.193"; };
        subnet4 = "172.20.77.192/28";
        dhcp = {
          start = "172.20.77.194";
          end = "172.20.77.206";
        };
      };
      priv42 = {
        hosts4 = { priv42-gw = "172.20.76.33"; };
        subnet4 = "172.20.76.32/28";
        dhcp = {
          start = "172.20.76.34";
          end = "172.20.76.46";
        };
      };
      priv43 = {
        hosts4 = { priv43-gw = "172.20.76.97"; };
        subnet4 = "172.20.76.96/28";
        dhcp = {
          start = "172.20.76.98";
          end = "172.20.76.110";
        };
      };
      priv44 = {
        hosts4 = { priv44-gw = "172.20.77.97"; };
        subnet4 = "172.20.77.96/28";
        dhcp = {
          start = "172.20.77.98";
          end = "172.20.77.110";
        };
      };
      priv45 = {
        hosts4 = { priv45-gw = "172.20.77.161"; };
        subnet4 = "172.20.77.160/28";
        dhcp = {
          start = "172.20.77.162";
          end = "172.20.77.174";
        };
      };
      priv46 = {
        hosts4 = { priv46-gw = "172.20.77.225"; };
        subnet4 = "172.20.77.224/28";
        dhcp = {
          start = "172.20.77.226";
          end = "172.20.77.238";
        };
      };
      priv47 = {
        hosts4 = { priv47-gw = "172.20.76.161"; };
        subnet4 = "172.20.76.160/28";
        dhcp = {
          start = "172.20.76.162";
          end = "172.20.76.174";
        };
      };
      priv48 = {
        hosts4 = { priv48-gw = "172.20.77.33"; };
        subnet4 = "172.20.77.32/28";
        dhcp = {
          start = "172.20.77.34";
          end = "172.20.77.46";
        };
      };
      priv49 = {
        hosts4 = { priv49-gw = "172.20.76.49"; };
        subnet4 = "172.20.76.48/28";
        dhcp = {
          start = "172.20.76.50";
          end = "172.20.76.62";
        };
      };
    };

    site.hosts = {
      priv1-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:19:00";
          priv1.hwaddr = "0A:14:48:01:19:01";
        };
      };
      priv2-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:18:00";
          priv2.hwaddr = "0A:14:48:01:18:01";
        };
      };
      priv3-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:08:00";
          priv3.hwaddr = "0A:14:48:01:08:01";
        };
      };
      priv4-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:17:01";
          priv4.hwaddr = "0A:14:48:01:17:00";
        };
      };
      priv5-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:12:00";
          priv5.hwaddr = "0A:14:48:01:12:01";
        };
      };
      priv6-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:11:00";
          priv6.hwaddr = "0A:14:48:01:11:01";
        };
      };
      priv7-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:10:00";
          priv7.hwaddr = "0A:14:48:01:10:01";
        };
      };
      priv8-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:09:00";
          priv8.hwaddr = "0A:14:48:01:09:01";
        };
      };
      priv9-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:20:00";
          priv9.hwaddr = "0A:14:48:01:20:01";
        };
      };
      priv10-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:13:02";
          priv10.hwaddr = "0A:14:48:01:13:03";
        };
      };
      priv11-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:29:00";
          priv11.hwaddr = "0A:14:48:01:29:01";
        };
      };
      priv12-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:00";
          priv12.hwaddr = "0A:14:48:01:2A:01";
        };
      };
      priv13-gw = {
        firewall.enable = true;
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:10";
          priv13.hwaddr = "0A:14:48:01:2A:11";
        };
      };
      priv14-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:12";
          priv14.hwaddr = "0A:14:48:01:2A:13";
        };
      };
      priv15-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:14";
          priv15.hwaddr = "0A:14:48:01:2A:15";
        };
      };
      priv16-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:16";
          priv16.hwaddr = "0A:14:48:01:2A:17";
        };
      };
      priv17-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:18";
          priv17.hwaddr = "0A:14:48:01:2A:19";
        };
      };
      priv17-gw-up3 = {
        role = "container";
        interfaces = {
          core = {
            type = "veth";
            hwaddr = "0A:14:47:02:2A:18";
          };
          priv17 = {
            type = "veth";
            hwaddr = "0A:14:47:02:2A:19";
          };
        };
        ospf.allowedUpstreams = [ "upstream3" "upstream4" "anon1" "freifunk" ];
      };
      priv18-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:1A";
          priv18.hwaddr = "0A:14:48:01:2A:1B";
        };
      };
      priv19-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:1C";
          priv19.hwaddr = "0A:14:48:01:2A:1D";
        };
      };
      priv20-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:1E";
          priv20.hwaddr = "0A:14:48:01:2A:1F";
        };
      };
      priv21-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:20";
          priv21.hwaddr = "0A:14:48:01:2A:21";
        };
      };
      priv22-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:24";
          priv22.hwaddr = "0A:14:48:01:2A:25";
        };
      };
      priv23-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:22";
          priv23.hwaddr = "0A:14:48:01:2A:23";
        };
      };
      priv24-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:26";
          priv24.hwaddr = "0A:14:48:01:2A:27";
        };
      };
      priv25-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:28";
          priv25.hwaddr = "0A:14:48:01:2A:29";
        };
      };
      priv26-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:2A";
          priv26.hwaddr = "0A:14:48:01:2A:2B";
        };
      };
      priv27-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:2C";
          priv27.hwaddr = "0A:14:48:01:2A:2D";
        };
      };
      priv28-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:2E";
          priv28.hwaddr = "0A:14:48:01:2A:2F";
        };
      };
      priv29-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:30";
          priv29.hwaddr = "0A:14:48:01:2A:31";
        };
      };
      priv30-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:32";
          priv30.hwaddr = "0A:14:48:01:2A:33";
        };
      };
      priv31-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:34";
          priv31.hwaddr = "0A:14:48:01:2A:35";
        };
      };
      priv32-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:36";
          priv32.hwaddr = "0A:14:48:01:2A:37";
        };
      };
      priv33-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:38";
          priv33.hwaddr = "0A:14:48:01:2A:39";
        };
      };
      priv34-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:40";
          priv34.hwaddr = "0A:14:48:01:2A:41";
        };
      };
      priv35-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:42";
          priv35.hwaddr = "0A:14:48:01:2A:43";
        };
      };
      priv36-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:44";
          priv36.hwaddr = "0A:14:48:01:2A:45";
        };
      };
      priv37-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:46";
          priv37.hwaddr = "0A:14:48:01:2A:47";
        };
      };
      priv38-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:48";
          priv38.hwaddr = "0A:14:48:01:2A:49";
        };
      };
      priv39-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:4A";
          priv39.hwaddr = "0A:14:48:01:2A:4B";
        };
      };
      priv40-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:4C";
          priv40.hwaddr = "0A:14:48:01:2A:4D";
        };
      };
      priv41-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:4E";
          priv41.hwaddr = "0A:14:48:01:2A:4F";
        };
      };
      priv42-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:50";
          priv42.hwaddr = "0A:14:48:01:2A:51";
        };
      };
      priv43-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:52";
          priv43.hwaddr = "0A:14:48:01:2A:53";
        };
      };
      priv44-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:54";
          priv44.hwaddr = "0A:14:48:01:2A:55";
        };
      };
      priv45-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:56";
          priv45.hwaddr = "0A:14:48:01:2A:57";
        };
      };
      priv46-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:58";
          priv46.hwaddr = "0A:14:48:01:2A:59";
        };
      };
      priv47-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:5A";
          priv47.hwaddr = "0A:14:48:01:2A:5B";
        };
      };
      priv48-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:5C";
          priv48.hwaddr = "0A:14:48:01:2A:5D";
        };
      };
      priv49-gw = {
        interfaces = {
          core.hwaddr = "0A:14:48:01:2A:5E";
          priv49.hwaddr = "0A:14:48:01:2A:5F";
        };
      };
    };
  } ]
)
