{
  site.net.coloradio = {
    domainName = "coloradio.zentralwerk.org";
    subnet4 = "192.168.9.0/24";
    hosts4 = {
      coloradio-gw = "192.168.9.1";
      coloradio-in = "192.168.9.2";
    };

    ipv6Router = "coloradio-gw";
    subnets6.dn42 = "fd23:42:c3d2:590::/64";
    hosts6.dn42 = {
      coloradio-gw = "fd23:42:c3d2:590::1";
    };
  };

  site.hosts = {
    coloradio-gw = {
      role = "container";
      interfaces = {
        core = {
          type = "veth";
          hwaddr = "0A:14:48:01:06:08";
          gw4 = null;
          gw6 = null;
        };
        coloradio = {
          type = "veth";
          hwaddr = "0A:14:48:01:06:09";
          gw4 = null;
          gw6 = null;
        };
      };
      ospf.allowedUpstreams = [ "upstream4" "upstream3" "freifunk" ];
    };
  };
}
