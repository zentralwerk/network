{
  site.net.cluster = {
    ipv6Router = "cls-gw";
    domainName = "cluster.zentralwerk.org";
    hosts4 = {
      cls-gw = "172.20.77.1";
      server1 = "172.20.77.30";
      server2 = "172.20.77.2";
      # server3 = "172.20.77.3";
      # server4 = "172.20.77.4";
      # server5 = "172.20.77.5";
      # server6 = "172.20.77.6";
      # server7 = "172.20.77.7";
      server8 = "172.20.77.8";
      server9 = "172.20.77.9";
      server10 = "172.20.77.10";
      # server11 = "172.20.77.11";
      # server12 = "172.20.77.12";
      # server13 = "172.20.77.13";
      # server14 = "172.20.77.14";
      # server15 = "172.20.77.15";
      # server16 = "172.20.77.16";
      # server17 = "172.20.77.17";
      # server18 = "172.20.77.18";
      # server19 = "172.20.77.19";
    };
    hosts6 = {
      dn42 = {
        cls-gw = "fd23:42:c3d2:586::1";
        server1 = "fd23:42:c3d2:586::11";
        server2 = "fd23:42:c3d2:586::12";
        # server3 = "fd23:42:c3d2:586::13";
        # server4 = "fd23:42:c3d2:586::14";
        # server5 = "fd23:42:c3d2:586::15";
        # server6 = "fd23:42:c3d2:586::16";
        # server7 = "fd23:42:c3d2:586::17";
        server8 = "fd23:42:c3d2:586::18";
        server9 = "fd23:42:c3d2:586::19";
        server10 = "fd23:42:c3d2:586::1a";
        # server11 = "fd23:42:c3d2:586::1b";
        # server12 = "fd23:42:c3d2:586::1c";
        # server13 = "fd23:42:c3d2:586::1d";
        # server14 = "fd23:42:c3d2:586::1e";
        # server15 = "fd23:42:c3d2:586::1f";
        # server16 = "fd23:42:c3d2:586::20";
        # server17 = "fd23:42:c3d2:586::21";
        # server18 = "fd23:42:c3d2:586::22";
        # server19 = "fd23:42:c3d2:586::23";
      };
      up4 = {
        cls-gw = "2a00:8180:2c00:284::1";
        server1 = "2a00:8180:2c00:284::11";
        server2 = "2a00:8180:2c00:284::12";
        # server3 = "2a00:8180:2c00:284::13";
        # server4 = "2a00:8180:2c00:284::14";
        # server5 = "2a00:8180:2c00:284::15";
        # server6 = "2a00:8180:2c00:284::16";
        # server7 = "2a00:8180:2c00:284::17";
        server8 = "2a00:8180:2c00:284::18";
        server9 = "2a00:8180:2c00:284::19";
        server10 = "2a00:8180:2c00:284::1a";
        # server11 = "2a00:8180:2c00:284::1b";
        # server12 = "2a00:8180:2c00:284::1c";
        # server13 = "2a00:8180:2c00:284::1d";
        # server14 = "2a00:8180:2c00:284::1e";
        # server15 = "2a00:8180:2c00:284::1f";
        # server16 = "2a00:8180:2c00:284::20";
        # server17 = "2a00:8180:2c00:284::21";
        # server18 = "2a00:8180:2c00:284::22";
        # server19 = "2a00:8180:2c00:284::23";
      };
    };
    subnet4 = "172.20.77.0/27";
    subnets6 = {
      dn42 = "fd23:42:c3d2:586::/64";
      up4 = "2a00:8180:2c00:284::/64";
    };
  };

  site.hosts =
    let
      makeServer = {
        role = "client";
        model = "nixos";
        interfaces = builtins.foldl' (interfaces: net:
          interfaces // {
            "${net}".type = "bridge";
          }
        ) {} [
          "cluster"
          "core"
          "mgmt"
          "serv"
          "c3d2"
          "c3d2iot"
          "play"
          "pub"
          "priv23"
          "priv31"
          "priv45"
          "bmx"
          "flpk"
        ];
      };
    in {
    cls-gw = {
      role = "container";
      interfaces = {
        cluster = {
          hwaddr = "0A:14:48:01:06:02";
          type = "veth";
        };
        core = {
          hwaddr = "0A:14:48:01:06:03";
          type = "veth";
        };
      };
      ospf.allowedUpstreams = [ "upstream4" "upstream3" "anon1" "freifunk" ];
    };
    server8 = makeServer;
    server9 = makeServer;
    server10 = makeServer;
  };
}
