{
  site.net.play = {
    dhcp = {
      start = "172.20.72.130";
      end = "172.20.72.190";
      router = "play-gw";
      server = "play-gw";
      # devices don't often change and a missing DNS record causes trouble
      time = 24 * 3600;
      max-time = 30 * 24 * 3600;
    };
    dynamicDomain = true;
    domainName = "play.zentralwerk.org";
    hosts4 = {
      play-gw = "172.20.72.129";
    };
    hosts6 = {
      up4 = {
        play-gw = "2a00:8180:2c00:288:ffff:ffff:ffff:ffff";
      };
      dn42 = {
        play-gw = "fd23:42:c3d2:588:ffff:ffff:ffff:ffff";
      };
    };
    subnet4 = "172.20.72.128/26";
    subnets6 = {
      dn42 = "fd23:42:c3d2:588::/64";
      up4 = "2a00:8180:2c00:288::/64";
    };
  };

  site.hosts.play-gw = {
    role = "container";
    interfaces = {
      core = {
        hwaddr = "0A:22:48:01:9f:01";
        type = "veth";
      };
      play = {
        hwaddr = "0A:22:48:01:9f:00";
        type = "veth";
      };
    };
    ospf.allowedUpstreams = [ "upstream4" "upstream3" "anon1" ];
  };
}
