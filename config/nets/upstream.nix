{ config, ... }:
let
  servHosts = config.site.net.serv.hosts4;
  inherit (config.site.net.c3d2.hosts4) dn42;
  inherit (config.site.net.flpk.hosts4) c3d2-web;

  monitoringTarget = [
    "8.8.8.8"
    "2001:4860:4860::8888"
    "r.njalla.net"
    "inbert.c3d2.de"
    "heise.de"
  ];
in
{
  site.hosts = {
    upstream3 = {
      interfaces = {
        core = {
          hwaddr = "0A:14:48:01:28:00";
          type = "veth";
        };
        up3 = {
          hwaddr = "00:23:74:D7:42:7D";
          type = "veth";
          upstream = {
            link = null;
            noNat = { subnets6 = [ ]; };
            provider = "starlink";
            staticIpv4Address = null;
            upBandwidth = null;
          };
        };
      };
      ospf.upstreamInstance = 7;
      role = "container";
      services.monitoring.target = monitoringTarget;
    };

    upstream4 = rec {
      forwardPorts = [
        { # http
          destination = servHosts.public-access-proxy;
          proto = "tcp";
          sourcePort = 80;
          # this is the default but written here explicitly because we do ip based filtering
          reflect = false;
        }
        { # https
          destination = servHosts.public-access-proxy;
          proto = "tcp";
          sourcePort = 443;
          # this is the default but written here explicitly because we do ip based filtering
          reflect = false;
        }
        { # gemini
          destination = "${c3d2-web}:1965";
          proto = "tcp";
          sourcePort = 1965;
        }
        {
          destination = servHosts.knot;
          proto = "tcp";
          sourcePort = 53;
          # this is the default but written here explicitly because we do ip based filtering
          reflect = false;
        }
        {
          destination = servHosts.knot;
          proto = "udp";
          sourcePort = 53;
          # this is the default but written here explicitly because we do ip based filtering
          reflect = false;
        }
        {
          destination = dn42;
          proto = "udp";
          sourcePort = 2325;
        }
        {
          destination = dn42;
          proto = "udp";
          sourcePort = 2327;
        }
        {
          destination = dn42;
          proto = "udp";
          sourcePort = 2337;
        }
        {
          destination = dn42;
          proto = "udp";
          sourcePort = 2338;
        }
        {
          destination = dn42;
          proto = "udp";
          sourcePort = 2339;
        }
        {
          destination = dn42;
          proto = "udp";
          sourcePort = 2340;
        }
        {
          destination = dn42;
          proto = "udp";
          sourcePort = 2342;
        }
        {
          destination = dn42;
          proto = "udp";
          sourcePort = 2399;
        }
        {
          destination = dn42;
          proto = "udp";
          sourcePort = 24699;
        }
        {
          destination = dn42;
          proto = "udp";
          sourcePort = 64699;
        }
        # ?
        {
          destination = "172.22.99.175:22";
          proto = "tcp";
          sourcePort = 2224;
        }
        {
          destination = servHosts.gitea;
          proto = "tcp";
          sourcePort = 22;
        }
        {
          destination = servHosts.jabber;
          proto = "tcp";
          sourcePort = 5222;
        }
        {
          destination = servHosts.jabber;
          proto = "tcp";
          sourcePort = 5223;
        }
        {
          destination = servHosts.jabber;
          proto = "tcp";
          sourcePort = 5269;
        }
        {
          destination = servHosts.jabber;
          proto = "tcp";
          sourcePort = 3478;
        }
        {
          destination = servHosts.jabber;
          proto = "tcp";
          sourcePort = 3479;
        }
        {
          destination = servHosts.jabber;
          proto = "udp";
          sourcePort = 3478;
        }
        {
          destination = servHosts.jabber;
          proto = "udp";
          sourcePort = 3479;
        }
        # poelzi
        {
          destination = "172.20.73.162:22";
          proto = "tcp";
          sourcePort = 2323;
        }
        # jan
        {
          destination = "172.20.75.3:51820";
          proto = "udp";
          sourcePort = 30057;
        }
        {
          destination = config.site.net.core.hosts4.vpn-gw;
          proto = "udp";
          sourcePort = config.site.vpn.wireguard.port;
          reflect = true;
        }
        {
          destination = servHosts.gnunet;
          proto = "tcp";
          sourcePort = 2086;
        }
        # dresden zone
        {
          destination = servHosts.dresden-zone;
          proto = "udp";
          sourcePort = 51844;
        }
        # data-hoarder
        {
          destination = servHosts.data-hoarder;
          proto = "udp";
          sourcePort = 51820;
          reflect = true;
        }
        {
          destination = "${servHosts.data-hoarder}:22";
          proto = "tcp";
          sourcePort = 2269;
        }
        # data-hoarder-staging
        {
          destination = "${servHosts.staging-data-hoarder}:51820";
          proto = "udp";
          sourcePort = 51821;
          reflect = true;
        }
        {
          destination = "${servHosts.ftp}:22";
          proto = "tcp";
          sourcePort = 1022;
        }
        # coloRadio
        {
          proto = "tcp";
          sourcePort = 8000;
          destination = "192.168.9.127";
        }
      ];
      interfaces = {
        core = {
          hwaddr = "0A:14:48:01:28:01";
          type = "veth";
        };
        up4 = {
          hwaddr = "00:23:74:D7:42:7E";
          type = "veth";
        };
        up4-pppoe = {
          type = "pppoe";
          upstream = {
            link = "up4";
            noNat = {
              subnets6 = [
                "2a00:8180:2000:37::1/128"
                "2a00:8180:2c00:200::/56"
              ];
            };
            provider = "dsi";
            staticIpv4Address = "81.201.149.152";
            upBandwidth = 98000;
          };
        };
      };
      ospf = {
        upstreamInstance = 8;
        stubNets4 = [
          "${interfaces.up4-pppoe.upstream.staticIpv4Address}/32"
        ];
      };
      role = "container";
      services.monitoring.target = monitoringTarget;
    };

    freifunk.ospf.upstreamInstance = 6;

    anon1 = {
      interfaces = {
        core = {
          hwaddr = "0A:14:48:01:14:00";
          type = "veth";
        };
        njalla = {
          type = "wireguard";
          upstream = {
            provider = "njal.la";
            upBandwidth = 45000;
          };
        };
      };
      ospf = {
        allowedUpstreams = [ "upstream3" "upstream4" "freifunk" ];
        upstreamInstance = 5;
      };
      role = "container";
      services.monitoring.target = monitoringTarget;
    };
  };
}
