{ lib, ... }:
{
  site.net.c3d2 = {
    dhcp = {
      server = "c3d2-gw3";
      start = "172.22.99.100";
      end = "172.22.99.199";
      fixed-hosts = {
        "172.22.99.96" = "08:00:27:bb:8c:b3";
        "172.22.99.98" = "08:00:27:aa:90:e2";
        # "beere" = "b8:27:eb:ac:65:d2";
        # "beere2" = "b8:27:eb:53:0b:27";
        "dacbert" = "dc:a6:32:e0:46:bf";
        "dn42" = "aa:00:42:7a:32:46";
        "glotzbert" = "90:1b:0e:88:da:0a";
        # "wled-nix-snowflake" = "44:17:93:10:77:e8";
        # "wled-fairy-dust" = "3c:61:05:e3:2f:ad";
        # "wled-warnbert" = "3c:61:05:fc:21:37";
        # "wled-matrix" = "e8:db:84:e4:f4:30";
        # "ledball1" = "b8:27:eb:53:0b:27";
        # Beleuchtungskiste auf Traverse über Fernseher
        # "ledbeere" = "b8:27:eb:60:99:59";
        "pipebert" = "ec:a8:6b:fe:b4:cb";
        "pulsebert" = "b8:27:eb:16:31:61";
        # "ruststripe1" = "06:32:0e:39:21:69";
        "schalter" = "b8:27:eb:ac:65:d2";
      };
      time = 300;
      max-time = 30 * 24 * 3600;
      router = "c3d2-gw3";
    };
    domainName = "c3d2.zentralwerk.org";
    dynamicDomain = true;
    subnet4 = "172.22.99.0/24";
    hosts4 = {
      c3d2-anon = "172.22.99.1";
      c3d2-gw1 = "172.22.99.2";
      c3d2-gw2 = "172.22.99.3";
      c3d2-gw3 = "172.22.99.4";
      dacbert = "172.22.99.203";
      schalter = "172.22.99.204";
      glotzbert = "172.22.99.205";
      pulsebert = "172.22.99.208";
      pipebert = "172.22.99.209";
      # Mikrocontroller im ABB Metrawatt SE 790
      plotter = "172.22.99.210";
      bgp = "172.22.99.250";
      dn42 = "172.22.99.253";
    };
    ipv6Router = "c3d2-gw3";
    hosts6.dn42 = {
      bgp = "fd23:42:c3d2:523::c3d2:ff0b";
      c3d2-anon = "fd23:42:c3d2:523::c3d2:1";
      c3d2-gw1 = "fd23:42:c3d2:523::c3d2:2";
      c3d2-gw2 = "fd23:42:c3d2:523::c3d2:3";
      c3d2-gw3 = "fd23:42:c3d2:523::c3d2:4";
    };
    hosts6.up4 = {
      bgp = "2a00:8180:2c00:223::c3d2:ff0b";
      c3d2-anon = "2a00:8180:2c00:223::c3d2:1";
      c3d2-gw1 = "2a00:8180:2c00:223::c3d2:2";
      c3d2-gw2 = "2a00:8180:2c00:223::c3d2:3";
      c3d2-gw3 = "2a00:8180:2c00:223::c3d2:4";
      glotzbert = "2a00:8180:2c00:223:921b:eff:fe88:da0a";
      pipebert = "2a00:8180:2c00:223:eea8:6bff:fefe:b4cb";
    };
    subnets6 = {
      dn42 = "fd23:42:c3d2:523::/64";
      up4 = "2a00:8180:2c00:223::/64";
    };
  };

  site.hosts =
    let
      makeGateway = lib.recursiveUpdate {
        interfaces = {
          c3d2.type = "veth";
          core.type = "veth";
        };
        role = "container";
      };
    in
    {
    c3d2-anon = makeGateway {
      interfaces = {
        c3d2.hwaddr = "0A:14:48:01:07:05";
        core.hwaddr = "0A:14:48:01:07:04";
      };
      ospf.allowedUpstreams = [ "anon1" "freifunk" ];
    };
    c3d2-gw1 = makeGateway {
      interfaces = {
        c3d2.hwaddr = "0A:14:48:01:21:01";
        core.hwaddr = "0A:14:48:01:21:00";
      };
      ospf.allowedUpstreams = [ "flpk-gw" "freifunk" "upstream4" "upstream3" "anon1" ];
    };
    c3d2-gw2 = makeGateway {
      interfaces = {
        c3d2.hwaddr = "0A:14:48:01:21:03";
        core.hwaddr = "0A:14:48:01:21:02";
      };
      ospf.allowedUpstreams = [ "upstream3" "upstream4" "anon1" "freifunk" ];
    };
    c3d2-gw3 = makeGateway {
      interfaces = {
        c3d2.hwaddr = "0A:14:48:01:21:05";
        core.hwaddr = "0A:14:48:01:21:04";
      };
      ospf.allowedUpstreams = [ "upstream4" "upstream3" "anon1" "freifunk" ];
    };
  };
}
