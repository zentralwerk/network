{
  site.net.saaliot = {
    dhcp = {
      start = "192.168.38.2";
      end = "192.168.38.254";
      router = "siot-gw";
      server = "siot-gw";
      # devices don't often change and a missing DNS record causes trouble
      time = 3600;
      max-time = 24 * 3600;
    };
    dynamicDomain = true;
    domainName = "saaliot.zentralwerk.org";
    hosts4 = {
      siot-gw = "192.168.38.1";
    };
    hosts6 = {
      dn42 = {
        siot-gw = "fd23:42:c3d2:589:ffff:ffff:ffff:ffff";
      };
    };
    subnet4 = "192.168.38.0/24";
    subnets6 = {
      dn42 = "fd23:42:c3d2:589::/64";
      up4 = "2a00:8180:2c00:289::/64";
    };
  };

  site.hosts.siot-gw = {
    firewall = {
      enable = true;
      allowedNets = [ "priv17" ];
    };
    interfaces = {
      core = {
        hwaddr = "0A:22:48:01:24:03";
        type = "veth";
      };
      saaliot = {
        hwaddr = "0A:22:48:01:24:02";
        type = "veth";
      };
    };
    ospf = {
      allowedUpstreams = [ "upstream4" "upstream3" "anon1" ];
    };
    role = "container";
  };
}
