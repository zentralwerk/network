{
  site.net.roof = {
    # not switched, only a wifi
    vlan = null;
    # leave space for vxlan
    mtu = 1574;
    subnet4 = "10.0.57.0/24";
    hosts4 = {
      ap57 = "10.0.57.1";
      ap58 = "10.0.57.2";
    };
    hosts6.dn42 = {
      ap57 = "fd23:42:c3d2:584::39";
      ap58 = "fd23:42:c3d2:584::40";
    };
    subnets6.dn42 = "fd23:42:c3d2:584::/64";
  };
}
