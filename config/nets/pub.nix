{
  site.net.pub = {
    dhcp = {
      start = "172.20.78.2";
      end = "172.20.79.253";
      router = "pub-gw";
      server = "pub-gw";
      time = 120;
      max-time = 12 * 3600;
    };
    domainName = "pub.zentralwerk.org";
    dynamicDomain = true;
    hosts4 = {
      pub-gw = "172.20.78.1";
    };
    hosts6 = {
      dn42 = {
        pub-gw = "fd23:42:c3d2:583::1";
      };
      flpk = {
        pub-gw = "2a0f:5382:acab:1403::1";
      };
    };
    subnet4 = "172.20.78.0/23";
    subnets6 = {
      dn42 = "fd23:42:c3d2:583::/64";
      flpk = "2a0f:5382:acab:1403::/64";
    };
  };

  site.hosts.pub-gw = {
    interfaces = {
      core = {
        hwaddr = "0A:14:48:01:16:00";
        type = "veth";
      };
      pub = {
        hwaddr = "0A:14:48:01:16:01";
        type = "veth";
      };
    };
    ospf = {
      allowedUpstreams = [ "anon1" "freifunk" ];
      allowedUpstreams6 = [ "flpk-gw" "anon1" "freifunk" ];
    };
    role = "container";
  };
}
