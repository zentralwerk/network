{
  site.net.solar = {
    dhcp = {
      start = "192.168.40.2";
      end = "192.168.40.30";
      router = "solar-gw";
      server = "solar-gw";
      # devices don't often change and a missing DNS record causes trouble
      time = 3600;
      max-time = 24 * 3600;
    };
    dynamicDomain = true;
    domainName = "solar.zentralwerk.org";
    hosts4 = {
      solar-gw = "192.168.40.1";
    };
    subnet4 = "192.168.40.0/27";
    subnets6 = {
      up4 = "2a00:8180:2c00:290::/64";
    };
  };

  site.hosts.solar-gw = {
    firewall = {
      enable = true;
      allowedNets = [ ];
    };
    interfaces = {
      core = {
        hwaddr = "0A:22:48:01:24:05";
        type = "veth";
      };
      solar = {
        hwaddr = "0A:22:48:01:24:04";
        type = "veth";
      };
    };
    ospf = {
      allowedUpstreams = [ "upstream4" "upstream3" "anon1" ];
    };
    role = "container";
  };
}
