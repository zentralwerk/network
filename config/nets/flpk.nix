{
  site.net.flpk = {
    domainName = "flpk.zentralwerk.org";
    ipv6Router = "flpk-gw";
    subnet4 = "45.158.40.160/27";
    # we get a /56
    subnets6.flpk = "2a0f:5382:acab:1400::/64";
    hosts4 = {
      flpk-gw = "45.158.40.160";
      notice-me-senpai = "45.158.40.162"; # tlms monitoring
      sshlog = "45.158.40.163";
      caveman = "45.158.40.164";
      # "45.158.40.165" unused
      mastodon = "45.158.40.166";
      c3d2-web = "45.158.40.167";
      mail = "45.158.40.168";
      dresden-zone-dns = "45.158.40.169";
      # "45.158.40.170" unused
      rtrlab = "45.158.40.171"; # temporary
    };
    hosts6.flpk = {
      flpk-gw = "2a0f:5382:acab:1400::c3d2";
      notice-me-senpai = "2a0f:5382:acab:1400:2de:5bff:fef9:e23e"; # tlms-monitoring
      sshlog = "2a0f:5382:acab:1400::22";
      caveman = "2a0f:5382:acab:1400::a4";
      # "2a0f:5382:acab:1400::a5" unused
      mastodon = "2a0f:5382:acab:1400::a6";
      c3d2-web = "2a0f:5382:acab:1400::a7";
      # mail = "2a0f:5382:acab:1400::a8"; # we don't have an PTR for IPv6 and it gets way more often marked as spam
      dresden-zone-dns = "2a0f:5382:acab:1400::a9";
      # "2a0f:5382:acab:1400::aa" unused
      rtrlab = "2a0f:5382:acab:1400::ab";
    };
  };

  site.hosts.flpk-gw = {
    interfaces = {
      core = {
        hwaddr = "0A:14:48:b7:e4:91";
        type = "veth";
      };
      flpk = {
        hwaddr = "0A:14:48:01:16:01";
        type = "veth";
      };
      up-flpk = {
        type = "wireguard";
        mtu = 1372;
        upstream = {
          provider = "flpk";
          noNat = {
            subnets4 = [ "45.158.40.160/27" ];
            subnets6 = [ "2a0f:5382:acab:1400::/56" ];
          };
        };
      };
    };
    ospf = {
      allowedUpstreams = [ "upstream4" "upstream3" "freifunk" ];
      upstreamInstance = 2;
    };
    role = "container";
  };
}
