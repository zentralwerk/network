{
  site.hosts = {
    switch-a1 = {
      role = "switch";
      model = "HP-procurve-2824";
      location = "Saal A";
      interfaces = { mgmt.type = "phys"; };

      links = {
        switch-a2.ports = [ "7" ];
        priv25.ports = [
          # A6: Kleiner Saal Schaltschrank
          "1"
          # Kabinett A10
          "2"
          "3"
          # A16: Buehne rechts unten
          "4"
          # Panel A2: Foyer
          "8"
        ];
        priv31.ports = [
          # A4: Buero
          "6"
        ];
        saaliot.ports = [
          # Panel A8: Kleiner Saal Buehne
          "5"
        ];
        solar.ports = [
          "9" "10"
        ];

        # A17: Grosser Saal ueber der Buehne
        # switch-a2 Port 13
        # Panel A6: kl Saal hinten
      };
    };
    switch-a2 = {
      role = "switch";
      model = "dumb";
      location = "Saal A";

      links = {
        switch-c1.ports = [ "1" "1-1" ];
        switch-a1.ports = [ "2" ];
        switch-ds1.ports = [ "3" ];
        switch-ds2.ports = [ "4" ];
        ap44.ports = [ "10" ];
        ap45.ports = [ "11" ];
        ap46.ports = [ "12" ];
        ap47.ports = [ "13" ];
        ap48.ports = [ "14" ];
        ap49.ports = [ "15" ];
        ap50.ports = [ "16" ];
        ap52.ports = [ "17" ];
        # A14: projektor
        # A3: Techniklager
      };
    };

    switch-b3 = {
      role = "switch";
      model = "junos";
      location = "Haus B Souterrain";
      interfaces = { mgmt.type = "phys"; };

      # Ports 1-19 ungerade oben
      # Ports 2-20 gerade unten
      # (15, 16 gehen aktuell nach Haus A)
      # Ports 21-24 unten seitlich (optional optisch)
      # Port 7 geht aktuell nach Turm C Erdgeschoss und dadurch zur Ecce
      links = {
        ap23.ports = [ "ge-0/0/10" ];
        ap74.ports = [ "ge-0/0/16" ];
        iso1.ports = [ "ge-0/0/2" ];
        iso2.ports = [ "ge-0/0/3" ];
        iso3.ports = [ "ge-0/0/4" ];
        coloradio.ports = [
          # Patchpanel C8
          "ge-0/0/22"
        ];
        c3d2.ports = [
          "ge-0/0/5"
          "ge-0/0/8"
          # riscbert
          "ge-0/0/13"
        ];
        server1 = {
          group = "3";
          ports = [
            "ge-0/0/24"
            "ge-0/0/25"
            "ge-1/0/24"
            "ge-1/0/25"
          ];
        };
        server2 = {
          group = "1";
          ports = [
            "ge-0/0/38" "ge-0/0/39"
            "ge-1/0/32" "ge-1/0/33"
          ];
        };
        hydra = {
          group = "7";
          trunk = false;
          ports = [
            "ge-0/0/14" "ge-0/0/15"
            "ge-1/0/14" "ge-1/0/15"
          ];
        };
        server10 = {
          group = "5";
          ports = [
            "ge-0/0/36" "ge-0/0/37"
            "ge-1/0/36" "ge-1/0/37"
          ];
        };
        switch-c1 = {
          group = "2";
          ports = [ "ge-0/0/30" "ge-0/0/31" "ge-1/0/30" "ge-1/0/31" ];
        };
        switch-c3d2-main = {
          group = "4";
          ports = [
            "ge-0/0/26"
            "ge-0/0/27"
            "ge-1/0/26"
            "ge-1/0/27"
          ];
        };
        switch-d1.ports = [ "ge-0/0/34" ];

        ap1.ports = [ "ge-1/0/8" ];
        ap11.ports = [ "ge-1/0/10" ];
        ap34.ports = [ "ge-1/0/12" ];
        ap18.ports = [ "ge-1/0/18" ];
        ap29.ports = [ "ge-0/0/46" ];
        ap8.ports = [ "ge-1/0/22" ];
        ap35.ports = [ "ge-1/0/23" ];
        ap37.ports = [ "ge-1/0/39" ];
        ap63.ports = [ "ge-1/0/17" ];
        ap40.ports = [ "ge-1/0/21" ];
        ap41.ports = [ "ge-0/0/47" ];
        ap42.ports = [ "ge-1/0/6" ];
        ap5.ports = [ "ge-1/0/7" ];
        ap51.ports = [ "ge-1/0/13" ];
        ap53.ports = [ "ge-0/0/7" ];
        ap72.ports = [ "ge-1/0/38" ];
        ap55.ports = [ "ge-1/0/19" ];
        ap56.ports = [ "ge-1/0/9" ];
        ap60.ports = [ "ge-1/0/20" ];
        ap62.ports = [ "ge-0/0/11" ];
        ap65.ports = [ "ge-0/0/9" ];
        ap66.ports = [ "ge-1/0/43" ];
        mgmt.ports = [
          "ge-0/0/0"
          "ge-0/0/1"
          "ge-1/0/44"
          # server8
          "ge-1/0/47"
          # server9
          "ge-1/0/48"
        ];
        priv1.ports = [ "ge-1/0/3" ];
        priv19.ports = [ "ge-1/0/40" ];
        priv2.ports = [ "ge-1/0/4" ];
        priv24.ports = [ "ge-0/0/6" "ge-1/0/16" ];
        priv3.ports = [ "ge-1/0/5" ];
        priv30.ports = [ "ge-0/0/12" ];
        priv49.ports = [ "ge-1/0/1" ];
        ap67.ports = [ "ge-1/0/34" ];
        ap68.ports = [ "ge-1/0/35" ];
        ap69.ports = [ "ge-0/0/35" ];
        ap73.ports = [ "ge-0/0/45" ];
        pub.ports = [
          "ge-1/0/11"
        ];
        server9 = {
          group = "10";
          ports = [
            "ge-0/0/28"
            "ge-0/0/29"
            "ge-1/0/28"
            "ge-1/0/29"
          ];
        };
        server8 = {
          group = "6";
          ports = [
            "ge-0/0/41"
            "ge-0/0/42"
            "ge-1/0/41"
            "ge-1/0/42"
          ];
        };
      };
    };

    switch-c1 = {
      role = "switch";
      model = "HP-procurve-2824";
      location = "Turm C Keller, bei Kabelanschluessen";
      interfaces = { mgmt.type = "phys"; };

      links = {
        # Saal A: durch dummen PoE-Switch mit Aggregation an ap44-50,52 + switch-a1
        switch-a2 = {
          group = "1";
          ports = [ "15-16" ];
        };
        switch-b3 = {
          group = "2";
          ports = [ "21-24" ];
        };
        switch-dach.ports = [ "6" ];
        # Vodafone Modems
        up1.ports = [ "1" ];
        up2.ports = [ "2" ];
        # DSI
        up4.ports = [ "4" ];
        # Turm C APs
        ap17.ports = [ "19" ];
        ap19.ports = [ "17" ];
        ap26.ports = [ "18" ];
        ap38.ports = [ "7" ];
        ap61.ports = [ "13" ];
        # Iso nets
        iso1.ports = [ "9" ];
        iso2.ports = [ "10" ];
        iso3.ports = [ "11" ];
        iso4.ports = [ "12" ];
        iso6.ports = [ "14" ];
        # Saal Foyer
        priv25.ports = [ "20" ];
      };
    };

    switch-c3d2-main = {
      role = "switch";
      # in reality: HP-procurve-2848
      model = "HP-procurve-2824";
      location = "C3D2";
      interfaces = { mgmt.type = "phys"; };

      links = {
        switch-b3 = {
          group = "1";
          ports = [ "1-4" ];
        };
        mgmt.ports = [ "5" ];
        # SDK
        ap2.ports = [ "13" ];
        # AP ceiling entrance
        ap31.ports = [ "14" ];
        # /proc Fenster
        ap33.ports = [ "15" ];
        # Datenspuren: VOC
        iso4.ports = [ "19" ];
        # Datenspuren: POC
        iso5.ports = [ "20" ];
        # Lino's Playground Server
        play.ports = [ "23-24" ];
        # general c3d2 network
        c3d2.ports = [ "25-48" ];
      };
    };

    switch-d1 = {
      role = "switch";
      model = "TL-SG3210";
      location = "Turm D Elektroraum";
      interfaces = { mgmt.type = "phys"; };

      links = {
        switch-b3 = {
          group = "1";
          ports = [ "1" ];
        };
        switch-d2.ports = [ "3" ];

        # Turm D APs
        ap7.ports = [ "8" ];
        ap9.ports = [ "5" ];
        ap10.ports = [ "4" ];
        ap12.ports = [ "7" ];
        ap22.ports = [ "2" ];
      };
    };

    switch-d2 = {
      role = "switch";
      model = "dumb";
      location = "Turm D Durchgang 1. Etage";

      links = {
        switch-d1 = {
          group = "1";
          ports = [ "12" ];
        };
        ap3.ports = [ "1" ];
        ap59.ports = [ "2" ];
      };
    };

    switch-dach = {
      role = "switch";
      model = "HP-procurve-2824";
      location = "Dach";
      interfaces = { mgmt.type = "phys"; };

      links = {
        mgmt.ports = [ "1" ];
        switch-c1.ports = [ "24" ];
        # Freifunk nodes
        bmx.ports = [ "12,14,16" ];
        # radiobert chirp lorawan
        serv.ports = [ "7,15,17" ];
        # Starlink
        up3.ports = [ "3" ];
        # unifiac-mesh
        ap57.ports = [ "10" ];
        # TLMS tetra and traffic-stop-box
        c3d2.ports = [ "19,20" ];
      };
    };

    switch-ds1 = {
      role = "switch";
      model = "3com-5500G";
      location = "Großer Saal-hinter Buehne";
      interfaces = { mgmt.type = "phys"; };

      links = {
        # Public
        pub.ports = [
          "GigabitEthernet1/0/1"
          "GigabitEthernet1/0/2"
          "GigabitEthernet1/0/3"
          "GigabitEthernet1/0/4"
          "GigabitEthernet1/0/5"
          "GigabitEthernet1/0/6"
          "GigabitEthernet1/0/7"
          "GigabitEthernet1/0/8"
          "GigabitEthernet1/0/9"
          "GigabitEthernet1/0/10"
          "GigabitEthernet1/0/11"
          "GigabitEthernet1/0/12"
          "GigabitEthernet1/0/13"
          "GigabitEthernet1/0/14"
        ];
        # Stage uplink
        priv25.ports = [
          "GigabitEthernet1/0/16"
          "GigabitEthernet1/0/17"
          "GigabitEthernet1/0/18"
          "GigabitEthernet1/0/19"
        ];
        # VOC isolated
        iso4.ports = [
          # voc Kabinett
          "GigabitEthernet1/0/20"
          "GigabitEthernet1/0/22"
        ];
        # "GigabitEthernet1/0/21"
        # POC isolated
        iso5.ports = [
          "GigabitEthernet1/0/23"
          "GigabitEthernet1/0/15"
        ];
        # Uplink
        switch-a2.ports = [ "GigabitEthernet1/0/24" ];
      };
    };

    switch-ds2 = {
      role = "switch";
      model = "3com-5500G";
      location = "Grosser Saal oben";
      interfaces = { mgmt.type = "phys"; };

      links = {
        # Public
        pub.ports = [
          "GigabitEthernet1/0/1"
          "GigabitEthernet1/0/2"
          "GigabitEthernet1/0/3"
          "GigabitEthernet1/0/4"
          "GigabitEthernet1/0/5"
          "GigabitEthernet1/0/6"
          "GigabitEthernet1/0/7"
          "GigabitEthernet1/0/8"
          "GigabitEthernet1/0/9"
          "GigabitEthernet1/0/10"
          "GigabitEthernet1/0/11"
          "GigabitEthernet1/0/12"
          "GigabitEthernet1/0/13"
          "GigabitEthernet1/0/14"
          "GigabitEthernet1/0/15"
          "GigabitEthernet1/0/16"
          "GigabitEthernet1/0/17"
          "GigabitEthernet1/0/18"
          "GigabitEthernet1/0/19"
          "GigabitEthernet1/0/20"
          "GigabitEthernet1/0/21"
          "GigabitEthernet1/0/22"
        ];
        # POC isolated
        iso5.ports = [
          "GigabitEthernet1/0/23"
        ];
        # Uplink
        switch-a2.ports = [ "GigabitEthernet1/0/24" ];
      };
    };

    switch-ds3 = {
      role = "switch";
      model = "3com-5500G";
      location = "Kleiner Saal";
      interfaces = { mgmt.type = "phys"; };

      links = {
        # Public
        pub.ports = [
          "GigabitEthernet1/0/1"
          "GigabitEthernet1/0/2"
          "GigabitEthernet1/0/3"
          "GigabitEthernet1/0/4"
          "GigabitEthernet1/0/5"
          "GigabitEthernet1/0/6"
          "GigabitEthernet1/0/7"
          "GigabitEthernet1/0/8"
          "GigabitEthernet1/0/9"
          "GigabitEthernet1/0/10"
          "GigabitEthernet1/0/11"
          "GigabitEthernet1/0/12"
          "GigabitEthernet1/0/13"
          "GigabitEthernet1/0/14"
          "GigabitEthernet1/0/15"
          "GigabitEthernet1/0/16"
          "GigabitEthernet1/0/17"
          "GigabitEthernet1/0/18"
          "GigabitEthernet1/0/19"
        ];
        # Stage uplink
        priv25.ports = [
          "GigabitEthernet1/0/20"
          "GigabitEthernet1/0/21"
        ];
        # VOC isolated
        iso4.ports = [
          "GigabitEthernet1/0/22"
          "GigabitEthernet1/0/23"
        ];
        # Uplink
        switch-a2.ports = [ "GigabitEthernet1/0/24" ];
      };
    };
  };
}
