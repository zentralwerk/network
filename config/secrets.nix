# Dummy secrets for testing
{
  site.net = {
    core.ospf.secret = "encrypted";
    pub.wifi.ieee80211rKey = "2dc40abba46da9490ea0e00f93f18ce5";
    c3d2.wifi.ieee80211rKey = "d1b1fa2461efc0df9e2d96579607b7f6";
  };

  site.hosts = {
    ap1.password = "encrypted";
    ap2.password = "encrypted";
    ap3.password = "encrypted";
    ap4.password = "encrypted";
    ap5.password = "encrypted";
    ap6.password = "encrypted";
    ap7.password = "encrypted";
    ap8.password = "encrypted";
    ap9.password = "encrypted";
    ap10.password = "encrypted";
    ap11.password = "encrypted";
    ap12.password = "encrypted";
    ap15.password = "encrypted";
    ap17.password = "encrypted";
    ap18.password = "encrypted";
    ap19.password = "encrypted";
    ap21.password = "encrypted";
    ap22.password = "encrypted";
    ap23.password = "encrypted";
    ap24.password = "encrypted";
    ap25.password = "encrypted";
    ap26.password = "encrypted";
    ap27.password = "encrypted";
    ap28.password = "encrypted";
    ap29.password = "encrypted";
    ap30.password = "encrypted";
    ap31.password = "encrypted";
    ap32.password = "encrypted";
    ap33.password = "encrypted";
    ap34.password = "encrypted";
    ap35.password = "encrypted";
    ap36.password = "encrypted";
    ap37.password = "encrypted";
    ap38.password = "encrypted";
    ap40.password = "encrypted";
    ap41.password = "encrypted";
    ap42.password = "encrypted";
    ap44.password = "encrypted";
    ap45.password = "encrypted";
    ap46.password = "encrypted";
    ap47.password = "encrypted";
    ap48.password = "encrypted";
    ap49.password = "encrypted";
    ap50.password = "encrypted";
    ap51.password = "encrypted";
    ap52.password = "encrypted";
    ap53.password = "encrypted";
    ap54.password = "encrypted";
    ap55.password = "encrypted";
    ap56.password = "encrypted";
    ap57.password = "encrypted";
    ap58.password = "encrypted";
    ap59.password = "encrypted";
    ap60.password = "encrypted";
    ap61.password = "encrypted";
    ap63.password = "encrypted";
    ap64.password = "encrypted";
    ap65.password = "encrypted";
    ap66.password = "encrypted";
    ap67.password = "encrypted";
    ap68.password = "encrypted";
    ap69.password = "encrypted";
    ap70.password = "encrypted";
    ap71.password = "encrypted";
    ap72.password = "encrypted";
    ap73.password = "encrypted";
    ap74.password = "encrypted";
    switch-a1.password = "encrypted";
    switch-b1.password = "encrypted";
    switch-b2.password = "encrypted";
    switch-b3.password = "encrypted";
    switch-c1.password = "encrypted";
    switch-c3d2-main.password = "encrypted";
    switch-d1.password = "encrypted";
    switch-dach.password = "encrypted";
    switch-ds1.password = "encrypted";
    switch-ds2.password = "encrypted";
    switch-ds3.password = "encrypted";

    upstream4.interfaces.up4-pppoe.upstream = {
      user = "encrypted";
      password = "encrypted";
    };

    anon1.interfaces.njalla.wireguard = {
      addresses = [ "fec0::1/64" "192.168.0.1/24" ];
      endpoint = "0.0.0.1";
      privateKey = "encrypted";
      publicKey = "encrypted";
    };
    flpk-gw.interfaces.up-flpk.wireguard = {
      addresses = [ "fec0::1/64" "192.168.0.1/24" ];
      endpoint = "0.0.0.1";
      privateKey = "encrypted";
      publicKey = "encrypted";
    };

    ap1.wifi."platform/qca953x_wmac".ssids."uebergangsnetz".psk = "encrypted";
    ap10.wifi."platform/qca953x_wmac".ssids = {
      "iz-dresden.org".psk = "encrypted";
    };
    ap11.wifi."platform/ahb/18100000.wmac".ssids."braeunigkoschnik".psk = "encrypted";
    ap12.wifi."platform/ar934x_wmac".ssids = {
      "IrèneMélix".psk = "encrypted";
      "paperheart".psk = "encrypted";
      "hanyo".psk = "encrypted";
    };
    ap15.wifi."platform/ahb/18100000.wmac".ssids."etz250".psk = "encrypted";
    ap17.wifi."platform/ahb/18100000.wmac".ssids = {
      "EDUB".psk = "encrypted";
      "Zweitwohnsitz".psk = "encrypted";
      "e-Stuetzpunkt".psk = "encrypted";
      "Weltalmanach".psk = "encrypted";
    };
    ap18.wifi."platform/qca953x_wmac".ssids."Restaurierung Wolff/Kober".psk = "encrypted";
    ap19.wifi."platform/qca953x_wmac".ssids = {
      "Bockwurst".psk = "encrypted";
      "Walter".psk = "encrypted";
    };
    ap2.wifi = {
      "pci0000:00/0000:00:00.0".ssids."C3D2".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids = {
        "C3D2 legacy".psk = "encrypted";
        "C3D2 IoT".psk = "encrypted";
      };
    };
    ap23.wifi = {
      "pci0000:00/0000:00:00.0".ssids."LBK Network".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."LBK Network".psk = "encrypted";
    };
    ap24.wifi."platform/ar933x_wmac".ssids."farbwerk".psk = "encrypted";
    ap25.wifi."platform/ar933x_wmac".ssids."farbwerk".psk = "encrypted";
    ap26.wifi."pci0000:00/0000:00:00.0".ssids."Dezember".psk = "encrypted";
    ap29.wifi = {
      "pci0000:00/0000:00:00.0".ssids."jungnickel-fotografie".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."jungnickel-fotografie".psk = "encrypted";
    };
    ap3.wifi = {
      "pci0000:00/0000:00:00.0".ssids."C3D2".psk = "encrypted";
      "platform/ar934x_wmac".ssids."C3D2 legacy".psk = "encrypted";
    };
    ap31.wifi = {
      "pci0000:00/0000:00:00.0".ssids."C3D2".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids = {
        "C3D2 legacy" = { "psk" = "encrypted"; };
        "C3D2 IoT" = { "psk" = "encrypted"; };
        "FOTOAKADEMIEdd" = { "psk" = "encrypted"; };
      };
    };
    ap32.wifi = {
      "pci0000:00/0000:00:00.0".ssids."ZW_stage".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."ZW_stage_legacy".psk = "encrypted";
    };
    ap33.wifi = {
      "pci0000:00/0000:00:00.0".ssids."C3D2".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."C3D2 legacy".psk = "encrypted";
    };
    ap34.wifi."pci0000:00/0000:00:00.0".ssids."etz250".psk = "encrypted";
    ap34.wifi."platform/ahb/18100000.wmac".ssids."etz250".psk = "encrypted";
    ap35.wifi."platform/ahb/18100000.wmac".ssids."Koch".psk = "encrypted";
    ap36.wifi."platform/ar933x_wmac".ssids."C3D2 legacy".psk = "encrypted";
    ap37.wifi = {
      "pci0000:00/0000:00:00.0".ssids."hechtfilm.de".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."hechtfilm.de legacy".psk = "encrypted";
    };
    ap38.wifi = {
      "pci0000:00/0000:00:00.0".ssids = {
        "plop" = { "psk" = "encrypted"; };
      };
      "platform/ahb/18100000.wmac".ssids = {
        "plop" = { "psk" = "encrypted"; };
        "millimeter" = { "psk" = "encrypted"; };
      };
    };
    ap4.wifi."platform/ahb/18100000.wmac".ssids."jam-circle.de".psk = "encrypted";
    ap40.wifi = {
      "pci0000:00/0000:00:00.0".ssids."M".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."M legacy".psk = "encrypted";
    };
    ap41.wifi = {
      "pci0000:00/0000:00:00.0".ssids."Walter".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."Walter".psk = "encrypted";
    };
    ap42.wifi = {
      "pci0000:00/0000:00:00.0".ssids."jam-circle.de".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."jam-circle.de legacy".psk = "encrypted";
    };
    ap44.wifi = {
      "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0".ssids."ZW_stage_legacy".psk = "encrypted";
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0".ssids."ZW_stage".psk = "encrypted";
    };
    ap45.wifi = {
      "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0".ssids."ZW_stage_legacy".psk = "encrypted";
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0".ssids."ZW_stage".psk = "encrypted";
    };
    ap46.wifi = {
      "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0".ssids = {
        "ZW_stage_legacy".psk = "encrypted";
      };
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0".ssids = {
        "ZW_stage".psk = "encrypted";
      };
    };
    ap47.wifi = {
      "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0".ssids."ZW_stage_legacy".psk = "encrypted";
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0".ssids."ZW_stage".psk = "encrypted";
    };
    ap48.wifi = {
      "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0".ssids."ZW_stage_legacy".psk = "encrypted";
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0".ssids."ZW_stage".psk = "encrypted";
    };
    ap49.wifi = {
      "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0".ssids."ZW_stage_legacy".psk = "encrypted";
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0".ssids."ZW_stage".psk = "encrypted";
    };
    ap5.wifi."platform/ahb/18100000.wmac".ssids."verbalwerk.de".psk = "encrypted";
    ap50.wifi = {
      "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0".ssids = {
        "ZW_stage_legacy".psk = "encrypted";
        "gerdwork".psk = "encrypted";
      };
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0".ssids."ZW_stage".psk = "encrypted";
    };
    ap51.wifi = {
      "pci0000:00/0000:00:00.0".ssids."antrares".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."antrares".psk = "encrypted";
    };
    ap52.wifi = {
      "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0".ssids."ZW_stage_legacy".psk = "encrypted";
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0".ssids."ZW_stage".psk = "encrypted";
    };
    ap53.wifi."platform/qca953x_wmac".ssids."Karen Koschnick".psk = "encrypted";
    ap54.wifi = {
      "pci0000:00/0000:00:00.0".ssids."Abyssinia".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."Abyssinia".psk = "encrypted";
    };
    ap55.wifi = {
      "pci0000:00/0000:00:00.0".ssids."MagLAN".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."MagLAN (legacy)".psk = "encrypted";
    };
    ap56.wifi = {
      "pci0000:00/0000:00:00.0".ssids."MagLAN".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."MagLAN (legacy)".psk = "encrypted";
    };
    ap57.wifi = {
      "pci0000:00/0000:00:00.0".ssids."Zentralwerk".psk = "encrypted";
    };
    ap58.wifi = {
      "pci0000:00/0000:00:00.0".ssids."Zentralwerk".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."LIZA".psk = "encrypted";
    };
    ap59.wifi = {
      "pci0000:00/0000:00:00.0".ssids."Ebs 5000".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."Ebs 2000".psk = "encrypted";
    };
    ap60.wifi = {
      "pci0000:00/0000:00:00.0".ssids."Abyssinia".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."Abyssinia".psk = "encrypted";
    };
    ap61.wifi = {
      "pci0000:00/0000:00:00.0".ssids."tomiru".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."tomiru".psk = "encrypted";
    };
    ap62.wifi = {
      "pci0000:00/0000:00:00.0".ssids."Wolke7".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."Wolke7 legacy".psk = "encrypted";
    };
    ap63.wifi = {
      "pci0000:00/0000:00:00.0".ssids."EckiTino".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."EckiTino legacy".psk = "encrypted";
    };
    ap64.wifi = {
      "platform/ahb/18100000.wmac".ssids."Princess Castle".psk = "encrypted";
    };
    ap65.wifi = {
      "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0".ssids = {
        "farbwerk".psk = "encrypted";
        "Kaffeetasse".psk = "encrypted";
      };
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0".ssids."farbwerk".psk = "encrypted";
    };
    ap66.wifi = {
      "pci0000:00/0000:00:00.0".ssids."Buschfunk4.03".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."Buschfunk4.03 legacy".psk = "encrypted";
    };
    ap67.wifi = {
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0".ssids."farbwerk".psk = "encrypted";
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0+1".ssids."farbwerk".psk = "encrypted";
    };
    ap68.wifi = {
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0".ssids."farbwerk".psk = "encrypted";
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0+1".ssids."farbwerk".psk = "encrypted";
    };
    ap69.wifi = {
      "pci0000:00/0000:00:00.0".ssids."LIZA".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."LIZA".psk = "encrypted";
    };
    ap7.wifi."platform/qca953x_wmac".ssids."mino".psk = "encrypted";
    ap70.wifi = {
      "pci0000:00/0000:00:00.0".ssids."M".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."M legacy".psk = "encrypted";
    };
    ap72.wifi = {
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0".ssids."farbwerk".psk = "encrypted";
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0+1".ssids."farbwerk".psk = "encrypted";
    };
    ap73.wifi = {
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0".ssids."Princess Castle".psk = "encrypted";
      "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0+1".ssids."Princess Castle".psk = "encrypted";
    };
    ap74.wifi = {
      "platform/soc/18000000.wifi".ssids = {
        "teknologi".psk = "encrypted";
      };
    };
    ap8.wifi = {
      "pci0000:00/0000:00:00.0".ssids."WLANb0402".psk = "encrypted";
      "platform/ahb/18100000.wmac".ssids."WLANb0402".psk = "encrypted";
    };
    ap9.wifi."platform/qca953x_wmac".ssids."Herzzbuehne".psk = "encrypted";
  };

  site = {
    dyndnsKey = "oYmxXCIa0nArp0679L6v+y/UfnhripOudLv+R5Cop8I=";
    portunusKey = "oYmxXCIa0nArp0679L6v+y/UfnhripOudLv+R5Cop8I=";
  };

  site.vpn.wireguard = {
    privateKey = "wPNXY4ED3Jz3Kz0KOmvfQOou6/wHrgqSsykaMYrtb28=";
    peers = [ {
      # privateKey: GOdfeizQZjPmyYnh3LMI3LrYeEtqYMyOvK8KASVgI1Q=
      publicKey = "4aTjdm/APMTERczvtnLXRFYjSWYsmwPFTumjyno4nx4=";
      allowedIPs = [
        "172.20.76.226"
        "fd23:42:c3d2:585::/64"
        "2a00:8180:2c00:285::/64"
      ];
    } ];
  };
}
