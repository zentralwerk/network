{ config, ... }:
{
  site.hosts = {
    ap-test1 = {
      interfaces = {
        bmx.type = "bridge";
        c3d2.type = "bridge";
        mgmt.type = "phys";
        pub.type = "bridge";
      };
    };
    ap1 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv6.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv6 = {
          ports = [ "lan:4" "lan:3" "lan:2" "lan:1" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "weg";
      model = "tl-wr841-v10";
      role = "ap";
      wifi = {
        "platform/qca953x_wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            uebergangsnetz = { net = "priv6"; };
          };
        };
      };
    };
    ap10 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv15.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        pub = {
          ports = [ "lan:4" "lan:3" "lan:2" "lan:1" ];
        };
        switch-d1 = {
          ports = [ "wan" ];
        };
      };
      location = "Turm D, 1. Etage";
      model = "tl-wr841-v9";
      role = "ap";
      wifi = {
        "platform/qca953x_wmac" = {
          channel = 6;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            "iz-dresden.org" = { net = "priv15"; encryption = "wpa2"; };
          };
        };
      };
    };
    ap11 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv8.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv8 = {
          ports = [ "lan" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "B 2.03.04";
      model = "tplink_tl-wr1043nd-v2";
      role = "ap";
      wifi = {
        "platform/ahb/18100000.wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            braeunigkoschnik = { net = "priv8"; };
          };
        };
      };
    };
    ap12 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv28.type = "bridge";
        priv38.type = "bridge";
        priv42.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        pub = {
          ports = [ "lan:4" "lan:1" "lan:2" "lan:3" ];
        };
        switch-d1 = {
          ports = [ "wan" ];
        };
      };
      location = "Turm D, 4. Etage";
      model = "tl-wr841-v8";
      role = "ap";
      wifi = {
        "platform/ar934x_wmac" = {
          channel = 6;
          htmode = "HT20";
          ssids = {
            hanyo = { net = "priv28"; encryption = "wpa2"; };
            "IrèneMélix" = { net = "priv38"; encryption = "wpa2"; };
            "ZW public" = { net = "pub"; };
            paperheart = { net = "priv42"; encryption = "wpa2"; };
          };
        };
      };
    };
    ap15 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv10.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv10 = {
          ports = [ "lan" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "B4.09.01";
      model = "tplink_tl-wr1043nd-v3";
      role = "ap";
      wifi = {
        "platform/ahb/18100000.wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            etz250 = { net = "priv10"; };
          };
        };
      };
    };
    ap17 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv29.type = "bridge";
        priv33.type = "bridge";
        priv34.type = "bridge";
        priv36.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv33.ports = [ "lan:3" "lan:4" ];
        priv36.ports = [ "lan:1" "lan:2" ];
        switch-c1.ports = [ "wan" ];
      };
      location = "Turm C, 2. Etage";
      model = "tplink_tl-wr1043nd-v3";
      role = "ap";
      wifi = {
        "platform/ahb/18100000.wmac" = {
          channel = 5;
          htmode = "HT20";
          ssids = {
            EDUB = { net = "priv33"; };
            "ZW public" = { net = "pub"; };
            Zweitwohnsitz = { net = "priv34"; };
            e-Stuetzpunkt = { net = "priv29"; };
            Weltalmanach = { net = "priv36"; };
          };
        };
      };
    };
    ap18 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv9.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv9 = {
          ports = [ "lan:4" "lan:3" "lan:2" "lan:1" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "Haus B, 2. Etage, zum Innenhof";
      model = "tl-wr841-v10";
      role = "ap";
      wifi = {
        "platform/qca953x_wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "Restaurierung Wolff/Kober" = { net = "priv9"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap19 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv26.type = "bridge";
        priv41.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        pub = {
          ports = [ "lan:4" "lan:3" "lan:2" "lan:1" ];
        };
        switch-c1 = {
          ports = [ "wan" ];
        };
      };
      location = "Turm C oberste Etage";
      model = "tl-wr841-v11";
      role = "ap";
      wifi = {
        "platform/qca953x_wmac" = {
          channel = 6;
          htmode = "HT20";
          ssids = {
            "Bockwurst" = { net = "priv41"; encryption = "wpa2"; };
            Walter = { net = "priv26"; encryption = "wpa2"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap2 = {
      interfaces = {
        c3d2.type = "bridge";
        c3d2iot.type = "bridge";
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        pub.type = "bridge";
      };
      links = {
        c3d2 = {
          ports = [ "lan" ];
        };
        switch-c3d2-main = {
          ports = [ "wan" ];
        };
      };
      location = "C3D2 Backstage";
      model = "tplink_archer-c7-v2";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            C3D2 = { net = "c3d2"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "C3D2 legacy" = { net = "c3d2"; };
            "C3D2 IoT" = {
              net = "c3d2iot";
              hidden = true;
              disassocLowAck = false;
            };
            "ZW public legacy" = { net = "pub"; };
          };
        };
      };
    };
    ap21 = {
      interfaces = {
        c3d2.type = "bridge";
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        pub.type = "bridge";
      };
      links = {
        ap42.ports = [ "lan" ];
      };
      location = "Hof (on events)";
      model = "ubnt_unifiac-lite";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 149;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 5;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap22 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        pub.type = "bridge";
      };
      links = {
        pub = {
          ports = [ "lan" ];
        };
        switch-d1 = {
          ports = [ "wan" ];
        };
      };
      location = "Haus B Souterrain unter der Treppe an Turm D";
      model = "tl-wr740n-v1";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 11;
          htmode = "HT20";
          ssids = { "ZW public" = { net = "pub"; }; };
        };
      };
    };
    ap23 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv30.type = "bridge";
        pub.type = "bridge";
      };
      links.switch-b3.ports = [ "lan" ];
      location = "Seminarraum, Haus B";
      model = "ubnt_unifiac-lite";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            "LBK Network" = { net = "priv30"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "LBK Network" = { net = "priv30"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap24 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv12.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        # Ends up in /etc/config but not in `swconfig dev switch0 show`
        priv12.ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        switch-b3.ports = [ "wan" ];
      };
      location = "Farbwerk";
      model = "tl-wr740n-v4";
      role = "ap";
      wifi = {
        "platform/ar933x_wmac" = {
          channel = 6;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            farbwerk = { net = "priv12"; encryption = "wpa2"; };
          };
        };
      };
    };
    ap25 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv12.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv12 = {
          ports = [ "lan" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "Farbwerk, lost";
      model = "tl-wr740n-v1";
      role = "ap";
      wifi = {
        "platform/ar933x_wmac" = {
          channel = 6;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            farbwerk = { net = "priv12"; };
          };
        };
      };
    };
    ap26 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv37.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        pub = {
          ports = [ "lan" ];
        };
        switch-c1 = {
          ports = [ "wan" ];
        };
      };
      location = "Turm C, 1. Etage";
      model = "tl-wr740n-v1";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 11;
          htmode = "HT20";
          ssids = {
            Dezember = { net = "priv37"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap27 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        pub.type = "bridge";
      };
      links = {
        pub = {
          ports = [ "lan:4" "lan:3" "lan:2" "lan:1" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "Weg?";
      model = "tl-wr841-v10";
      role = "ap";
      wifi = {
        "platform/qca953x_wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = { "ZW public" = { net = "pub"; }; };
        };
      };
    };
    ap28 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        pub.type = "bridge";
      };
      links = {
        ap3 = {
          ports = [ "wan" ];
        };
        pub = {
          ports = [ "lan:4" "lan:1" "lan:2" "lan:3" ];
        };
      };
      location = "Tunnel";
      model = "tl-wr841-v8";
      role = "ap";
      wifi = {
        "platform/ar934x_wmac" = {
          channel = 9;
          htmode = "HT20";
          ssids = { "ZW public" = { net = "pub"; }; };
        };
      };
    };
    ap29 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv13.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv13 = {
          ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "B1.05.07";
      model = "tplink_archer-c7-v4";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            jungnickel-fotografie = { net = "priv13"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 6;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            jungnickel-fotografie = { net = "priv13"; };
          };
        };
      };
    };
    ap3 = {
      interfaces = {
        c3d2.type = "bridge";
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        pub.type = "bridge";
      };
      links = {
        ap28 = {
          ports = [ "lan:1" ];
        };
        c3d2 = {
          ports = [ "lan:2" "lan:3" "lan:4" ];
        };
        switch-d2 = {
          ports = [ "wan" ];
        };
      };
      location = "C3D2 Keller";
      model = "tplink_tl-wdr4300-v1";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 128;
          htmode = "HT20";
          ssids = {
            C3D2 = { net = "c3d2"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ar934x_wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "C3D2 legacy" = { net = "c3d2"; };
            "ZW public legacy" = { net = "pub"; };
          };
        };
      };
    };
    ap30 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv14.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv14 = {
          ports = [ "lan:4" "lan:3" "lan:2" "lan:1" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "B 4.02 (old)";
      model = "tplink_tl-wr1043nd-v4";
      role = "ap";
      wifi = {
        "platform/ahb/18100000.wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            WLANb0402 = { net = "priv14"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap31 = {
      interfaces = {
        c3d2.type = "bridge";
        c3d2iot.type = "bridge";
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv39.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        switch-c3d2-main = {
          ports = [ "lan" ];
        };
      };
      location = "C3D2 Assembly";
      model = "ubnt_unifiac-lite";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 149;
          htmode = "VHT80";
          ssids = {
            C3D2 = { net = "c3d2"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 5;
          htmode = "HT20";
          ssids = {
            "C3D2 legacy" = { net = "c3d2"; };
            "C3D2 IoT" = {
              net = "c3d2iot";
              hidden = true;
              disassocLowAck = false;
            };
            FOTOAKADEMIEdd = { net = "priv39"; };
            "ZW public legacy" = { net = "pub"; };
          };
        };
      };
    };
    ap32 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv25.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        switch-b3 = {
          ports = [ "lan" ];
        };
      };
      location = "Auf Lager";
      model = "ubnt_unifiac-lite";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 149;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            "ZW_stage" = { net = "priv25"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 9;
          htmode = "HT20";
          ssids = {
            "ZW public legacy" = { net = "pub"; };
            "ZW_stage_legacy" = { net = "priv25"; };
          };
        };
      };
    };
    ap33 = {
      interfaces = {
        c3d2.type = "bridge";
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        pub.type = "bridge";
      };
      links = {
        switch-c3d2-main = {
          ports = [ "lan" ];
        };
      };
      location = "C3D2 Podest/Hinterhof";
      model = "ubnt_unifiac-lite";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 100;
          htmode = "VHT80";
          ssids = {
            C3D2 = { net = "c3d2"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 9;
          htmode = "HT20";
          ssids = {
            "C3D2 legacy" = { net = "c3d2"; };
            "ZW public legacy" = { net = "pub"; };
          };
        };
      };
    };
    ap34 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        pub.type = "bridge";
        priv10.type = "bridge";
      };
      links = {
        switch-b3 = {
          ports = [ "lan" ];
        };
      };
      location = "B4.09.01";
      model = "ubnt_unifiac-lite";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 149;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            etz250 = { net = "priv10"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 9;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            etz250 = { net = "priv10"; };
          };
        };
      };
    };
    ap35 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv18.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv18 = {
          ports = [ "lan:4" "lan:3" "lan:2" "lan:1" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "B 4.08";
      model = "tplink_tl-wr1043n-v5";
      role = "ap";
      wifi = {
        "platform/ahb/18100000.wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            Koch = { net = "priv18"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap36 = {
      interfaces = {
        c3d2.type = "bridge";
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        pub.type = "bridge";
      };
      links = {
        pub = {
          ports = [ "lan" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "Reserve";
      model = "tl-wr740n-v1";
      role = "ap";
      wifi = {
        "platform/ar933x_wmac" = {
          channel = 5;
          htmode = "HT20";
          ssids = {
            "C3D2 legacy" = { net = "c3d2"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap37 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv19.type = "bridge";
        priv43.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv19.ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        switch-b3.ports = [ "wan" ];
      };
      location = "B3.11.01";
      model = "tplink_archer-c7-v5";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            "hechtfilm.de" = { net = "priv19"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 6;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            "hechtfilm.de legacy" = { net = "priv19"; };
          };
        };
      };
    };
    ap38 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv20.type = "bridge";
        priv47.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        pub = {
          ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        };
        switch-c1 = {
          ports = [ "wan" ];
        };
      };
      location = "ECCE-Raum";
      model = "tplink_archer-c7-v4";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            plop = { net = "priv20"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 11;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            plop = { net = "priv20"; };
            millimeter = { net = "priv47"; };
          };
        };
      };
    };
    ap4 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv4.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv4 = {
          ports = [ "lan" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "Returned";
      model = "tplink_tl-wr1043nd-v2";

      role = "ap";
      wifi = {
        "platform/ahb/18100000.wmac" = {
          channel = 11;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            "jam-circle.de" = { net = "priv4"; };
          };
        };
      };
    };
    ap40 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv22.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv22.ports = [ "lan:2" "lan:3" "lan:4" ];
        ap70.ports = [ "lan:1" ];
        switch-b3.ports = [ "wan" ];
      };
      location = "B4.01";
      model = "tplink_archer-c7-v5";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 128;
          htmode = "VHT80";
          ssids = {
            M = { net = "priv22"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 6;
          htmode = "HT20";
          ssids = {
            "M legacy" = { net = "priv22"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap41 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv26.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv26 = {
          ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "B3.01";
      model = "tplink_archer-c7-v5";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 128;
          htmode = "VHT80";
          ssids = {
            Walter = { net = "priv26"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 6;
          htmode = "HT20";
          ssids = {
            Walter = { net = "priv26"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap42 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv4.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv4.ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        switch-b3.ports = [ "wan" ];
      };
      location = "Dresden School of Lindy Hop";
      model = "tplink_archer-c7-v5";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 128;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            "jam-circle.de" = { net = "priv4"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 11;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            "jam-circle.de legacy" = { net = "priv4"; };
          };
        };
      };
    };
    ap44 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv25.type = "bridge";
        saaliot.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        switch-a2 = {
          ports = [ "lan" ];
        };
      };
      location = "Saal A vorn";
      model = "ubnt_unifi-nanohd";
      role = "ap";
      wifi = {
        "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "ZW public legacy" = { net = "pub"; encryption = "none"; };
            "ZW_stage_legacy" = { net = "priv25"; };
            "ZW_iot_legacy" = { net = "saaliot"; };
          };
        };
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            "ZW_stage" = { net = "priv25"; };
            "ZW_iot" = { net = "saaliot"; };
          };
        };
      };
    };
    ap45 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv25.type = "bridge";
        saaliot.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        switch-a2 = {
          ports = [ "lan" ];
        };
      };
      location = "Saal A mitte";
      model = "ubnt_unifi-nanohd";
      role = "ap";
      wifi = {
        "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0" = {
          channel = 5;
          htmode = "HT20";
          ssids = {
            "ZW public legacy" = { net = "pub"; encryption = "none"; };
            "ZW_stage_legacy" = { net = "priv25"; };
            "ZW_iot_legacy" = { net = "saaliot"; };
          };
        };
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0" = {
          channel = 149;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            "ZW_stage" = { net = "priv25"; };
            "ZW_iot" = { net = "saaliot"; };
          };
        };
      };
    };
    ap46 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv25.type = "bridge";
        saaliot.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        switch-a2 = {
          ports = [ "lan" ];
        };
      };
      location = "Saal A hinten";
      model = "ubnt_unifi-nanohd";
      role = "ap";
      wifi = {
        "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0" = {
          channel = 9;
          htmode = "HT20";
          ssids = {
            "ZW public legacy" = { net = "pub"; encryption = "none"; };
            "ZW_stage_legacy" = { net = "priv25"; };
            "ZW_iot_legacy" = { net = "saaliot"; };
          };
        };
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            "ZW_stage" = { net = "priv25"; };
            "ZW_iot" = { net = "saaliot"; };
          };
        };
      };
    };
    ap47 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv25.type = "bridge";
        saaliot.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        switch-a2 = {
          ports = [ "lan" ];
        };
      };
      location = "Saal Foyer";
      model = "ubnt_unifi-nanohd";
      role = "ap";
      wifi = {
        "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "ZW public legacy" = { net = "pub"; encryption = "none"; };
            "ZW_stage_legacy" = { net = "priv25"; };
            "ZW_iot_legacy" = { net = "saaliot"; };
          };
        };
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0" = {
          channel = 149;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            "ZW_stage" = { net = "priv25"; };
            "ZW_iot" = { net = "saaliot"; };
          };
        };
      };
    };
    ap48 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv25.type = "bridge";
        saaliot.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        switch-a2 = {
          ports = [ "lan" ];
        };
      };
      location = "Saal A Kleiner Saal Tuer";
      model = "ubnt_unifi-nanohd";
      role = "ap";
      wifi = {
        "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0" = {
          channel = 5;
          htmode = "HT20";
          ssids = {
            "ZW public legacy" = { net = "pub"; encryption = "none"; };
            "ZW_stage_legacy" = { net = "priv25"; };
            "ZW_iot_legacy" = { net = "saaliot"; };
          };
        };
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            "ZW_stage" = { net = "priv25"; };
            "ZW_iot" = { net = "saaliot"; };
          };
        };
      };
    };
    ap49 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv25.type = "bridge";
        saaliot.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        switch-a2 = {
          ports = [ "lan" ];
        };
      };
      location = "Saal A Kabinett";
      model = "ubnt_unifi-nanohd";
      role = "ap";
      wifi = {
        "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0" = {
          channel = 9;
          htmode = "HT20";
          ssids = {
            "ZW public legacy" = { net = "pub"; encryption = "none"; };
            "ZW_stage_legacy" = { net = "priv25"; };
            "ZW_iot_legacy" = { net = "saaliot"; };
          };
        };
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0" = {
          channel = 149;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            "ZW_stage" = { net = "priv25"; };
            "ZW_iot" = { net = "saaliot"; };
          };
        };
      };
    };
    ap5 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv5.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv5 = {
          ports = [ "lan" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "a";
      model = "tplink_tl-wr1043nd-v3";
      role = "ap";
      wifi = {
        "platform/ahb/18100000.wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            "verbalwerk.de" = { net = "priv5"; };
          };
        };
      };
    };
    ap50 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv25.type = "bridge";
        priv32.type = "bridge";
        saaliot.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        switch-a2 = {
          ports = [ "lan" ];
        };
      };
      location = "Saal A Kleiner Saal Buehne";
      model = "ubnt_unifi-nanohd";
      role = "ap";
      wifi = {
        "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "ZW public legacy" = { net = "pub"; encryption = "none"; };
            "ZW_stage_legacy" = { net = "priv25"; };
            "ZW_iot_legacy" = { net = "saaliot"; };
            gerdwork = { net = "priv32"; };
          };
        };
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            "ZW_stage" = { net = "priv25"; };
            "ZW_iot" = { net = "saaliot"; };
          };
        };
      };
    };
    ap51 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv17.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv17 = {
          ports = [ "lan" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "antrares";
      model = "tplink_archer-c7-v2";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 106;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            antrares = { net = "priv17"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 9;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            antrares = { net = "priv17"; };
          };
        };
      };
    };
    ap52 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv25.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        switch-a2 = {
          ports = [ "lan" ];
        };
      };
      location = "Saal (TODO)";
      model = "ubnt_unifi-nanohd";
      role = "ap";
      wifi = {
        "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0" = {
          channel = 9;
          htmode = "HT20";
          ssids = {
            "ZW public legacy" = { net = "pub"; encryption = "none"; };
            "ZW_stage_legacy" = { net = "priv25"; };
            "ZW_iot_legacy" = { net = "saaliot"; };
          };
        };
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0" = {
          channel = 149;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            "ZW_stage" = { net = "priv25"; };
            "ZW_iot" = { net = "saaliot"; };
          };
        };
      };
    };
    ap53 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv11.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv11 = {
          ports = [ "lan:4" "lan:3" "lan:2" "lan:1" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "B2.05.01";
      model = "tl-wr841-v10";
      role = "ap";
      wifi = {
        "platform/qca953x_wmac" = {
          channel = 9;
          htmode = "HT20";
          ssids = {
            "Karen Koschnick" = { net = "priv11"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap54 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv35.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv35 = {
          ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "Removed";
      model = "tplink_archer-c7-v5";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 128;
          htmode = "HT20";
          ssids = {
            Abyssinia = { net = "priv35"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            Abyssinia = { net = "priv35"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap55 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv6.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv6 = {
          ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "B3.05.03";
      model = "tplink_archer-c7-v5";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            MagLAN = { net = "priv6"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 9;
          htmode = "HT20";
          ssids = {
            "MagLAN (legacy)" = { net = "priv6"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap56 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv6.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv6 = {
          ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "B4.04.01";
      model = "tplink_archer-c7-v5";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 100;
          htmode = "VHT80";
          ssids = {
            MagLAN = { net = "priv6"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "MagLAN (legacy)" = { net = "priv6"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap57 = {
      role = "ap";
      model = "ubnt_unifiac-mesh";
      location = "Dach";
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "bridge";
        };
        pub.type = "bridge";
        roof.type = "phys";
        ap58 = {
          type = "vxlan";
          vxlan.peer = config.site.net.roof.hosts6.dn42.ap58;
        };
        priv43.type = "bridge";
      };
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 100;
          htmode = "HT40";
          ssids = {
            "Zentralwerk" = {
              net = "roof";
              disassocLowAck = false;
            };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "ZW public legacy" = { net = "pub"; encryption = "none"; };
          };
        };
      };
      links = {
        switch-dach.ports = [ "lan" ];
        ap58.ports = [ "ap58" ];
      };
    };
    ap58 = {
      role = "ap";
      model = "ubnt_unifiac-mesh";
      location = "Coswiger Str.";
      interfaces = {
        roof.type = "phys";
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        pub.type = "bridge";
        ap57 = {
          type = "vxlan";
          vxlan.peer = config.site.net.roof.hosts6.dn42.ap57;
        };
        priv43.type = "bridge";
      };
      wifiOnLink.enable = false;
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 100;
          htmode = "VHT80";
          ssids = {
            "Zentralwerk" = {
              net = "roof";
              mode = "sta";
            };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 9;
          htmode = "HT20";
          ssids = {
            "ZW public".net = "pub";
            "LIZA".net = "priv43";
          };
        };
      };
      links = {
        priv43.ports = [ "lan" ];
        ap57.ports = [ "ap57" ];
      };
    };
    ap59 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        pub.type = "bridge";
        priv21.type = "bridge";
      };
      links = {
        priv21 = {
          ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        };
        switch-d2 = {
          ports = [ "wan" ];
        };
      };
      location = "Turm D, Erdgeschoss";
      model = "tplink_archer-c7-v5";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            "Ebs 5000" = { net = "priv21"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 6;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            "Ebs 2000" = { net = "priv21"; };
          };
        };
      };
    };
    ap6 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        pub.type = "bridge";
      };
      links = {
        pub = {
          ports = [ "lan:4" "lan:3" "lan:2" "lan:1" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "Broken flash";
      model = "tl-wr841-v10";
      role = "ap";
      wifi = {
        "platform/qca953x_wmac" = {
          channel = 13;
          htmode = "HT20";
          ssids = { "ZW public" = { net = "pub"; }; };
        };
      };
    };
    ap60 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv35.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv35 = {
          ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "B1.05.02";
      model = "tplink_archer-c7-v5";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 128;
          htmode = "HT20";
          ssids = {
            Abyssinia = { net = "priv35"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            Abyssinia = { net = "priv35"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap61 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv44.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        pub.ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        switch-c1.ports = [ "wan" ];
      };
      location = "Turm C, 3. Etage";
      model = "tplink_archer-c7-v5";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 36;
          htmode = "HT20";
          ssids = {
            tomiru = { net = "priv44"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            tomiru = { net = "priv44"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap62 = {
      interfaces = {
        priv45.type = "bridge";
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        pub.type = "bridge";
      };
      links = {
        priv45 = {
          ports = [ "lan" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "B 4.06";
      model = "tplink_archer-c7-v2";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            "Wolke7" = { net = "priv45"; encryption = "wpa2"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 9;
          htmode = "HT20";
          ssids = {
            "Wolke7 legacy" = { net = "priv45"; encryption = "wpa2"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap63 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        pub.type = "bridge";
        priv7.type = "bridge";
      };
      links = {
        priv7 = {
          ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "B 4.TODO";
      model = "tplink_tl-wdr3600-v1";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 36;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            EckiTino = { net = "priv7"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 9;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            "EckiTino legacy" = { net = "priv7"; };
          };
        };
      };
    };
    ap64 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv46.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv46 = {
          ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "replaced by ap73";
      model = "tplink_tl-wr1043nd-v2";
      role = "ap";
      wifi = {
        "platform/ahb/18100000.wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            "Princess Castle" = { net = "priv46"; };
          };
        };
      };
    };
    ap65 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv12.type = "bridge";
        priv27.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        switch-b3.ports = [ "lan" ];
      };
      location = "El Perro";
      model = "ubnt_unifi-6-lite";
      role = "ap";
      wifi = {
        "1e140000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0" = {
          channel = 6;
          htmode = "HT20";
          ssids = {
            "ZW public".net = "pub";
            "farbwerk".net = "priv12";
            "Kaffeetasse".net = "priv27";
          };
        };
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0" = {
          channel = 149;
          htmode = "VHT80";
          ssids = {
            "ZW public".net = "pub";
            "farbwerk".net = "priv12";
          };
        };
      };
    };
    ap66 = {
      interfaces = {
        priv48.type = "bridge";
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        pub.type = "bridge";
      };
      links = {
        priv48.ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        switch-b3.ports = [ "wan" ];
      };
      location = "B 4.03.01";
      model = "tplink_archer-c7-v5";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            "Buschfunk4.03" = { net = "priv48"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 9;
          htmode = "HT20";
          ssids = {
            "Buschfunk4.03 legacy" = { net = "priv48"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap67 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv12.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv12.ports = [
          "lan1" "lan2" "lan3"
        ];
        switch-b3.ports = [ "wan" ];
      };
      location = "Farbwerk";
      model = "zyxel_wsm20";
      role = "ap";
      wifi = {
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0" = {
          channel = 6;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            farbwerk = { net = "priv12"; };
          };
        };
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0+1" = {
          channel = 149;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            farbwerk = { net = "priv12"; };
          };
        };
      };
    };
    ap68 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv12.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv12.ports = [
          "lan1" "lan2" "lan3"
        ];
        switch-b3.ports = [ "wan" ];
      };
      location = "Farbwerk";
      model = "zyxel_wsm20";
      role = "ap";
      wifi = {
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            farbwerk = { net = "priv12"; };
          };
        };
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0+1" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            farbwerk = { net = "priv12"; };
          };
        };
      };
    };
    ap69 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv43.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv43 = {
          ports = [ "lan" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "B.01.B01";
      model = "tplink_archer-c7-v2";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 36;
          htmode = "HT40+";
          ssids = {
            "ZW public".net = "pub";
            "LIZA".net = "priv43";
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "ZW public".net = "pub";
            "LIZA".net = "priv43";
          };
        };
      };
    };
    ap7 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv40.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        pub = {
          ports = [ "lan:4" "lan:3" "lan:2" "lan:1" ];
        };
        switch-d1 = {
          ports = [ "wan" ];
        };
      };
      location = "Turm D, 5. Etage";
      model = "tl-wr841-v10";
      role = "ap";
      wifi = {
        "platform/qca953x_wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            mino = { net = "priv40"; };
          };
        };
      };
    };
    ap70 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv22.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv22.ports = [ "lan" ];
        ap40.ports = [ "wan" ];
      };
      location = "B4.01 behind ap40";
      model = "tplink_archer-c7-v2";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 149;
          htmode = "HT40+";
          ssids = {
            "ZW public".net = "pub";
            M.net = "priv22";
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 9;
          htmode = "HT20";
          ssids = {
            "ZW public".net = "pub";
            "M legacy".net = "priv22";
          };
        };
      };
    };
    ap71 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv22.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv22.ports = [ "eth1" "eth2" ];
        ap40.ports = [ "eth0" ];
      };
      location = "B4.01 behind ap40";
      model = "ubnt_unifi-usg";
      role = "ap";
      # No WiFi, splits just VLANs
    };
    ap72 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv12.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv12.ports = [
          "lan1" "lan2" "lan3"
        ];
        switch-b3.ports = [ "wan" ];
      };
      location = "B1.05.02 (Patchpanel B12)";
      model = "zyxel_wsm20";
      role = "ap";
      wifi = {
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            farbwerk = { net = "priv12"; };
          };
        };
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0+1" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            farbwerk = { net = "priv12"; };
          };
        };
      };
    };
    ap73 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv46.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv46.ports = [
          "lan1" "lan2" "lan3"
        ];
        switch-b3.ports = [ "wan" ];
      };
      location = "B4.07";
      model = "zyxel_wsm20";
      role = "ap";
      wifi = {
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            "Princess Castle" = { net = "priv46"; };
          };
        };
        "1e140000.pcie/pci0000:00/0000:00:01.0/0000:02:00.0+1" = {
          channel = 36;
          htmode = "VHT80";
          ssids = {
            "ZW public" = { net = "pub"; };
            "Princess Castle" = { net = "priv46"; };
          };
        };
      };
    };
    ap74 = {
      interfaces = {
        c3d2.type = "bridge";
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv23.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv23 = {
          ports = [ "lan1" "lan2" "lan3" "lan4" ];
        };
        switch-b3 = {
          ports = [ "wan" ];
        };
      };
      location = "Poelzi";
      model = "asus_tuf-ax6000";
      role = "ap";
      wifi = {
        "platform/soc/18000000.wifi+1" = {
          channel = 36;
          htmode = "HT20";
          ssids = {
            teknologi = { net = "priv23"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/soc/18000000.wifi" = {
          channel = 13;
          htmode = "HT20";
          ssids = {
            "ZW public" = { net = "pub"; };
            teknologi = { net = "priv23"; };
          };
        };
      };
    };
    ap8 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv14.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        priv14.ports = [ "lan:1" "lan:2" "lan:3" "lan:4" ];
        switch-b3.ports = [ "wan" ];
      };
      location = "B 4.02";
      model = "tplink_tl-wdr4300-v1";
      role = "ap";
      wifi = {
        "pci0000:00/0000:00:00.0" = {
          channel = 36;
          htmode = "HT20";
          ssids = {
            WLANb0402 = { net = "priv14"; };
            "ZW public" = { net = "pub"; };
          };
        };
        "platform/ahb/18100000.wmac" = {
          channel = 13;
          htmode = "HT20";
          ssids = {
            WLANb0402 = { net = "priv14"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
    ap9 = {
      interfaces = {
        mgmt = {
          gw4 = "mgmt-gw";
          gw6 = "mgmt-gw";
          type = "phys";
        };
        priv16.type = "bridge";
        pub.type = "bridge";
      };
      links = {
        pub = {
          ports = [ "lan:4" "lan:3" "lan:2" "lan:1" ];
        };
        switch-d1 = {
          ports = [ "wan" ];
        };
      };
      location = "Turm D, 2. Etage";
      model = "tl-wr841-v10";
      role = "ap";
      wifi = {
        "platform/qca953x_wmac" = {
          channel = 1;
          htmode = "HT20";
          ssids = {
            Herzzbuehne = { net = "priv16"; };
            "ZW public" = { net = "pub"; };
          };
        };
      };
    };
  };
}
