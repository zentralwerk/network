{ config, lib, ... }:
let
  containers = lib.filterAttrs (_: { role, ... }:
    role == "container"
  ) config.site.hosts;

  bridgeInterfaces = builtins.foldl' (result: { interfaces, ... }:
    result // builtins.mapAttrs (_: _: {
      type = "bridge";
    }) (
      lib.filterAttrs (_: { type, ... }:
        builtins.elem type [ "veth" "bridge" ]
      ) interfaces
    )
  ) {} (builtins.attrValues containers);

  makeServer = _name: {
    role = "server";
    model = "pc";
    interfaces = bridgeInterfaces // {
      cluster = {
        type = "bridge";
        gw4 = "cls-gw";
        gw6 = "cls-gw";
      };
    };
  };
in
{
  site = {
    hosts = {
      server1 = makeServer "server1";
      server2 = makeServer "server2";
    };

    sshPubKeys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDOFs2LdK23ysS0SSkXZuULUOCZHe1ZxvfOKj002J6rkvAaDLar9g5aKuiIV70ZR33A2rchoLMiM4pLLwoSAPJg1FgIgJjU+DFoWtiW+IjzKXdHHVspb2iOIhpfbfk8WC5HZ/6fPz4RUqadGQ43ImnMhSN0ge3s/oM48hpc96ne6tH+mGiugdPx8097NE9yTqJHi8deBhi3daeJH4eQeg66Fi+kDIAZv5TJ0Oca5h7PBd253/vf3l21jRH8u1D1trALv9KStGycTk5Nwih+OHx+Rnvue/B/nxgAz4I3mmQa+jhRlGaQVG0MtOBRY3Ae7ZNqhjuefDUCM2hwG70toU9xDUw0AihC2ownY+P2PjssoG1O8f/D7ilw7qrXJHEeM8HwzqMH8X4ELYHaHTwjeWfZTTFev1Djr969LjdS1UZzqCZHO0jmQ5Pa3eXw8xcoprtt620kYLTKSMs6exLstE48o57Yqfn+eTJDy7EkcjiLN6GNIi42b9Z73xXNpZx1WR9O6OulJf/6pWgrApasvxiGmxxILq98s1/VnZkOFXR8JXnpvKHEIOIr3bFQu3GLCrzY2Yuh4NL5wy6lcZNTr/0rr6AO24IbEWM7TApbXnKA5XQhAbThrVsuFBdT3+bBP2nedvWQ0W+Q6SUf+8T2o5InnFqs5ABnTixBItiWw+9BiQ== root@server1"
    ];
  };
}
