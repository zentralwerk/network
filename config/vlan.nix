{ lib, ... }:

{
  site.net = builtins.mapAttrs (_: vlan: { inherit vlan; }) {
    # switches and CPE only have IP addresses configured in the management vlan
    mgmt = 1;
    # routers, OSPF area 0
    core = 2;
    # servers...
    serv = 3;
    # ZW public
    pub = 4;
    # C3D2 home network
    c3d2 = 5;
    cluster = 6;
    bmx = 7;
    flpk = 8;
    coloradio = 9;
    # Modems
    up1 = 10;
    up2 = 11;
    up3 = 12;
    up4 = 13;
    # IoT
    c3d2iot = 20;
    # Playground
    play = 21;
    # IoT
    saaliot = 22;
    solar = 23;
    # Isolated neighbors directly connectied with their modems
    iso1 = 101;
    iso2 = 102;
    iso3 = 103;
    iso4 = 104;
    iso5 = 105;
    iso6 = 106;
  } // builtins.foldl' (result: i:
    # Neighbor subnets
    result // {
      "priv${toString i}".vlan = i + 39;
    }
  ) {} (lib.range 1 61);
}
